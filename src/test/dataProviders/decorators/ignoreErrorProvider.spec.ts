/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {AsyncTest, Expect, TestFixture} from "alsatian";
import {DataProvider} from "../../../main/dataProviders/dataProvider";
import {DataObject} from "../../../main/dataProviders/dataObjects/dataObject";
import {IgnoreErrorProvider} from "../../../main/dataProviders/decorators/ignoreErrorProvider";

class DummyProvider<T extends DataObject> implements DataProvider<T, void> {
    public constructor(public readonly shouldThrow: boolean) { }

    public getContext() {
        return async () => {
            if (this.shouldThrow) {
                throw Error("Dummy throw");
            }
            return [];
        };
    }
}

@TestFixture("Testing ignore error provider")
export class IgnoreErrorProviderSpec {

    @AsyncTest("ignore error")
    public async testRetry() {
        const context = new IgnoreErrorProvider(new DummyProvider(false)).getContext({});

        await Expect(async () => context()).not.toThrowAsync();
    }

    @AsyncTest("ignore error (FAIL)")
    public async testRetryFail() {
        const context = new IgnoreErrorProvider(new DummyProvider(true)).getContext({});

        // errors is propagated
        await Expect(async () => context()).not.toThrowAsync();
    }
}