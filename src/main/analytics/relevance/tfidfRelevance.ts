import {DataObject} from "../../dataProviders/dataObjects/dataObject";
import {TokenizerAndStemmer} from "../../util/tokenizerAndStemmer";

/**
 The MIT License (MIT)

 Copyright (c) 2017 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

export class TfidfRelevance<T extends DataObject> {

	private preparer: TokenizerAndStemmer = new TokenizerAndStemmer();
	private termCount: Map<string, number> = new Map<string, number>();
	private documents: Array<Array<string>> = [];


	/**
	 * calculates the tfidf of the document and keyword
	 * @param {number} document the document
	 * @param {string} keyword the keyword to be searched. Should be stemmed.
	 * @returns {number} tfidf
	 */
	public tfidf(document: Array<string>, keyword: string): number {
		const tfidf = this.tf(keyword, document) * this.idf(keyword);
		return Math.min(tfidf, 1);
	}

	/**
	 * returns the average tfidf value of all keywords in the document
	 * @param {Array<string>} document the document to be searched in. Has to be tokenized
	 * @param {Array<string>} keywords the keywords. should be stemmed
	 * @returns {number} average tfidf of all keywords
	 */
	public tfIdfAvg(document: Array<string>, keywords: Array<string>): number {
		let retSum = 0;
		let hitCount = 0;
		keywords.forEach((kw) => {
			let tmp = this.tfidf(document, kw);
			retSum += tmp;
			if (tmp > 0)
				hitCount++;
		});
		if (hitCount == 0) return 0;
		return retSum / hitCount;
	}

	/**
	 * returns the vector of all the tfidfs of the keywords
	 * @param {Array<string>} document tokenized and stemmed document
	 * @param {Array<string>} keywords the keywords. should be stemmed
	 * @returns {Array<number>} tfidf vector of all keywords
	 */
	public tfIdfVector(document: Array<string>, keywords: Array<string>): Array<number> {
		let retVector: Array<number> = [];
		keywords.forEach((kw) => {
			retVector.push(this.tfidf(document, kw));
		});
		return retVector;

	}

	/**
	 * adds a document to the intern document field
	 * @param {Array<string>} document tokenized and stemmed document
	 * @returns {number} the id of the added document
	 */
	public addDocument(document: Array<string>): number {
		let ret = this.documents.push(document);
		document.forEach(word => {
			let freq = this.termCount.get(word);
			if (freq == undefined)
				this.termCount.set(word, 1);
			else
				this.termCount.set(word, freq + 1);
		});
		return ret - 1;
	}

    /**
	 * prepares the documents and adds them to the tfidf analysis array
     * @param {Array<DataObject>} data an array of all the dataobjects that are to be added
     * @returns {Array<Array<string>>} returns the internal document corpus
     */
	public addDocuments(data: Array<T>): Array<Array<string>> {
		data.forEach(dataobject => {
			this.addDocument(this.preparer.prepareDataObject(dataobject));
		});
		return this.documents;
	}

	/**
	 * calculates the inverse document frequency of the term over all documents
	 * @param {string} term the term to be searched. Should be stemmed.
	 * @returns {number} the inverse document frequency
	 */
	private idf(term: string): number {
		let count = this.termCount.get(term);
		if (count == undefined || 0) {
			return 0;
		} else {
			return Math.log(this.documents.length / count + 1);
		}
	}

	/**
	 * calculates the term frequency of the term in the document
	 * @param {string} term the term to be searched. Should be stemmed.
	 * @param {Array<string>} document the document to search in. Should be tokenized and stemmed.
	 * @returns {number} the term frequency of the term in the document
	 */
	private tf(term: string, document: Array<string>): number {
		let freq = 0;
		document.forEach(word => {
			if (word.includes(term)) {
				freq++;
			}
		});
		return freq;
	}

}