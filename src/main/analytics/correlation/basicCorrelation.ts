/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {DataObject} from "../../dataProviders/dataObjects/dataObject";
import {Correlation} from "./correlation";
import {Reason} from "./reason/reason";
import {createHmac} from "crypto";
import {avg} from "../../util/arrayUtil";
import {StringMap} from "../../util/stringMap";

/**
 * Represents a BasicCorrelation between multiple DataObjects
 */
export class BasicCorrelation implements Correlation {

    public readonly id: string;
    public readonly avgRelevance: number;
    public readonly avgPopularity: number;

    /**
     * constructs a basic correlation
     * @param {Array<DataObject>} dataObjects array of DataObjects, which correlate
     * @param {number} correlationFactor correlation factor between 0 and 1
     * @param {StringMap<Reason>} reasons map of reasons, which tells why the DataObjects correlate.
     *                                    The key is the type of reason
     */
    public constructor(public readonly dataObjects: Array<DataObject>,
                       public readonly correlationFactor: number,
                       public readonly reasons: StringMap<Reason>
    ) {
        this.id = this.calcId();
        this.avgRelevance = this.calcAvgRelevance();
        this.avgPopularity = this.calcAvgPopularity();
    }

    /**
     * Identifies a FoundCorrelation - the DataObject are sorted alphabetically and
     * hashed with key="key" to reduce the id length
     * This guarantees consistent IDs
     * @returns {string} unique identifier
     */
    private calcId(): string {
        //TODO might be performance bottleneck -> replace with ids that are unique in report (incrementing numbers). That would need to be done when creating a report
        // hashed to reduce the id length
        const text = this.dataObjects
            .map((dataObject) => dataObject._id)
            .sort()
            .toString();

        const key = "key";
        return createHmac("sha512", key).update(text).digest("hex");
    }

    /**
     * @returns {number} relevance just averaged out with equal weighting
     */
    private calcAvgRelevance(): number {
        return avg(this.dataObjects.map(dataObject => dataObject.relevance));
    }

    /**
     * @returns {number} popularity just averaged out with equal weighting
     */
    private calcAvgPopularity(): number {
        return avg(this.dataObjects.map(dataObject => dataObject.popularity));
    }
}