/**
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


import {DataObject} from "./dataObject";
import * as moment from "moment";
import {ProviderInfo} from "./providerInfo";

// This class should represent the relevant attributes of a single tweet
export class Tweet extends DataObject {

    // public readonly user: { id: number }; // Add more user attributes when needed
    public readonly retweet_count: number;
    public readonly favorite_count: number;

    /**
     * Creates a new Tweet object
     * @param {*} tweet returned from the Twitter API (untyped)
     * @param {Array<string>} links extracted links from the tweet with resolved URLs
     * @param {string} user who tweeted the tweet
     */
	public constructor(tweet: any, links: Array<string>, user: string) {
        super("TW" + tweet.id_str, tweet.text, undefined, moment(new Date(tweet.created_at)), links, "https://twitter.com/statuses/" + tweet.id_str,  new ProviderInfo("Twitter", user)); // Date passed through js-Date class to overcome formatting issues

        // this.user = { id: tweet.user.id };
        this.retweet_count = tweet.retweet_count;
        this.favorite_count = tweet.favorite_count;
    }

    /**
     * Provides an interface for the popularity calculations
     * @returns {*} list of attributes that are needed for the popularity calculations, with weight being between 0 and 1 and all weights adding up to 1
     */
    public get popularityAttributes(): Array<{ value: number, weight: number }> {
        return [{ value: this.retweet_count, weight: 0.5 }, {value: this.favorite_count, weight: 0.5 }];
    }
}