/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {Test, AsyncTest, Expect, TestCase, TestFixture} from "alsatian";
import {DataProvider} from "../../../main/dataProviders/dataProvider";
import {DataObject} from "../../../main/dataProviders/dataObjects/dataObject";
import {AddPopularity} from "../../../main/dataProviders/decorators/addPopularity";
import * as moment from "moment";
import {DataSourceCollection} from "../../../main/dataProviders/dataSourceCollection";
import {ProviderInfo} from "../../../main/dataProviders/dataObjects/providerInfo";

// Use this dummy class for testing since DataObject is abstract
class RelevanceDataObject extends DataObject {

    public constructor(private readonly att1: number, private readonly att2: number) {
        super("dasda", "sada", "", moment(), [], "originURL", new ProviderInfo("RelevanceDataObject", "TEST"));
    }

    public get popularityAttributes() {
        return [{ value: this.att1, weight: 0.5 }, { value: this.att2, weight: 0.5 }];
    }
}

class DummyProvider<T extends DataObject> implements DataProvider<T, void> {
    public constructor(public readonly data: Array<T>) { }

    public getContext() {
        return async () => {
            return this.data;
        };
    }
}

@TestFixture("Testing AddPopularity")
export class AddRelevanceSpecSpec {

    @TestCase([new RelevanceDataObject(3.5, 2)])
    @TestCase([new RelevanceDataObject(3.5, 2), new RelevanceDataObject(1.5, 0)])
    @AsyncTest(".decorate")
    public async testDecorate(arr: Array<DataObject>) {
        const context = new AddPopularity(new DummyProvider(arr)).getContext({});
        const result = await context();

        Expect(result.length).toEqual(arr.length);
        result.forEach((obj) => Expect(obj.popularity).toBeDefined());
    }

    @TestCase([new RelevanceDataObject(3.5, 2)])
    @TestCase([new RelevanceDataObject(3.5, 2), new RelevanceDataObject(1.5, 0)])
    @AsyncTest(".decorate (Alter)")
    public async testDecorateAlter(arr: Array<DataObject>) {
        // If popularity is already set, it may not alter it
        arr.forEach((obj) => obj.popularity = 0.99);

        const result = await new AddPopularity(new DummyProvider(arr)).getContext({})();
        result.forEach((obj) => Expect(obj.popularity).toEqual(0.99));
    }

    @Test("decoraterAddedToProviderManager")
    public async testAll() {
        const c = new DataSourceCollection();
        await c.updateSources();

        const result = await c.getContext()();

        for(let obj of result)
            Expect(obj.popularity).toBeDefined();
    }
}