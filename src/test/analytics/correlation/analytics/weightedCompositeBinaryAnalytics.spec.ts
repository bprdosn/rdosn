/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {Expect, Test, TestCase, TestFixture} from "alsatian";
import {DataObject} from "../../../../main/dataProviders/dataObjects/dataObject";
import {WeightedCompositeBinaryAnalytics} from "../../../../main/analytics/correlation/analytics/weightedCompositeBinaryAnalytics";
import {BinaryLinkAnalytics} from "../../../../main/analytics/correlation/analytics/binaryLinkAnalytics";
import {BinaryKeywordAnalytics} from "../../../../main/analytics/correlation/analytics/binaryKeywordAnalytics";
import {MinDataObject} from "../../../dataProviders/dataObjects/minDataObject";
import moment = require("moment");
import {arraysEquals} from "../../../../main/util/arrayUtil";
import {DummyBinaryAnalyser} from "./dummyBinaryAnalyzer";
import {Correlation} from "../../../../main/analytics/correlation/correlation";

const testAnalyser = new WeightedCompositeBinaryAnalytics([
    {analytics: new BinaryLinkAnalytics(), weight: (f) => f},
    {analytics: new BinaryKeywordAnalytics(), weight: (f) => f}
]);


const dummyAnalyser = new WeightedCompositeBinaryAnalytics([
    {analytics: new DummyBinaryAnalyser("Type1"), weight: () => 0.2},
    {analytics: new DummyBinaryAnalyser("Type2"), weight: () => 0.8}
]);

@TestFixture("Testing WeightedCompositeBinaryAnalytics")
export class WeightedCompositeBinaryAnalyticsSpec {

    @TestCase({ _id:"1", content_short: "IPhoNe EXPLOIT bei APPLE?", links: ["www.example.com"]},
        { _id:"2", content_short: "OMG WTF! APplE! iphone? exploit!", links: ["www.example.com"]}, 0.99)
    @TestCase({ _id:"1", content_short: "IPhoNe EXPLOIT bei APPLE?", links: ["www.example2.com"]},
        { _id:"2", content_short: "OMG WTF! SAMsung! LG? in AndroiD!", links: ["www.example2.com"]}, 0.99)
    @Test("moreThan correlationFactor .analyze")
    public testMoreAnalyze(obj1: DataObject, obj2: DataObject, expected: number) {
        Expect(WeightedCompositeBinaryAnalyticsSpec.getCorrelation(obj1, obj2).correlationFactor).toBeGreaterThan(expected);
    }

    @TestCase({ _id:"1", content_short: "IPhoNe EXPLOIT bei APPLE?", links: []},
        { _id:"2", content_short: "OMG WTF! SAMsung! LG? in AndroiD!", links: []}, 0.01)
    @TestCase({ _id:"1", content_short: "IPhoNe EXPLOIT bei APPLE?", links: ["www.example1.com"]},
        { _id:"2", content_short: "OMG WTF! SAMsung! LG? in AndroiD!", links: ["www.example2.com"]}, 0.01)
    @Test("lessThan correlationFactor .analyze")
    public testLessAnalyze(obj1: DataObject, obj2: DataObject, expected: number) {
        Expect(WeightedCompositeBinaryAnalyticsSpec.getCorrelation(obj1, obj2).correlationFactor).toBeLessThan(expected);
    }


    @Test("test reason array")
    public testReasons() {

        // create some data objects
        const dataObjects: Array<DataObject> = [];
        for (let i = 0; i < 3; i++) {
            dataObjects.push();
        }

        const obj1 = new MinDataObject("1", "", "", moment(), [], "");
        const obj2 = new MinDataObject("2", "", "", moment(), [], "");

        const result = dummyAnalyser.analyze(obj1, obj2);

        Expect(arraysEquals(result.dataObjects, [obj1, obj2])).toBeTruthy();
        Expect(arraysEquals(Object.keys(result.reasons), ["Type1", "Type2"])).toBeTruthy();
        Expect(result.correlationFactor).toBe(0.5);
    }

    private static getCorrelation(obj1: DataObject, obj2: DataObject): Correlation {
        testAnalyser.prepare([obj1, obj2]);
        const result = testAnalyser.analyze(obj1, obj2);
        testAnalyser.finish();
        return result;
    }
}