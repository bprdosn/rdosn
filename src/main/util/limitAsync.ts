/*
 The MIT License (MIT)

 Copyright (c) 2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

/* eslint-disable valid-jsdoc */
/**
 * Calls the functions and returns each result in the array. The asynchronicity is limited to limit.
 * When an error occurs, everything fails.
 * @param {number} limit how many calls are made in parallel
 * @param {Array<() => Promise<T>>} functions every one is called.
 * @returns {Promise<Array<T>>} promise of all results. Should be the same as calling Promise.all(functions.map((f) => f()))
 */
export async function limitAsync<T>(limit: number, functions: Array<() => Promise<T>>): Promise<Array<T>> {
	return new Promise<Array<T>>((resolve, reject) => {

		// keeps track of which calls were already made (lower or equal i)
		let i: number = 0;
		// keep track of running calls to know when to stop
		const activeCalls: Set<number> = new Set();
		const results: Array<T> = [];

		const addCall = () => {

			if (i >= functions.length) {
				// nothing more to do
				// check if we are the last call
				if (activeCalls.size == 0) {
					// nothing left to do, return
					resolve(results);
				}
				return;
			}

			// save count for local use then increment
			const myI = i;
			i++;

			// mark start of call
			activeCalls.add(myI);
			functions[myI]()
				.then((result) => {
					// save result
					results.push(result);
					// mark end of call
					activeCalls.delete(myI);
					// start next call
					addCall();
				})
				.catch(e => reject(e));
		};

		limit = Math.min(limit, functions.length);
		for (i = 0; i < limit; /*i++ is in addCall()*/) {
			addCall();
		}
	});
}