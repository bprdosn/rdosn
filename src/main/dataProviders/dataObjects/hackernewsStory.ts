/**
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import * as moment from "moment";
import {DataObject} from "./dataObject";
import {isNullOrUndefined} from "util";
import {ProviderInfo} from "./providerInfo";

// Holds information on a single story
export class HackernewsStory extends DataObject {

    // public readonly descendants: number;
    public readonly score: number;
    // public readonly type: string;

    /**
     * Creates a new HeackernewsStory
     * @param {*} story from Provider, untyped
     */
    public constructor(story: any) {
        // some stories might not have a URL
        const urls = isNullOrUndefined(story.url) ? [] : [story.url];
        super("HN" + story.id, story.title, undefined, moment.unix(story.time), urls, "https://news.ycombinator.com/item?id=" + story.id.toString(), new ProviderInfo("Hackernews", story.by));

        // this.descendants = story.descendants;
        this.score = story.score;
        // this.type = story.type;
    }

    /**
     * Provides an interface for the popularity calculations
     * @returns {*} list of attributes that are needed for the popularity calculations, with weight being between 0 and 1 and all weights adding up to 1
     */
    public get popularityAttributes(): Array<{ value: number, weight: number }> {
        return [{ value: this.score, weight: 1 }];
    }
}
