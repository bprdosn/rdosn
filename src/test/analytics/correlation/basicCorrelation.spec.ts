/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {Test, TestFixture, TestCase, Expect} from "alsatian";
import {BasicCorrelation} from "../../../main/analytics/correlation/basicCorrelation";
import {DataObject} from "../../../main/dataProviders/dataObjects/dataObject";
import {Keywords} from "../../../main/analytics/correlation/reason/keywords";

@TestFixture("Testing BasicCorrelation")
export class basicCorrelationSpec {

    @TestCase([{ id: "asdca" }, { id: "gee" }], [{ id: "asdca" }, { id: "gee" }])
    @TestCase([{ id: "agvsdg" }, { id: "awrqwe" }], [{ id: "awrqwe" }, { id: "agvsdg" }])
    @TestCase([{ id: "agvsdg" }, { id: "asgsdfqwe" }, { id: "kjhgfd" }, { id: "urtjufddj" }, { id: "124mdl8" }], [{ id: "124mdl8" }, { id: "urtjufddj" }, { id: "kjhgfd" }, { id: "asgsdfqwe" }, { id: "agvsdg" }])
    @TestCase([{ id: "asgsdfqwe" }, { id: "afasf" }, { id: "jklhgfd" }], [{ id: "asgsdfqwe" }, { id: "jklhgfd" }, { id: "afasf" }])
    @Test(".id")
    public testID(obj1: Array<DataObject>, obj2: Array<DataObject>) {
        Expect(new BasicCorrelation(obj1, 0, Keywords(["someKeyword"], 0)).id).toBe(new BasicCorrelation(obj2, 0, Keywords(["someKeyword"], 0)).id);
    }

    @TestCase([{ popularity: 0.5 }, { popularity: 1}], 0.75)
    @TestCase([{ popularity: 0.5 }, { popularity: 0.5}], 0.5)
    @TestCase([{ popularity: 0 }, { popularity: 0.5}], 0.25)
    @TestCase([{ popularity: 0.5 }, { popularity: undefined}], 0.5)
    @TestCase([{ popularity: undefined }, { popularity: 0.5}], 0.5)
    @TestCase([{ popularity: 1 }, { popularity: 0.5}, { popularity: 0.25 }, { popularity: 0.2}, { popularity: 0.3 }, { popularity: 0.75}], 0.5)
    @TestCase([], 0)
    @Test(".avgPopularity")
    public testAvgPopularity(objects: Array<DataObject>, expected: number) {
        Expect(new BasicCorrelation(objects, 0, Keywords(["someKeyword"], 0)).avgPopularity).toBe(expected);
    }


	@TestCase([{ relevance: 0.5 }, { relevance: 1}], 0.75)
	@TestCase([{ relevance: 0.5 }, { relevance: 0.5}], 0.5)
	@TestCase([{ relevance: 0 }, { relevance: 0.5}], 0.25)
	@TestCase([{ relevance: 0.5 }, { relevance: undefined}], 0.5)
	@TestCase([{ relevance: undefined }, { relevance: 0.5}], 0.5)
	@TestCase([{ relevance: 1 }, { relevance: 0.5}, { relevance: 0.25 }, { relevance: 0.2}, { relevance: 0.3 }, { relevance: 0.75}], 0.5)
	@TestCase([], 0)
	@Test(".avgRelevance")
	public testAvgRelevance(objects: Array<DataObject>, expected: number) {
		Expect(new BasicCorrelation(objects, 0, Keywords(["someKeyword"], 0)).avgRelevance).toBe(expected);
	}
}
