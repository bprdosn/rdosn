/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {AsyncTest, Expect, Test, TestCase, TestFixture} from "alsatian";
import {DataObject} from "../../../../main/dataProviders/dataObjects/dataObject";
import {TransitiveCorrelationAnalytics} from "../../../../main/analytics/correlation/analytics/transitiveCorrelationAnalytics";
import {Correlation} from "../../../../main/analytics/correlation/correlation";
import {binaryAnalyser} from "../../../../main/config/analytics";
import {MinDataObject} from "../../../dataProviders/dataObjects/minDataObject";
import moment = require("moment");
import {arraysEquals} from "../../../../main/util/arrayUtil";
import {WeightedCompositeBinaryAnalytics} from "../../../../main/analytics/correlation/analytics/weightedCompositeBinaryAnalytics";
import {DummyBinaryAnalyser} from "./dummyBinaryAnalyzer";

const testAnalyser = new TransitiveCorrelationAnalytics(binaryAnalyser);

@TestFixture("Testing TransitiveCorrelationAnalytics")
export class TransitiveCorrelationAnalyticsSpec {

    @TestCase([new MinDataObject("1", "IPhoNe EXPLOIT bei APPLE?", undefined, undefined, ["www.example.com"], undefined, 0.5),
            new MinDataObject("2", "OMG WTF! APplE! iphone? exploit!", undefined, undefined, ["www.example.com"], undefined, 0.5)],
        [[{ content_short: "IPhoNe EXPLOIT bei APPLE?"}, { content_short: "OMG WTF! APplE! iphone? exploit!"}]])
    @TestCase([new MinDataObject("1", "IPhoNe EXPLOIT bei APPLE?", undefined, undefined, ["www.example.com"], undefined, 0.4),
        new MinDataObject("2", "OMG WTF! APplE! iphone? exploit!", undefined, undefined, ["www.example.com"], undefined, 0.3)], [[]]) //too low avgPopularity
    @TestCase([new MinDataObject("1", "IPhoNe EXPLOIT bei APPLE?", undefined, undefined, ["www.example.com"], undefined, 0.5),
            new MinDataObject("2", "OMG WTF! APplE! iphone? exploit!", undefined, undefined, ["www.example.com"], undefined, 0.5),
            new MinDataObject("3", "some Content", undefined, undefined, ["www.example2.com"], undefined, 0.5),
            new MinDataObject("4", "this should be in iphone of apple", undefined, undefined, ["www.example.com"], undefined, 0.5)],
        [[{ content_short: "IPhoNe EXPLOIT bei APPLE?"}, { content_short: "OMG WTF! APplE! iphone? exploit!"},
            { content_short: "this should be in iphone of apple" }]])
    @TestCase([new MinDataObject("1", "IPhoNe EXPLOIT bei APPLE?", undefined, undefined, ["www.example.com"], undefined, 0.5),
            new MinDataObject("2", "OMG WTF! APplE! iphone? exploit!", undefined, undefined, ["www.example.com"], undefined, 0.5),
            new MinDataObject("3", "some Content pi", undefined, undefined, ["www.example2.com"], undefined, 0.5),
            new MinDataObject("4", "this should be in iphone of apple", undefined, undefined, ["www.example.com"], undefined, 0.5),
            new MinDataObject("5", "new Row! pi", undefined, undefined, ["www.example23.com"], undefined, 0.5)],
        [[{ content_short: "IPhoNe EXPLOIT bei APPLE?"}, { content_short: "OMG WTF! APplE! iphone? exploit!"},
            { content_short: "this should be in iphone of apple" }], [{ content_short: "some Content pi" }, { content_short: "new Row! pi"}]])
    @TestCase([new MinDataObject("1", "IPhoNe EXPLOIT bei APPLE?", undefined, undefined, ["www.example.com"], undefined, 0.5),
            new MinDataObject("2", "OMG WTF! APplE! iphone? exploit!", undefined, undefined, ["www.example.com"], undefined, 0.5),
            new MinDataObject("3", "some Content pi", undefined, undefined, ["www.example2.com"], undefined, 0.5),
            new MinDataObject("4", "notInThere", undefined, undefined, [], undefined, 0.5),
            new MinDataObject("5", "OMG WTF! SAMsung! LG? in AndroiD", undefined, undefined, ["www.example.com"], undefined, 0.5),
            new MinDataObject("6", "new Row! pi", undefined, undefined, ["www.example2.com"], undefined, 0.5),
            new MinDataObject("7", "doesn't appear either", undefined, undefined, ["www.anotherexample.com"], undefined, 0.5),
            new MinDataObject("8", "OMG WTF! in AndroiD! SAMsung! and LG?", undefined, undefined, [], undefined, 0.5)],
        [[{ content_short: "IPhoNe EXPLOIT bei APPLE?"}, { content_short: "OMG WTF! APplE! iphone? exploit!"},
            { content_short: "OMG WTF! SAMsung! LG? in AndroiD" }, { content_short: "OMG WTF! in AndroiD! SAMsung! and LG?" }],
            [{ content_short: "some Content pi" }, { content_short: "new Row! pi"}]])
    @Test(".transitiveCorrelation")
    public testTransitiveCorrelation(values: DataObject[], arrayOfDataObjects: DataObject[][]) {

        Expect(testAnalyser.analyze(values).map((c: Correlation) =>
                c.dataObjects.map(dO => dO.content_short).toString()).toString())
            .toBe(arrayOfDataObjects.reduce((p, c) => p.concat(c), []).map( dO => dO.content_short).toString());
    }

    @TestCase([new MinDataObject("1", "IPhoNe EXPLOIT bei APPLE?", undefined, undefined, ["www.example.com"], undefined, 0.5),
               new MinDataObject("2", "OMG WTF! APplE! iphone? exploit!", undefined, undefined, ["www.example.com"], undefined, 0.5)],
              [[{ content_short: "IPhoNe EXPLOIT bei APPLE?"}, { content_short: "OMG WTF! APplE! iphone? exploit!"}]])
    @TestCase([new MinDataObject("1", "IPhoNe EXPLOIT bei APPLE?", undefined, undefined, ["www.example.com"], undefined, 0.4),
               new MinDataObject("2", "OMG WTF! APplE! iphone? exploit!", undefined, undefined, ["www.example.com"], undefined, 0.3)],
              [[]]) //too low avgPopularity
    @TestCase([new MinDataObject("1", "IPhoNe EXPLOIT bei APPLE?", undefined, undefined, ["www.example.com"], undefined, 0.5),
               new MinDataObject("2", "OMG WTF! APplE! iphone? exploit!", undefined, undefined, ["www.example.com"], undefined, 0.5),
               new MinDataObject("3", "some Content", undefined, undefined, ["www.example2.com"], undefined, 0.5),
               new MinDataObject("4", "this should be in iphone of apple", undefined, undefined, ["www.example.com"], undefined, 0.5)],
        [[{ content_short: "IPhoNe EXPLOIT bei APPLE?"}, { content_short: "OMG WTF! APplE! iphone? exploit!"},
            { content_short: "this should be in iphone of apple" }]])
    @TestCase([new MinDataObject("1", "IPhoNe EXPLOIT bei APPLE?", undefined, undefined, ["www.example.com"], undefined, 0.5),
               new MinDataObject("2", "OMG WTF! APplE! iphone? exploit!", undefined, undefined, ["www.example.com"], undefined, 0.5),
               new MinDataObject("3", "some Content pi", undefined, undefined, ["www.example2.com"], undefined, 0.5),
               new MinDataObject("4", "this should be in iphone of apple", undefined, undefined, ["www.example.com"], undefined, 0.5),
               new MinDataObject("5", "new Row! pi", undefined, undefined, ["www.example23.com"], undefined, 0.5)],
              [[{ content_short: "IPhoNe EXPLOIT bei APPLE?"}, { content_short: "OMG WTF! APplE! iphone? exploit!"},
                { content_short: "this should be in iphone of apple" }], [{ content_short: "some Content pi" }, { content_short: "new Row! pi"}]])
    @TestCase([new MinDataObject("1", "IPhoNe EXPLOIT bei APPLE?", undefined, undefined, ["www.example.com"], undefined, 0.5),
               new MinDataObject("2", "OMG WTF! APplE! iphone? exploit!", undefined, undefined, ["www.example.com"], undefined, 0.5),
               new MinDataObject("3", "some Content pi", undefined, undefined, ["www.example2.com"], undefined, 0.5),
               new MinDataObject("4", "notInThere", undefined, undefined, [], undefined, 0.5),
               new MinDataObject("5", "OMG WTF! SAMsung! LG? in AndroiD", undefined, undefined, ["www.example.com"], undefined, 0.5),
               new MinDataObject("6", "new Row! pi", undefined, undefined, ["www.example2.com"], undefined, 0.5),
               new MinDataObject("7", "doesn't appear either", undefined, undefined, ["www.anotherexample.com"], undefined, 0.5),
               new MinDataObject("8", "OMG WTF! in AndroiD! SAMsung! and LG?", undefined, undefined, [], undefined, 0.5)],
              [[{ content_short: "IPhoNe EXPLOIT bei APPLE?"}, { content_short: "OMG WTF! APplE! iphone? exploit!"},
                { content_short: "OMG WTF! SAMsung! LG? in AndroiD" }, { content_short: "OMG WTF! in AndroiD! SAMsung! and LG?" }],
               [{ content_short: "some Content pi" }, { content_short: "new Row! pi"}]])
    @AsyncTest("transitive Correlation Analytics .analyze")
    //@Timeout(1000) // May take a second
    public async testAnalyze(values: DataObject[], arrayOfDataObjects: DataObject[][]) {
        Expect((await testAnalyser.analyze(values))
            .map((c: Correlation) =>
                c.dataObjects.map(dO => dO.content_short).toString()).toString())
            .toBe(arrayOfDataObjects.reduce((p, c) => p.concat(c), []).map( dO => dO.content_short).toString());

    }


    @Test("transitive correlation All combined")
    public testAllCombined() {

        // create some data objects
        const dataObjects: Array<DataObject> = [];
        for (let i = 0; i < 3; i++) {
            dataObjects.push(new MinDataObject(i.toString(), "", "", moment(), [], ""));
        }


        // create a dummy analyser
        const analyser = new TransitiveCorrelationAnalytics(
            new WeightedCompositeBinaryAnalytics([
                {analytics: new DummyBinaryAnalyser("type1"), weight: () => 0.25},
                {analytics: new DummyBinaryAnalyser("type2"), weight: () => 0.75},
            ]),
            0,
            0
        );

        const result: Array<Correlation> = analyser.analyze(dataObjects);

        Expect(result.length).toBe(1);

        Expect(arraysEquals(
            result[0].dataObjects,
            dataObjects
        )).toBeTruthy();

        Expect(result[0].correlationFactor).toBe(0.5);

        const reasons = result[0].reasons;
        Expect(Object.keys(reasons).length).toBe(2);
        Expect(reasons.type1.value.length).toBe(3);
        Expect(reasons.type2.value.length).toBe(3);
    }

}
