/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {DataObject} from "../../../../main/dataProviders/dataObjects/dataObject";
import {Expect, Test, TestCase, TestFixture} from "alsatian";
import {BinaryLevenshteinAnalytics} from "../../../../main/analytics/correlation/analytics/binaryLevenshteinAnalytics";

@TestFixture("Testing BinaryLevenshteinAnalytics")
export class BinaryLevenshteinAnalyticsSpec {

	@TestCase({_id: 1, content: "IPhoNe EXPLOIT bei APPLE?"},
		{_id: 2, content: "IPhoNe EXPLOIT bei APPLE?"}, 0.99) //Same titles should lead to almost "1"
	@Test("Cosine moreThan correlationFactor .analyze")
	public testMoreAnalyze(obj1: DataObject, obj2: DataObject, expected: number) {
		const analytics = new BinaryLevenshteinAnalytics();
		analytics.prepare([obj1, obj2]);
		Expect(analytics.analyze(obj1, obj2).correlationFactor).toBeGreaterThan(expected);
		analytics.finish();
	}

	@TestCase({_id: 3, content: "IPhoNe?"},
		{_id: 4, content: "IPhoNe EXPLOIT bei APPLE?"}, 0.5) //Same but just 1 Keyword (no links) should be less than 0.5
	@TestCase({_id: 5, content: "IPhoNe EXPLOIT bei APPLE?"},
		{_id: 6, content: "OMG WTF! SAMsung! LG? in AndroiD!"}, 0.01) //Totally different keywords should lead to a "0"
	@TestCase({_id: 7, content: "nothing in here"},
		{_id: 8, content: "text without keywords"}, 0.05) //Totally different keywords should lead to a "0"
	@Test("Cosine lessThan correlationFactor .analyze")
	public testLessAnalyze(obj1: DataObject, obj2: DataObject, lessThan: number) {
		const analytics = new BinaryLevenshteinAnalytics();
		analytics.prepare([obj1, obj2]);
		Expect(analytics.analyze(obj1, obj2).correlationFactor).toBeLessThan(lessThan);
		analytics.finish();
	}
}

