/**
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


import {AsyncTest, TestFixture, Expect, Timeout, TestCase} from "alsatian";
import {RSSFeedProvider} from "../../main/dataProviders/rssfeedProvider";

@TestFixture('Testing feed Provider')
export class TestRSSFeedProvider {

    @TestCase("https://threatpost.com/feed/")
    @TestCase("https://www.grahamcluley.com/feed/")
    @AsyncTest('.getData RSS')
    @Timeout(5000) // May take five seconds
    public async testGetFeed(feed: string) {
        const feeds = await RSSFeedProvider.instance.getContext(feed)();

        // The feed should not be empty
        Expect(feeds).not.toBeEmpty();
    }

    @TestCase("www.google.de") // not an RSS feed
    @AsyncTest(".getData(FAIL)")
    @Timeout(2000)
    public async testGetFeedFail(feed: string) {
        await Expect(() => RSSFeedProvider.instance.getData(feed)).toThrowAsync();
    }

}