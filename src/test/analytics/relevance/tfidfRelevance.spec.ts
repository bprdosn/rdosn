/**
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import {TfidfRelevance} from "../../../main/analytics/relevance/tfidfRelevance";
import {Setup, Test, TestFixture, Expect} from "alsatian";
import {PorterStemmer, WordTokenizer} from "natural";
import {DataObject} from "../../../main/dataProviders/dataObjects/dataObject";

const sw = require("stopword");

@TestFixture("Testing Text Analysis")
export class TfIdfRelevanceSpec {

	private tfidf: TfidfRelevance<DataObject> = new TfidfRelevance<DataObject>();
	private keywords = ["iphone", "ios", "mac", "macOS", "sierra", "apple", "android", "aws", "bug",
		"samsung", "huawei", "lg", "windows", "windows10", "microsoft", "symantec", "pgp",
		"security", "exploit", "phishing", "encryption", "rest", "google", "hardware",
		"eu", "usa", "germany", "russia", "play", "login", "api", "nest", "raspberry",
		"pi", "watch", "vulnerability", "insecure", "vpn", "https", "facebook", "twitter",
		"reddit", "linux", "aws", "malware", "worm", "ransomware", "wannacry", "cisco", "firewall", "remote", "code", "execution", "shellcode", "injection", "attack", "escape"].map(item => PorterStemmer.stem(item.toLowerCase()));
	private documents = [
		"RIP CERT.org – You Will Be Missed",
		"Shellen - Interactive shellcoding environment to easily craft shellcodes",
		"Flash allows remote code execution",
		"Trustico's website was vulnerable to a trivial shell command substitution injection, running as uid=0",
		"New SMBv3 DoS exploit for Windows 8.1 & Windows Server 2012",
		"Samba v3 exploit for Windows and Windows Server found",
		"CVE-2018-4087 PoC: Escaping the iOS sandbox by misleading bluetoothd",
		"SGX SPectre Attacks: Leaking Enclave Secrets via Speculative Execution",
		"new Spectre exploits with SGX execute speculative",
	].map((item) => sw.removeStopwords(new WordTokenizer().tokenize(item.toLowerCase())).map((word: string) => (PorterStemmer.stem(word))));

	@Setup
	public setup() {
		this.documents.forEach(item => this.tfidf.addDocument(item));
	}


	@Test("relevance.tfIdf")
	public testTfIdf() {
		Expect(this.tfidf.tfIdfAvg(this.documents[0], this.keywords)).toEqual(0);
		Expect(this.tfidf.tfIdfVector(this.documents[0], this.keywords)).toEqual(Array(this.keywords.length).fill(0));
		Expect(this.tfidf.tfIdfAvg(this.documents[1], this.keywords)).toBeGreaterThan(0);
		Expect(this.tfidf.tfIdfVector(this.documents[1], this.keywords)).toBeDefined();
		Expect(this.tfidf.tfIdfAvg(this.documents[2], this.keywords)).toBeGreaterThan(0);
		Expect(this.tfidf.tfIdfVector(this.documents[2], this.keywords)).toBeDefined();
	}
}
