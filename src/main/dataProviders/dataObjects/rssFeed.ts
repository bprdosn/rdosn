/**
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


import {DataObject} from "./dataObject";
import * as moment from "moment";
import {orDefault} from "../../util/orDefault";
import {ProviderInfo} from "./providerInfo";

// This class should represent the relevant attributes of a single feed
export class RssFeed extends DataObject {
    // Basic attributes
    public readonly creator: string;
    public readonly categories: Array<string>;
    public readonly language: string;

    /**
     * Creates new rssFeed object
     * @param {*} rssFeed feed returned from an rss feed page
     * @param {string} source name of the RSS feed
     */
    public constructor(rssFeed: any, source: string) {
        const id = "RS" + orDefault(
            rssFeed.guid,
            rssFeed.link + moment(rssFeed.pubDate).format("YYYYMMDDHHmmss" )
        );

        super(id , rssFeed.title, rssFeed.content, moment(rssFeed.pubDate), [rssFeed.link], rssFeed.link, new ProviderInfo("RSS", source));

        this.creator = rssFeed.creator;
        this.categories = rssFeed.categories;
        this.language = rssFeed.language;
    }

    /**
     * Provides an interface for the popularity calculations
     * In this case, there are no popularity attributes - so 0.75 is returned
     * @returns {*} list of attributes that are needed for the popularity calculations, with weight being between 0 and 1 and all weights adding up to 1
     */
    public get popularityAttributes(): Array<{ value: number, weight: number }> {
        return [{ value: 0.75, weight: 1 }];
    }
}