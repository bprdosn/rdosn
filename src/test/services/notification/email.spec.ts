/**
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import {TestFixture, TestCase, Expect, AsyncTest, Timeout} from "alsatian";
import {Email} from "../../../main/services/notification/email";

@TestFixture("Testing Email")
export class EmailSpec {

    @TestCase( "bp.rdons@gmail.com")
    @TestCase("DarlingBobster@mailinator.com")
    @TestCase("HeadyPaladin@mailinator.com")
    @AsyncTest(".getMail")
    @Timeout(4000)
    public async testSendMail(receiver: string) {
        const handle = Email.instance.getHandle(receiver);
        await Expect(async () => await handle("Test", "Test")).not.toThrowAsync();
    }

    @TestCase( "bp.rdons")
    @TestCase( "bulasdfasdfaf.de")
    @AsyncTest(".getMail (FAIL)")
    @Timeout(2000)
    public async testSendMailFail(receiver: string) {
        const handle = Email.instance.getHandle(receiver);
        await Expect(async () => await handle("Test", "Test")).toThrowAsync();
    }
}