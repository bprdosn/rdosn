/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {DataObject} from "../../../../main/dataProviders/dataObjects/dataObject";
import {Expect, Test, TestCase, TestFixture} from "alsatian";
import {BinaryLinkAnalytics} from "../../../../main/analytics/correlation/analytics/binaryLinkAnalytics";
import {Reason} from "../../../../main/analytics/correlation/reason/reason";
import {Correlation} from "../../../../main/analytics/correlation/correlation";

@TestFixture("Testing BinaryLinkAnalytics")
export class BinaryLinkAnalyticsSpec {

    @TestCase({ _id: "1", links: ["www.example.com", "www.exampleAnother.com"] }, { _id: "2", links: ["www.example.com", "www.exampleAnother.com"]}, 0.99)
    @TestCase({ _id: "3", links: ["www.example.com"] }, { _id: "4", links: ["www.example.com"]}, 0.99) // Same links
    @TestCase({ _id: "5", links: ["www.example.com"] }, { _id: "6", links: ["www.example.com", "www.someLink.com"]}, 0.7) // one of two links are the same
    @Test("moreThan correlationFactor .analyze")
    public testMoreAnalyze(obj1: DataObject, obj2: DataObject, expected: number) {
        Expect(BinaryLinkAnalyticsSpec.getCorrelation(obj1, obj2).correlationFactor).toBeGreaterThan(expected);
    }

    @TestCase({ _id: "id1", links: ["www.example1.com"] }, { _id: "id2", links: ["www.example2.com"]}, 0.01) // Different Links
    @Test("lessThan correlationFactor .analyze")
    public testLessAnalyze(obj1: DataObject, obj2: DataObject, expected: number) {
        Expect(BinaryLinkAnalyticsSpec.getCorrelation(obj1, obj2).correlationFactor).toBeLessThan(expected);
    }

    @TestCase({ _id: "1", links: ["www.example.com", "www.exampleAnother.com"] }, { _id: "2", links: ["www.example.com", "www.exampleAnother.com"] }, ["www.example.com", "www.exampleAnother.com"])
    @TestCase({ _id: "3", links: ["www.example.com"] }, { _id: "4", links: ["www.example.com", "www.exampleAnother.com"] }, ["www.example.com"])
    @TestCase({ _id: "5", links: ["www.example.com"] }, { _id: "6", links: ["www.exampleAnother.com"] }, [])
    @TestCase({ _id: "7", links: ["www.example.com", "www.newExample.de"] }, { _id: "8", links: ["www.exampleAnother.com", "www.newExample.de"] }, ["www.newExample.de"])
    @Test("reason .analyze")
    public testReasonAnalyze(obj1: DataObject, obj2: DataObject, expected: Array<string>) {
        const result = BinaryLinkAnalyticsSpec.getCorrelation(obj1, obj2);
        const reasons: Array<Reason> = Object.keys(result.reasons).map(type => result.reasons[type]);
        Expect(reasons.map(s => s.toString()).toString()).toBe(expected.toString());
    }

    private static getCorrelation (obj1: DataObject, obj2: DataObject): Correlation {
        let binaryLinkAnalytics = new BinaryLinkAnalytics();
        binaryLinkAnalytics.prepare([obj1, obj2]);
        const result = binaryLinkAnalytics.analyze(obj1, obj2);
        binaryLinkAnalytics.finish();
        return result;
    }
}
