/*
 The MIT License (MIT)

 Copyright (c) 2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {FacebookProvider} from "../dataProviders/facebookProvider";
import {TwitterProvider} from "../dataProviders/twitterProvider";
import {RedditProvider} from "../dataProviders/redditProvider";
import {RSSFeedProvider} from "../dataProviders/rssfeedProvider";
import {HackerNewsProvider} from "../dataProviders/hackerNewsProvider";
import {Source, SourceType} from "../services/db/source";
import {DataProvider, ProviderContext} from "../dataProviders/dataProvider";
import {DataObject} from "../dataProviders/dataObjects/dataObject";
import {orThrow} from "../util/orDefault";
import {IgnoreErrorProvider} from "../dataProviders/decorators/ignoreErrorProvider";
import {AddPopularity} from "../dataProviders/decorators/addPopularity";
import {RetryProvider} from "../dataProviders/decorators/retryProvider";
import {TimeoutProvider} from "../dataProviders/decorators/timeoutProvider";
import {XforceProvider} from "../dataProviders/xforceProvider";
import {AddTfIdfRelevance} from "../dataProviders/decorators/addTfIdfRelevance";

export class ProviderManager implements DataProvider<DataObject, Source> {

    public static readonly instance: ProviderManager = new ProviderManager();

    private providers: Map<SourceType, DataProvider<DataObject, any>> = new Map();

    private constructor() {}

    /* eslint-disable valid-jsdoc */
    /**
     * Register a new provider so that it is available to use
     * @param {string} type of provider to register, string is used as a key in the map
     * @param {DataProvider<DataObject, *>} provider to register
     */
    public registerProvider(type: string, provider: DataProvider<DataObject, any>) {
        if(this.providers.get(type))
            throw new TypeError("Can't register provider with a already used key!");

        this.providers.set(type, ProviderManager.addDecorators(provider));
    }

    /**
     * @param {Source} src to get provider for
     * @returns {ProviderContext<D extends DataObject>} appropriate provider for given source
     */
    public getContext<D extends DataObject, O>(src: Source): ProviderContext<D> {
        const provider = orThrow(this.providers.get(src.type),
                                 "Missing Provider Context for type " + src.type) as DataProvider<D, O>;

        return provider.getContext(src.options);
    }

    // decorators are added right when they are registered, so no need to wrap all the different parameters
    private static addDecorators<T extends DataObject, O>(provider: DataProvider<T, O>): DataProvider<T, O> {
        return new AddTfIdfRelevance(new AddPopularity(new IgnoreErrorProvider(new TimeoutProvider(new RetryProvider(provider)))));
    }
}

// this might be loaded from a config file
ProviderManager.instance.registerProvider("Facebook", FacebookProvider.instance);
ProviderManager.instance.registerProvider("HackerNews", HackerNewsProvider.instance);
ProviderManager.instance.registerProvider("Reddit", RedditProvider.instance);
ProviderManager.instance.registerProvider("RSSFeed", RSSFeedProvider.instance);
ProviderManager.instance.registerProvider("Twitter", TwitterProvider.instance);
ProviderManager.instance.registerProvider("XForce", XforceProvider.instance);