import {Test, TestFixture, Expect} from "alsatian";
import {ProviderManager} from "../../main/config/providerManager";
import {Source} from "../../main/services/db/source";
import {DataProvider} from "../../main/dataProviders/dataProvider";
import {DataObject} from "../../main/dataProviders/dataObjects/dataObject";

@TestFixture("Testing ProviderManager")
export class ProviderManagerSpec {

    private readonly dummyProvider: DataProvider<DataObject, void> = {
        getContext: () => { return async () => []; } };

    @Test("all")
    public testAll() {
        Expect(() => ProviderManager.instance.getContext(new Source("test", {}))).toThrow();

        ProviderManager.instance.registerProvider("test", this.dummyProvider);
        Expect(() => ProviderManager.instance.getContext(new Source("test", {}))).not.toThrow();
        Expect(() => ProviderManager.instance.registerProvider("test", this.dummyProvider)).toThrow();
    }
}