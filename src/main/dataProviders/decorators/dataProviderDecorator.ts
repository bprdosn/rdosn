/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {DataObject} from "../dataObjects/dataObject";
import {DataProvider, ProviderContext} from "../dataProvider";

/**
 * Allows subclasses to decorate the results from a data provider
 * Also allows going from type T to type S
 */
export abstract class DataProviderDecorator<T extends DataObject, O, S extends DataObject = T> implements DataProvider<S, O> {

    public constructor(protected readonly provider: DataProvider<T, O>) {}

    public getContext(options: O): ProviderContext<S> {
        const context = this.provider.getContext(options);
        return () => this.decorate(context, options);
    }

    /**
     * Wraps the context call
     * @param {ProviderContext<T extends DataObject>} context of the provider wrapped
     * @return {Promise<T[]>} the decorated result from the original provider
     */
    protected abstract decorate(context: ProviderContext<T>, options: O): Promise<S[]>
}