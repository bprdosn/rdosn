/**
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import {Expect, Test, TestCase, TestFixture} from "alsatian";
import {User, UserIdentity} from "../../../main/services/db/user";
import {BasicCorrelation} from "../../../main/analytics/correlation/basicCorrelation";
import * as moment from "moment";
import {Keywords} from "../../../main/analytics/correlation/reason/keywords";
import {MinDataObject} from "../../dataProviders/dataObjects/minDataObject";

@TestFixture("Testing User")
export class UserSpec {

    @TestCase({ provider: "facebook", id: "43252523fsasf" })
    @TestCase({ provider: "google", id: "58678dfg5" })
    @Test(".getID")
    public testID(identity: UserIdentity) {
        Expect(User.getID(identity)).toEqual(new User(identity, "")._id);
    }

    @Test("seenCorrelations")
    public testSeenCorrelations() {
        const user = new User({ provider: "facebook", id: "43252523fsasf" }, "", true);

        const c = new BasicCorrelation([new MinDataObject("asd", "asd", "asdasd", moment(), [], ""),
                                  new MinDataObject("asdasd", "asdasdf", "asdsafdasasd", moment(), [], "")], 0, Keywords(["someKeyword"], 0));
        Expect(user.hasSeen(c)).not.toBeTruthy();

        user.addSeenCorrelations([c]);

        Expect(user.hasSeen(c)).toBeTruthy();
    }

    @Test(".intervalToUnit")
    public testIntervalToUnit() {

        const mangle: (a: any) => any = (a) => a;

        Expect(User.intervalToUnit("HOURLY")).toBe("hours");
        Expect(User.intervalToUnit("DAILY")).toBe("days");
        Expect(User.intervalToUnit("WEEKLY")).toBe("weeks");
        Expect(User.intervalToUnit(mangle("GIBBERISH"))).toBe("hours");


    }
}