/*
 The MIT License (MIT)

 Copyright (c) 2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {Page} from "../page";
import {isNullOrUndefined} from "util";
import {getLogger} from "log4js";
import {reportsDB} from "../../../main/services/db/dbs";
import {Correlation} from "../../../main/analytics/correlation/correlation";
import {DataObject} from "../../../main/dataProviders/dataObjects/dataObject";
import {formatDate} from "../../../main/util/formatDate";
import {orThrow} from "../../../main/util/orDefault";
import * as moment from "moment";

const logger = getLogger("correlation page");


export class CorrelationPage extends Page {

	public static readonly instance = new CorrelationPage();

	public readonly route: string = "/report/:reportId/:correlationId";
	public readonly requiresAuthentication: boolean = true;

	private constructor() {
		super();
	}

	public getHandler(req: any, res: any) {
		const reportId =  req.params.reportId;
		const correlationId =  req.params.correlationId;
		if (isNullOrUndefined(reportId) || isNullOrUndefined(correlationId)) {
			logger.error("Params are undefined!");
			res.render("error/error");
			return;
		}

		this.getCorrelation(reportId, correlationId)
			.then(c => {
				c.dataObjects.sort((a,b)=> {
					const scoreA: number =
						orThrow(a.popularity, "Missing popularity for " + a._id)
						* orThrow(a.relevance, "Missing relevance for " + a._id);
					const scoreB: number =
						orThrow(b.popularity, "Missing popularity for " + b._id)
						* orThrow(b.relevance, "Missing relevance for " + b._id);
					return scoreB - scoreA;
				});
				res.render("correlation/correlation", {
					correlation: c,
					bestDataObject: c.dataObjects[0],
					dataObjectComparer: (a: DataObject, b: DataObject) => {
						const isBefore = moment(a.created_at).isAfter(moment(b.created_at));
						return isBefore ? 1 : -1;
					},
					formatDate: formatDate
				});
			})
			.catch(e => {
				logger.error("Error getting correlation", e);
				res.render("error/error");
			});

	}

	private async getCorrelation(reportId: string, correlationId: string): Promise<Correlation> {

		const report = await reportsDB().get(reportId);

		const correlation = report.correlations.find(c => c.id == correlationId);

		if (isNullOrUndefined(correlation)) {
			throw new Error("Undefined correlation");
		}

		// try to expand data objects
		return correlation.getExpandedVersion();

	}

}