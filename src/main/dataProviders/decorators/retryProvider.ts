/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {ProviderContext} from "../dataProvider";
import {DataObject} from "../dataObjects/dataObject";
import {getLogger} from "log4js";
import {DataProviderDecorator} from "./dataProviderDecorator";
import {retry} from "../../util/retry";

const logger = getLogger("retry provider");

/*
 * Retries the provider up to MAX_RETRIES times before giving up an throwing an exception.
 * Also logs a warning when multiple tries where needed.
 */
export class RetryProvider<T extends DataObject, O> extends DataProviderDecorator<T, O> {
    public static readonly MAX_RETRIES: number = 2;

    protected async decorate(context: ProviderContext<T>): Promise<T[]> {
        return await retry(context, RetryProvider.MAX_RETRIES, 0, logger);
    }
}