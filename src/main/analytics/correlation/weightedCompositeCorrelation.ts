/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {Correlation} from "./correlation";
import {DataObject} from "../../dataProviders/dataObjects/dataObject";
import {Reason} from "./reason/reason";
import {isNullOrUndefined} from "util";
import {flatMap, sum, unique} from "../../util/arrayUtil";
import {BasicCorrelation} from "./basicCorrelation";
import {StringMap} from "../../util/stringMap";
import {WeightedCorrelation} from "./weightedCorrelation";

/**
 * Unifies multiple correlations into one and applies weights to them
 */
export class WeightedCompositeCorrelation extends BasicCorrelation {


    /**
     * constructs a Correlation based on an array of weighted correlations
     * @param {WeightedCorrelation[]} children weighted correlations
     */
    public constructor(children: Array<WeightedCorrelation>) {
        super(
            WeightedCompositeCorrelation.uniqueDataObjects(children),
            WeightedCompositeCorrelation.calcCorrelationFactor(children),
            WeightedCompositeCorrelation.calcReasons(children)
        );
    }

    /**
     * calculates the unique DataObjects of every child
     * @param {Array<WeightedCorrelation>} children provide data objects
     * @returns {Array<DataObject>} Array with unique DataObjects
     */
    private static uniqueDataObjects(children: Array<Correlation>): Array<DataObject> {
        //TODO: might be performance bottleneck -> replace with Set and add each data object?
        return unique(flatMap(children, (child) => child.dataObjects));
    }

    /**
     * calculates the correlationFactor based on the weights
     * @param {Array<WeightedCorrelation>} children provide factors
     * @returns {number} weighted correlationFactor
     */
    private static calcCorrelationFactor(children: Array<WeightedCorrelation>): number {
        const sumOfWeights = sum(children.map(child => child.weight));
        if (sumOfWeights === 0) return 0;
        const factorSum = sum(children.map(child => child.totalCorrelationFactor()));
        return factorSum / sumOfWeights;
    }

    private static calcReasons(children: Array<Correlation>): StringMap<Reason> {
        const result: StringMap<Reason> = {};

        for (const child of children) {
            for (const [type, reason] of Object.entries(child.reasons)) {

                const storedReason = result[type];

                if (isNullOrUndefined(storedReason)) {
                    // not existing yet
                    result[type] = reason;
                } else {
                    // merge
                    result[type] = storedReason.combine(reason);
                }
            }
        }

        return result;
    }

}