/**
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
import {getLogger} from "log4js";
import {isNullOrUndefined} from "util";
import {env} from "../main/config/env";
import {Page} from "./pages/page";
import {IndexPage} from "./pages/index";
import {HomePage} from "./pages/home/home";
import {SettingsPage} from "./pages/settings/settings";
import {RedirectReport, ReportPage} from "./pages/report/report";
import {CorrelationPage} from "./pages/correlation/correlation";
import * as express from "express";
import * as session from "express-session";
import * as passport from "passport";
import * as helmet from "helmet";
import expressEnforcesSsl = require("express-enforces-ssl");

const cfEnv = require("cfenv");
const WebAppStrategy = require("bluemix-appid").WebAppStrategy;
const UserAttributeManager = require("bluemix-appid").UserAttributeManager;

const logger = getLogger("server");

// local turns off security (and obviously starts the server even if not on cloud)
const runLocal = env().LOCAL_SERVER === "true";

export class Server {
    private static readonly LOGIN_URL = "/ibm/bluemix/appid/login";
    private static readonly CALLBACK_URL = "/ibm/bluemix/appid/callback";
    private static readonly PORT = env().PORT || 3000;

    private static app: express.Express = express();

    private static pages: Array<Page> = [];

    /**
     * Initializes and runs the server
     * If executed in the cloud, the Server will listen on the port specified above
     * Otherwise, it will run in a local mode
     * @returns {*} nothing
     */
    public static start() {
        if(cfEnv.getAppEnv().isLocal) {
            if(!runLocal) {
                logger.info("Detected Local Environment. Webserver will not start...");
                return;
            } else
                logger.info("Detected Local Environment. Webserver will start in local mode...");
        } else
            logger.info("Detected Server Environment. Starting server...");

        if (!runLocal) Server.initSecurityConfig();
        Server.initExpress();
        if (!runLocal) Server.initAuthentication();
        Server.initRoutes();

        // start the server
        Server.app.listen(Server.PORT, () =>
            logger.info("Listening on Port " + Server.PORT));
    }

    /**
     * Registers a new page to the server
     * @param {Page} page to register
     * @returns {*} nothing
     */
    public static registerPage(page: Page) {
        if(!this.pages.find((p) => p.route.valueOf() == page.route))
            this.pages.push(page);
        else
            logger.warn("Tried to register page with a route already occupied by another page." +
                "Aborted registration. Route:", page.route);
    }

    private static initSecurityConfig() {
        Server.app.use(helmet());
        Server.app.use(helmet.noCache());

        Server.app.enable("trust proxy");
        Server.app.use(expressEnforcesSsl());
    }

    private static initExpress() {
        const secret = env().EXPRESS_SESSION_SECRET;
        if(isNullOrUndefined(secret))
            throw new TypeError("Missing Secret for Express Session!");

        // Configure session
        Server.app.use(session({
            secret: secret,
            resave: true,
            saveUninitialized: true,
            proxy: true,
            cookie: {
                httpOnly: true,
                secure: true
            }
        }));

        // Use .ejs
        Server.app.set("view engine", "ejs");
        Server.app.set("views", __dirname + "/pages/");
        Server.app.use("/public", express.static("public"));

        // Init encoding
        Server.app.use(express.urlencoded({ extended: true }));
        Server.app.use(express.json());
    }

    private static initAuthentication() {
        // Configure express to use passportjs
        Server.app.use(passport.initialize());
        Server.app.use(passport.session());

        passport.use(new WebAppStrategy());

        // Initialize the user attribute Manager
        UserAttributeManager.init();

        // Configure passportjs with user serialization/deserialization
        // For persistent, authenticated sessions across multiple requests
        passport.serializeUser((user: any, cb: any) => cb(null, user));
        passport.deserializeUser((id: any, cb: any) => cb(null, id));
    }

    private static initRoutes() {
        // Explicit login endpoint
        Server.app.get(Server.LOGIN_URL, passport.authenticate(WebAppStrategy.STRATEGY_NAME));

        // Callback to finish the authorization process
        Server.app.get(Server.CALLBACK_URL, passport.authenticate(WebAppStrategy.STRATEGY_NAME));

        Server.pages.forEach((page) => {
            if (!runLocal && page.requiresAuthentication) {
                // Protected area. If current user is not authenticated - a redirect to the login widget will be returned.
                const auth = passport.authenticate(WebAppStrategy.STRATEGY_NAME,
                                                   { successRedirect: page.route });

                Server.app.get(page.route, auth, page.getHandler.bind(page));
                Server.app.post(page.route, auth, page.postHandler.bind(page));
            } else {
                Server.app.get(page.route, page.getHandler.bind(page));
                Server.app.post(page.route, page.postHandler.bind(page));
            }
        });
    }
}

Server.registerPage(IndexPage.instance);
Server.registerPage(HomePage.instance);
Server.registerPage(SettingsPage.instance);
Server.registerPage(ReportPage.instance);
Server.registerPage(RedirectReport.instance);
Server.registerPage(CorrelationPage.instance);