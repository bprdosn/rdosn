/**
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


/*import {AsyncTest, TestFixture, Expect, Timeout, TestCase} from "alsatian";
import {HTMLProvider} from "../../main/dataProviders/htmlProvider";
import {HTML} from "../../main/dataProviders/dataObjects/html";

@TestFixture("Testing HTML Provider")
export class HTMLProviderSpec {
    private readonly provider: HTMLProvider = new HTMLProvider();

    @TestCase("http://www.google.com")
    @TestCase("http://www.amazon.com")
    @TestCase("https://www.npmjs.com")
    @TestCase("https://www.", false)
    @AsyncTest(".getWebsiteHTML")
    @Timeout(5000) // May take five seconds
    public async testGetWebsiteHTML(site: string, shouldBeReachable: boolean = true) {
        try {
            let html: HTML = await this.provider.getWebsiteHTML(site);

            // If it is not reachable, a error must be thrown
            Expect(shouldBeReachable).toBeTruthy();

            // The html should have quite a length
            Expect(html.content_short).not.toBeNull();
            Expect(html.content_long.length).toBeGreaterThan(100);

            // The html should start and end with html tags
            Expect(html.content_long.startsWith("<html") || html.content_long.startsWith("<!doctype html>")).toBeTruthy();
            Expect(html.content_long.endsWith("html>") || html.content_long.endsWith("-->")).toBeTruthy();
        }
        catch(e) {
            // The Promise should only be rejected when the link is unreachable
            if(shouldBeReachable)
                Expect(e).toBeNull();
        }
    }
}*/
