/*
 The MIT License (MIT)

 Copyright (c) 2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {Page} from "../page";
import {reportsDB} from "../../../main/services/db/dbs";
import {getLogger} from "log4js";
import {isNullOrUndefined} from "util";
import {formatDate, formatMoment} from "../../../main/util/formatDate";

const logger = getLogger("report");

export class ReportPage extends Page {

	public static readonly instance = new ReportPage();

	public readonly route = "/report/:reportId";
	public readonly requiresAuthentication = true;

	private constructor() {
		super();
	}

	public getHandler(req: any, res: any) {
		const id = req.params.reportId;
		if (isNullOrUndefined(id)) {
			logger.error("Param is undefined!");
			res.render("error/error");
			return;
		}

		reportsDB().get(id)
			.then(report => {
				res.render("report/report", {
					dateString: formatMoment(report.createdAt),
					report: report,
				});
			})
			.catch(e => {
				logger.error("Error getting report", e);
				res.render("error/error");
			});
	}

}

export class RedirectReport extends Page {

	public static readonly instance = new RedirectReport();

	public readonly route = "/report";
	public readonly requiresAuthentication = true;

	private constructor() {
		super();
	}

	public getHandler(req: any, res: any) {
		reportsDB().getLastReport()
			.then(report => res.redirect("/report/" + report._id))
			.catch(e => {
				logger.error(e);
				res.render("error/error");
			});
	}

}