/**
 The MIT License (MIT)

 Copyright (c) 2017 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import {DataProvider, ProviderContext} from "../../dataProviders/dataProvider";
import {DataObject} from "../../dataProviders/dataObjects/dataObject";
import {ProviderManager} from "../../config/providerManager";

export type SourceType = string;

export class Source implements DataProvider<DataObject, void> {

    // Don't know how useful it is to keep the id here and force a uniform id schema
    public readonly _id: string;

    public constructor(public readonly type: SourceType, public readonly options: any) {
        this._id = this.type + "_" + JSON.stringify(this.options);
    }

    public getContext<T extends DataObject>(): ProviderContext<T> {
        return ProviderManager.instance.getContext(this);
    }
}