How to Contribute
=================

All development tasks are organized on our [Trello board](https://trello.com/b/fYzoEMXW) (can also be found in the sidebar under 'Boards').

To integrate bitbucket on the trello board, go to the [Trello board](https://trello.com/b/fYzoEMXW), click on 'Bitbucket' in the top right and finally 'Connect to Bitbucket'.

Workflow:
---------

**Membership** As soon as you start working/reviewing a task, add yourself as a member to the given task. Never remove members from a task.

**Branching** If you start working on a new feature, create a new branch named after the User Story ID and assign it to the trello task. If the feature does not belong to a UserStory, choose a sensible name. Once you think the feature is finished and all potential merge conflicts have been resolved, create a Pull Request via Bitbucket and attach it to the trello task. Pushing to the master branch directly is disabled. See below where to move the card on trello in progress of your work.

**Reviewing** Don't review cards, where you participated yourself. If problems during the review occur, move the task to "Rejected" and add a note to the checklist as well as a comment informing the other members about the problems. If the review is successful, add a "Reviewed" comment, move the task to "Done" and note the corresponding iteration in the excel document. Then accept the pull request and close the corresponding feature branch while squashing the commits.

**Testing** For each feature a sufficient amount of tests must be written and 95% code coverage (based on functions) is to be achieved. For each source file there must be a corresponding test file.

**Communication** Communication is handled via slack, however all discussions regarding a task take place on trello. They may be linked to a slack channel via trello bot

**Credentials** Credentials must be saved in the .env file, which should never be accesable for non-project members. If you add new entries, make sure each team member as well as the bitbucket pipeline ("Settings -> Pipelines -> Environment Variables") get the changes as well.

**License** In each file the license template must be placed at top.

The lists have the following meaning:
-------------------------------------

**Backlog** This is the place all new tasks / user stories / bugs start.

**Blocked** If a task can not be worked on because something else needs to happen first, place it here, add a reason why and link the blocking trello task.

**Current Iteration** This is the list of things planned for the current iteration. This list will be filled at the start of an iteration.

**In Progress** When working on a task, please place in this list. If you stop working on a task, move it back to "Current Iteration" and add a description of what has been done if this is not clear from the commit messages alone. 

**Rejected** If a task was rejected during a review, it is placed here.

**Ready for review** When finished with your task move it here until someone reviews it and decides it can be moved to done. For details on how to review a task, take a look at the [Workflow Section](#Workflow).

**Done** A place for all finished tasks. We will have to see if we need to archive the list after a while.

**Misc** For short lived ideas, unfinished task descriptions, etc.

Please also use the following labels:
-------------------------------------

**User story** For user stories also include the story points and a unique identifier in the form of '(Story Points) ID: Story description', e.g. '(2) U1: As a user I want to ...'

**Bug** For code that is broken and needs to be fixed.

**Task** For work which is neither related to a user story nor a bug.


You can of course create your own label if none fit.

Release:
--------

**Release Branch** Everything on the release branch gets updated and started via the [Bluemix toolchain](https://console.bluemix.net/devops/toolchains?env_id=ibm:yp:us-south). Tag the releases accordingly.

**Environment variables** To add custom environment variables to bluemix go on the [Bluemix dashboard](https://console.bluemix.net/dashboard/apps) and select US South as Region. Click on the app name > Runtime (on the left) > Environment variables. You can add new ones in the User defined section.

Setup local environment
--------------------------------------

**Webstorm** _File_ -> _new_ -> _Edit File Templates..._ -> _Add_:
Copy text from [LICENSE.md](LICENSE.md) and paste as template text between "__/** */__". Set _Extension_ to "ts"
