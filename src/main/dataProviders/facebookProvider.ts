/**
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import {ProviderContext, DataProvider} from "./dataProvider";
import {FacebookPost} from "./dataObjects/facebookPost";
import {LinkResolver} from "../analytics/linkResolver";
import {PopularityRelevance} from "../analytics/relevance/popularityRelevance";
import {getLogger} from "log4js";
import {env} from "../config/env";
import {orThrow} from "../util/orDefault";
let FB = require("fb");

const logger = getLogger("provider.facebook");

// All communication with the Facebook API should be handled by this class
export class FacebookProvider implements DataProvider<FacebookPost, string> {
    private static _instance: FacebookProvider;

    private appSecret: string;
    private appID: string;

    public readonly relevance: PopularityRelevance<FacebookPost> = new PopularityRelevance<FacebookPost>();

    /**
     *  Initialises the Facebook client
     */
    public constructor() {
        this.appID = orThrow(env().FACEBOOK_APP_ID, "FACEBOOK_APP_ID in .env is undefined!");
        this.appSecret = orThrow(env().FACEBOOK_APP_SECRET, "FACEBOOK_APP_SECRET in .env is undefined!");

        new FB.Facebook({ appId: this.appID, appSecret: this.appSecret });
    }

    public static get instance(): FacebookProvider  {
        if(!FacebookProvider._instance)
            FacebookProvider._instance = new FacebookProvider();

        return FacebookProvider._instance;
    }

    /**
     * Returns an array of FacebookPosts of the last Facebook posts of a page given the tag of the page
     * @param {string} pageTag tag of the facebook page which can be found in the url of the facebook page
     * @returns {Promise<Array<FacebookPost>>} array of FacebookPosts posted on the page wrapped in a promise
     */
    public async getData(pageTag: string): Promise<Array<FacebookPost>>{
        logger.trace("Getting recent posts");
        await this.updateAccessToken();
        const ids: Array<string> = await this.getRecentPostIds(pageTag);

        return await Promise.all(ids.map( async (id) => await this.getPost(id, pageTag)));
    }

    /**
     * @param {string} pageTag site to pull data from
     * @returns {ProviderContext} use this and pull data via context.fetchData
     */
    public getContext(pageTag: string): ProviderContext<FacebookPost> {
        return () => this.getData(pageTag);
    }

    /**
     * Returns a new FacebookPost given the id of the post
     * @param {string} id id of a Facebook post
     * @param {string} user display name of the user
     * @returns {Promise<FacebookPost>} FacebookPost with the same id as attribute as the given id wrapped in a promise
     */
    private async getPost(id: string, user: string): Promise<FacebookPost>{
        const res = await FB.api(id + "?fields=link,message,created_time,description");

        // resolve potential link
        const resolvedLink = await LinkResolver.resolveLink(res.link);

        // Until Facebook permissions are sufficient, hard set likes and shares
        res.likes = 500;
        res.shares = 100;

        return new FacebookPost(res, [resolvedLink], user);
    }

    /**
     * Gets the 25 recent post ids of a facebook page
     * @param {string} pageTag tag of the facebook page which can be found in the url of the facebook page
     * @returns {Promise<Array<string>>} Array of post ids wrapped in a promise
     */
    private async getRecentPostIds(pageTag: string): Promise<Array<string>>{
        const res = await FB.api(pageTag + "/posts");

        return res.data.map((val: any) => val.id);
    }

    /**
     * Gets a new access token by requesting Facebook Graph API
     * and updates the access token of this client
     * @returns {Promise<string>} the updated access token wrapped in a promise
     */
    private async updateAccessToken(): Promise<string> {
        logger.trace("Updating access token");
        const res = await FB.api("oauth/access_token", {
            client_id: this.appID,
            client_secret: this.appSecret,
            grant_type: "client_credentials"});

        FB.setAccessToken(res.access_token);
        return res.access_token;
    }
}

