/*
 The MIT License (MIT)

 Copyright (c) 2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {AsyncTest, Expect, SetupFixture, Test, TestFixture, Timeout} from "alsatian";
import {MinimalReport, Report} from "../../../main/services/db/report";
import * as moment from "moment";
import {Correlation} from "../../../main/analytics/correlation/correlation";
import {BasicCorrelation} from "../../../main/analytics/correlation/basicCorrelation";
import {MinDataObject} from "../../dataProviders/dataObjects/minDataObject";
import {Reason} from "../../../main/analytics/correlation/reason/reason";
import {dataObjectsDB} from "../../../main/services/db/dbs";

@TestFixture("report")
export class ReportSpec {

	private report!: Report;

	@SetupFixture
	public setup() {
		const d1 = new MinDataObject("d1");
		const d2 = new MinDataObject("d2");
		const d3 = new MinDataObject("d3");
		const d4 = new MinDataObject("d4");
		const correlations: Array<Correlation> = [
			new BasicCorrelation([d1, d2], 1, Reason.create("made up", ["is", "good"], 100)),
			new BasicCorrelation([d3, d4], 0.4, Reason.create("made up", ["is", "okay"], 50)),
		];
		this.report = new Report(moment(), correlations);
	}

	@Test("minify report")
	public testMinify() {
		const reportSize = JSON.stringify(this.report).length;

		const min = this.report.getMinimalVersion();
		const minSize = JSON.stringify(min).length;

		Expect(reportSize).toBeGreaterThan(minSize);

		// sanity checks
		Expect(min.createdAt).toBe(this.report.createdAt);
		Expect(min.correlations.length).toBe(this.report.correlations.length);


		// from JSON

		const fromJSON = MinimalReport.fromJSON(min);

		Expect(fromJSON).toEqual(min);

	}

	@AsyncTest("expand minimal correlation")
	@Timeout(2000)
	public async testExpand() {

		const minimalCorrelation = this.report.getMinimalVersion().correlations[0];
		// safe the data objects to the db
		const expectedDataObjects = minimalCorrelation.dataObjects.map(d =>
			new MinDataObject(d._id, undefined, undefined, undefined, undefined, d.selfLink)
		);

		await dataObjectsDB().bulkUpdate(expectedDataObjects);
		const expanded = await minimalCorrelation.getExpandedVersion();

		const expandedDataObjects = expanded.dataObjects;

		Expect(expectedDataObjects.length).toBe(expectedDataObjects.length);

		for (let i = 0; i < expandedDataObjects.length; i++) {
			const result = expandedDataObjects[i];
			const expected = expectedDataObjects[i];
			Expect(result._id).toEqual(expected._id);
			Expect(result.originURL).toEqual(expected.originURL);
			Expect(result.content).toEqual(expected.content);
		}

	}

}
