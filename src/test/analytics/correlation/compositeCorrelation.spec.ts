/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {Test, TestFixture, TestCase, Expect} from "alsatian";
import {WeightedCorrelation} from "../../../main/analytics/correlation/weightedCorrelation";
import {WeightedCompositeCorrelation} from "../../../main/analytics/correlation/weightedCompositeCorrelation";
import {DataObject} from "../../../main/dataProviders/dataObjects/dataObject";
import {BasicCorrelation} from "../../../main/analytics/correlation/basicCorrelation";
import * as moment from "moment";
import {Links} from "../../../main/analytics/correlation/reason/links";
import {Reason} from "../../../main/analytics/correlation/reason/reason";
import {StringMap} from "../../../main/util/stringMap";
import {MinDataObject} from "../../dataProviders/dataObjects/minDataObject";

type WeightedCorrelationArgs = {weight: number; correlationFactor: number};

@TestFixture("Testing WeightedCompositeCorrelation")
export class compositeCorrelationSpec {

    @TestCase([ { id: "someID"}, {id: "anotherID"}], [{id: "anotherID"}, {id: "someID"} ])
    @TestCase([ { id: "someID"}, {id: "anotherID"}], [{id: "someID"},{id: "anotherID"} ])
    @TestCase([ { id: "agvsdg" }, { id: "asgsdfqwe" }, { id: "kjhgfd" }, { id: "urtjufddj" }, { id: "124mdl8" }], [{ id: "124mdl8" }, { id: "urtjufddj" }, { id: "kjhgfd" }, { id: "asgsdfqwe" }, { id: "agvsdg" }])
    @TestCase([ { id: "asgsdfqwe" }, { id: "afasf" }, { id: "jklhgfd" }], [{ id: "asgsdfqwe" }, { id: "jklhgfd" }, { id: "afasf" }])
    @Test(".id")
    public testID(wc1: Array<WeightedCorrelationArgs>, wc2: Array<WeightedCorrelationArgs>) {
        Expect(new WeightedCompositeCorrelation(this.createWCs(wc1)).id).toBe(new WeightedCompositeCorrelation(this.createWCs(wc2)).id);
    }

    @Test(".dataObjects uniqueness")
    public testDataObjects() {
        const dataObj1: DataObject = new MinDataObject("ID1", "CS1", "CL1", moment(), [], "");
        const dataObj2: DataObject = new MinDataObject("ID2", "CS2", "CL2", moment(), [], "");
        const listOfDataObjects: Array<Array<DataObject>> =[[dataObj1, dataObj2], [dataObj2, dataObj1]];

        const getStringOfDataObjects = (dataObjects: Array<DataObject>) => dataObjects.map((dataObj: DataObject) => dataObj._id).toString;

        const wc: Array<WeightedCorrelation> = listOfDataObjects
            .map((dataObjects: Array<DataObject>) => new BasicCorrelation(dataObjects, 0, {}))
            .map((bc: BasicCorrelation) => new WeightedCorrelation(bc, 0));
        Expect(getStringOfDataObjects(new WeightedCompositeCorrelation(wc).dataObjects)).toBe(getStringOfDataObjects([dataObj1, dataObj2]));
    }

    @TestCase([{weight: 1, correlationFactor: 0.5}, {weight: 1, correlationFactor: 0}], 0.25)
    @TestCase([{weight: 2, correlationFactor: 0.5}, {weight: 0.5, correlationFactor: 0}], 0.4)
    @TestCase([{weight: 3, correlationFactor: 0.5}, {weight: 2, correlationFactor: 0}, {weight: 1, correlationFactor: 0.75}, {weight: 4, correlationFactor: 1}], 0.625)
    @TestCase([], 0)
    @Test(".correlationFactor")
    public testCorrelationFactor(args: Array<WeightedCorrelationArgs>, expect: number) {
        const wc = this.createWCs(args);
        Expect(new WeightedCompositeCorrelation(wc).correlationFactor).toBe(expect);
    }

    @TestCase([["www.example.com"],["www.example/another.com"]], ["www.example.com", "www.example/another.com"])
    @TestCase([["www.example.com", "www.example/another.com"],["www.example/another.com"]], ["www.example.com", "www.example/another.com"])
    @TestCase([["www.example.com"],["www.example/another.com"],["www.third/link.de", "www.fourth/link.de"], ["www.fourth/link.de"]],
        ["www.example.com", "www.example/another.com", "www.third/link.de", "www.fourth/link.de"])
    @Test(".listOfReason")
    public testListOfReason(listOfListOfString: Array<Array<string>>, expect: Array<string>) {


        const wc: Array<WeightedCorrelation> = listOfListOfString
            .map((listOfString: Array<string>) => Links(listOfString, 0))
            .map((mappedReasons: StringMap<Reason>) => new BasicCorrelation([], 0, mappedReasons))
            .map((bc: BasicCorrelation) => new WeightedCorrelation(bc, 0));

        const composite = new WeightedCompositeCorrelation(wc);
        const reasons = Object.values(composite.reasons);

        Expect(reasons.map((r: Reason) => r.toString()).toString()).toBe(expect.toString());
    }

    private createWCs(args: Array<WeightedCorrelationArgs>): Array<WeightedCorrelation> {
        return args.map(arg => new WeightedCorrelation(new BasicCorrelation([], arg.correlationFactor, {}), arg.weight));
    }
}