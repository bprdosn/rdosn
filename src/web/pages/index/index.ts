import {Page} from "../page";

export class IndexPage extends Page {
    public static readonly instance = new IndexPage();

    public readonly route = "/";
    public readonly requiresAuthentication = false;

    private constructor() {
        super();
    }

    public getHandler(req: any, res: any) {
        res.render("index/index");
    }
}