/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {isURL} from "validator";

/**
 * Provides general utilities for text analytics
 */
export class TextAnalytics {

    // splits into "words", excludes punctuation marks, whitespaces etc.
    private static wordRegex: RegExp = new RegExp(/[…,.?!:; ]+/);

    // splits into "words" (no regard for special characters (except ...), only spaces and similar count)
    private static linkRegex: RegExp = new RegExp(/…|\.{3,}|\s+/);

    /**
     * Extracts single words out of a text
     * Currently able to deal with commas, full stops, question marks, exclamation marks, semicolons and whitespaces
     * @param {string} text to extract words out of
     * @returns {Array<string>} extracted words
     */
    public static extractWords(text: string): Array<string> {
        return text.split(TextAnalytics.wordRegex).filter(x => x); // Filter out empty strings
    }

    /**
     * Extract everything that looks like a valid URL from the text
     * @param {string} text to extract links out of
     * @returns {Array<string>} list of possible links. Can be empty but will never be null.
     */
    public static extractLinks(text: string): Array<string> {
        // get every word
        const words = text.split(TextAnalytics.linkRegex);
        // to make life easier, validate the links with validate.js
        return words.filter(match => isURL(match, {
            require_host: true,
            // TODO should this be set to false? When true, false URLs will be filtered out (example.doc), but so might some real ones (google.de)
            require_protocol: true
        }));
    }
}