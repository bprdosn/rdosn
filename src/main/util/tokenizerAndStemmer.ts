import {PorterStemmer, WordTokenizer} from "natural";
import {DataObject} from "../dataProviders/dataObjects/dataObject";
const stopWord = require("stopword");

export class TokenizerAndStemmer {

    private tokenizer = new WordTokenizer();

    public prepareDataObject(document: DataObject): Array<string> {
        return stopWord.removeStopwords(
            this.tokenizer.tokenize(
                (document.content).toLowerCase()
            ).filter(item => item != undefined && item != "undefined")
        ).map((word: string) => PorterStemmer.stem(word));
    }

    public static prepareKeywords(rawKeywords: Array<string>) {
        return rawKeywords.map((word: string) => PorterStemmer.stem(word));
    }
}