/*
 The MIT License (MIT)

 Copyright (c) 2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import moment = require("moment");
import {Moment} from "moment";

const FORMAT = "DD.MM.YYYY HH:mm";

/* istanbul ignore next */
/**
 * Turn a moment date into a readable date string
 * @param {moment.Moment} date to format
 * @returns {string} in the format declared by the constant in this file
 */
export function formatMoment(date: Moment): string {
	return date.format(FORMAT);
}

/* istanbul ignore next */
/**
 * Turn a ISO date string (as given by moment) into a readable date string
 * @param {string} date as formatted by moment
 * @returns {string} in the format declared by the constant in this file
 */
export function formatDate(date: string): string {
	return formatMoment(moment(date));
}