/**
 The MIT License (MIT)

 Copyright (c) 2017 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
/** created on {05.03.2018}*/


/**
 * Explicitly giving dummy keywords for the start, this should definitely be automated over time
 * Keywords must be given in lower case
 * @type {Array}
 */
export const keywords = ["iphone", "ios", "mac", "macOS", "sierra", "apple", "android", "aws", "bug",
	"samsung", "huawei", "lg", "windows", "windows10", "microsoft", "symantec", "pgp",
	"security", "exploit", "phishing", "encryption", "rest", "google", "hardware",
	"eu", "usa", "germany", "russia", "play", "login", "api", "nest", "raspberry", "watch", "vulnerability", "insecure", "vpn", "https", "facebook", "twitter",
	"reddit", "linux", "aws", "malware", "worm", "ransomware", "wannacry", "cisco", "firewall", "cve-"];