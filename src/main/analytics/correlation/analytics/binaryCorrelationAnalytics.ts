/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {Correlation} from "../correlation";
import {DataObject} from "../../../dataProviders/dataObjects/dataObject";

/**
 * This is a general interface for all correlation analytics like linkAnalytics, keywordAnalytics, ...
 */
export abstract class BinaryCorrelationAnalytics {

	/**
     * Give the algorithm a chance to prepare all the data at once. Called once before a batch of data.
	 * @param {Array<DataObject>} data all the data that will later be called in the analyze method
     * @returns {*} nothing
	 */
	public prepare(data: Array<DataObject>): void {}

    /**
     * Analyzes two given DataObjects and calculates a correlation, correlation factors and reasons.
     * @param {DataObject} obj1 first DataObject to analyze
     * @param {DataObject} obj2 second DataObject to analyze
     * @returns {Correlation} the calculated correlation
     */
    abstract analyze(obj1: DataObject, obj2: DataObject): Correlation;

	/**
	 * Marks the end of the analyze phase giving the analyzer a chance to clean up.
	 * @returns {void} nothing
	 */
	public finish(): void {}
}