# Indicators of new threads

 **Definition "Threat":**
	An indication or warning of probable trouble.
 
 **Is it actually a Threat?**
	Is it a threat for me?
	Is it a threat in general?
	    These questions are too hard to answer right now.
	
 **Is the threat new?**
	In the first 14 days yes, after that it's not new.
	
 **What methods to use?**
 - Timestamp of publication
 - Dates and times in the text
 - Check for correlation inter / intra platforms
 - Check reactions
 - more comments / likes usually => more importance
	
	
	
 **High or critical vulnerabilities are defined as:**
 - Easily exploitable
 - Usually remote from the public Internet
 - Application and Network layers combined
 - Root Cause: Coding errors, configuration flaws and out-of-date or no patching applied
 
## Average Time for vulnerabilities to get fixed:
	Network:
		100% of critical vulnerabilities get fixed in under a month
		9% of high vulnerabilities get fixed in under a month, 29% in 1-3 months, 62% in 3-12 months
		29% of medium vulnerabilities get fixed in under a month, 18% in 1-3 months, 52% in 3-12 months
	Applications
		65% of critical vulnerabilities get fixed in under a month, 23% in 1-3 months, 12% in 3-12 months
		30% of high vulnerabilities get fixed in under a month, 68% in 1-3 months, 2% in 3-12 months
		42% of medium vulnerabilities get fixed in under a month, 24% in 1-3 months, 34% in 3-12 months

## Sources:
 
- https://www.edgescan.com/assets/docs/reports/2016-edgescan-stats-report.pdf
- https://dl.acm.org/citation.cfm?id=2389714

