/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {AsyncTest, TestFixture, Timeout, Expect, Test} from "alsatian";
import {dataObjectsDB, testDB} from "../../../main/services/db/dbs";
import {DB, DocumentHandle} from "../../../main/services/db/db";


@TestFixture("Testing DB connection")
export class DBSpec {

    private db: DB = testDB();

    // Automatically also tests .request
    @AsyncTest(".find")
    @Timeout(10000)
    public async testFind() {
        const docs = await this.db.find();

        Expect(docs.length).toBeGreaterThan(0);
    }

    @AsyncTest(".request (FAIL)")
    @Timeout(10000)
    public async testRequestFail() {
        await Expect(async () => await this.db["request"](undefined)).toThrowAsync();
    }

    @AsyncTest("add document and override")
    @Timeout(10000)
    public async testAdd() {

        const before = await this.db.count();
        const result = await this.db.insert({ data: "This is some data"});
        const after = await this.db.count();

        Expect(before + 1).toBe(after);

        // override the element
        const data = "This is some other data";
        await this.db.insert({ data: data, _id: result.id, _rev: result.rev});
        const afterOverride = await this.db.count();
        // should not affect total count
        Expect(afterOverride).toBe(after);

        // read the element
        const readElement = await this.db.get(result.id);
        Expect(readElement["data"]).toBe(data);
        Expect(readElement["_id"]).toBe(result.id);
        Expect(readElement["_rev"]).not.toBe(result.rev);
    }

    @AsyncTest(".bulkInsert")
    @Timeout(10000)
    public async testBulkInsert() {

        const id1 = "testBulkInsert_1_" + Date.now();
        const id2 = "testBulkInsert_2_" + Date.now();
        const origDocs = [{"data": 1, "_id": id1}, {"data": "unchanged", "_id": id2}, {
            "data": 2,
            "_id": "iDontNeedToHaveAnId_" + Date.now(),
        }];
        const changedDocs = [{"data": "changed", "_id": id1}, {"data": "unchanged", "_id": id2}, {
            "data": 3,
            "_id": "iNeedToHaveAnId_" + Date.now(), // because every Doc in fetchRevsForDocs needs an _id
        }];
        let before = await this.db.count();
        let result = await this.db.bulkInsert(origDocs);
        let after = await this.db.count();

        Expect(result.length).toBe(3);
        Expect(before + 3).toBe(after);

        before = await this.db.count();
        result = await this.db.bulkInsert(await this.db.fetchRevsForDocs(changedDocs));
        after = await this.db.count();

        Expect(result.length).toBe(3);
        Expect(before + 1).toBe(after);

        const doc1: any = result.find(doc => doc.id == id1);
        Expect(doc1).toBeDefined();
        Expect(doc1.ok).toBeTruthy();
        // result only gives back the status, id and rev, not the data itself. So we can only see if the data was successfully inserted
    }

    @AsyncTest("destroy")
    @Timeout(1000)
    public async destroy() {
        // create a document
        const d = {text: "Test document to destroy"};
        // save it
        const handle = await this.db.insert(d);
        // destroy it
        await this.db.destroy(handle);
        // it is no longer in the db
        await Expect(() => this.db.get(handle.id)).toThrowErrorAsync(Error, "deleted");
    }

    @Test("data-objects DB works")
    public dataObjectsDB() {
        // calling it will initialize it. This is enough to test the connection
        dataObjectsDB();
    }

    @AsyncTest("overriding without _rev throws an error")
    @Timeout(1500)
    public async testOverrideError() {
        // add an element
        const handle = await this.db.insert({data: 1});
        // overriding it (without specifying the _rev), fails
        await Expect(() => this.db.insert({_id: handle.id, data: 2})).toThrowErrorAsync(Error, "Document update conflict.");
    }

    @AsyncTest(".getAllDocs")
    @Timeout(10000)
    public async testGetAllDocs() {
        const docs = await this.db.getAllDocs();

        Expect(docs.length).toBeGreaterThan(0); // we should have some data to test with

        docs.forEach((doc: any)=>{
            Expect(doc).toBeDefined();
            Expect(doc._id.startsWith("_")).not.toBeTruthy(); // no design documents
        });
    }


    @AsyncTest(".count")
    @Timeout(10000)
    public async testCount() {
        // calling count and getAllDocs should yield the same result
        const count = await this.db.count();
        const all = await this.db.getAllDocs();
        Expect(count).toEqual(all.length);
    }

    @AsyncTest(".fetchRevsForDocs")
    @Timeout(10000)
    public async testFetchRevsForDocs() {

        const id1 = "testFetchRevsForDocs_1_" + Date.now();
        const id2 = "testFetchRevsForDocs_2_" + Date.now();
        const docs = [{"_id": id1, "data": 1}, {"_id": id2, "data": 2}];
        const docsEmpty: Array<any> = [];
        const docsNoOriginalData = [{"_id": "randomId" + Date.now()}];
        const docsOriginalDataAndNewData = [{
            "_id": id1,
            "data": 1
        }, {
            "_id": "newId" + Date.now(),
            "data": "newData"
        }];

        // we're just gonna assume this function works properly
        const resultInsert: Array<any> = await this.db.bulkInsert(docs);

        const resultFetchOriginalData = await this.db.fetchRevsForDocs(docs);
        const resultFetchEmpty = await this.db.fetchRevsForDocs(docsEmpty);
        const resultFetchNoOriginalData = await this.db.fetchRevsForDocs(docsNoOriginalData);
        const resultFetchOriginalDataAndNewData = await this.db.fetchRevsForDocs(docsOriginalDataAndNewData);

        Expect(resultInsert.length).toBe(2);

        Expect(resultFetchOriginalData.length).toBe(2);

    docs.forEach(doc => {
    const withRev = resultFetchOriginalData.find(elem => elem._id === doc._id);
        Expect(withRev).toEqual(doc);

        const insertHandle: DocumentHandle = resultInsert.find(elem => elem.id === doc._id);
        Expect(withRev._rev).toEqual(insertHandle.rev);
    });

        Expect(resultFetchEmpty).toBeEmpty();

        Expect(resultFetchNoOriginalData).toEqual(docsNoOriginalData);

        Expect(resultFetchOriginalDataAndNewData.length).toBe(2);



        Expect(resultFetchOriginalDataAndNewData.find((elem: any) => {
            return elem._id === id1;
        })._id).toEqual(docs[0]._id);
        Expect(resultFetchOriginalDataAndNewData.find((elem: any) => {
            return elem._id === id1;
        })._rev).toEqual(resultInsert.find((elem: any) => {
            return elem.id === id1;
        }).rev);
    }

    @AsyncTest(".bulkUpdate")
    @Timeout(10000)
    public async testBulkUpdate() {

    // Test the update method by first inserting the documents (thus increasing the count)
    // then changing values and updating the docs with a new document (increasing the count again).
    // At the end see if the docs were updated.

    const id1 = "UpdateTest1" + Date.now();
    const id2 = "UpdateTest2" + Date.now();
    const id3 = "UpdateTest3" + Date.now();

    const doc1 = {_id: id1, data: "A"};
    const doc2 = {_id: id2, data: "A"};
    const doc3 = {_id: id3, data: "B"};

        const before = await this.db.count();
        await this.db.bulkUpdate([doc1, doc2]); // insert
        const middle = await this.db.count();

        // modify data
        doc1.data = "B";
        doc2.data = "B";
        await this.db.bulkUpdate([doc1, doc2, doc3]); // update
        const after = await this.db.count();

        // check counts
        Expect(before + 2).toBe(middle);
        Expect(middle + 1).toBe(after);

        // check the values
        const fetchResult = await this.db.fetchDocs([id1, id2, id3], {"include_docs": true});
        const docs  = fetchResult.map(result => result.doc);

        Expect(docs.length).toBe(3);
        docs.forEach(doc => Expect(doc.data).toBe("B"));

    }

    @AsyncTest("Quota exceeded")
    @Timeout(5000 * 10) /* 5 seconds per request? */
    public async testQuota() {
        // create an array and set each element to a new getAllDocs call
        const promises = Array.apply(null, {length: 10}).map( () => this.db.getAllDocs(), this);
        // calling all of these at the same time causes the quota to be exceeded.
        // So this tests whether the getAllDocs method handles this correctly by waiting/retrying
        await Promise.all(promises);
    }

    @AsyncTest("Update single doc")
    @Timeout(20000)
    public async testUpdate() {
        const id = "UPDATE_TEST_" + Date.now();
        const doc = { _id: id, data: 1 };
        await this.db.update(doc);

        const fetchedDoc = await this.db.get(id);
        Expect(fetchedDoc.data).toBe(1);
        Expect(fetchedDoc._id).toBe(id);
        Expect(fetchedDoc._rev).toBeDefined();

        const doc2 = { _id: id, data: 2};
        await this.db.update(doc2);

        const fetchedDoc2 = await this.db.get(id);
        Expect(fetchedDoc2.data).toBe(2);
        Expect(fetchedDoc2._id).toBe(id);
        Expect(fetchedDoc2._rev).toBeDefined();
    }

    @AsyncTest("Duplicate ids")
    @Timeout(2000)
    public async testDuplicateIds() {
        const doc = { _id: "DUPLICATE" + Date.now(), data: "Hello there"};
        await this.db.insert(doc);
        // id is already set, so an error is thrown
        await Expect(() => this.db.insert(doc)).toThrowAsync();
        // same with bulk insert
        await Expect(() => this.db.bulkInsert([doc])).toThrowAsync();
    }

    @AsyncTest("no id")
    public async testNoId() {
        await Expect(() => this.db.fetchRevsForDocs([{}])).toThrowAsync();
    }
}