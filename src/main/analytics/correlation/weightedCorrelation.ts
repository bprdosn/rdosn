/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {Correlation} from "./correlation";
import {ForwardingCorrelation} from "./forwardingCorrelation";

/**
 * Adds a weight to a correlation meaning that the correlation factor should be multiplied by the given factor.
 */
export class WeightedCorrelation extends ForwardingCorrelation {

    /**
     * Initialises the WeightedCorrelation
     * @param {Correlation} correlation which should be weighted
     * @param {number} weight gets compared to other weights
     */
    public constructor(correlation: Correlation, public readonly weight: number) {
        super(correlation);
    }

    //TODO this might make things trickier when deserializing
	public totalCorrelationFactor(): number {
        return this.correlationFactor * this.weight;
	}
}