/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {AsyncTest, Expect, TestFixture} from "alsatian";
import {DataProvider} from "../../../main/dataProviders/dataProvider";
import {DataObject} from "../../../main/dataProviders/dataObjects/dataObject";
import {RetryProvider} from "../../../main/dataProviders/decorators/retryProvider";

class DummyProvider<T extends DataObject> implements DataProvider<T, void> {

    public counter = 0;

    public constructor(public readonly ignoreCounter: boolean) { }

    public getContext() {
        return async () => {
            this.counter++;
            if (this.ignoreCounter || this.counter <= RetryProvider.MAX_RETRIES) throw Error("This should be caught by retry");
            else return [];
        };
    }
}

@TestFixture("Testing retry provider")
export class RetryProviderSpec {

    @AsyncTest("test retry")
    public async testRetry() {
        // wrap dummy provider in retry provider
        const p = new RetryProvider(new DummyProvider(false));
        const context = p.getContext({});

        // retry succeeds eventually
        await Expect(async () => context()).not.toThrowAsync();
    }

    @AsyncTest("test retry (FAIL)")
    public async testRetryFail() {
        // wrap dummy provider in retry provider
        const p = new RetryProvider(new DummyProvider(true));
        const context = p.getContext({});

        // retry runs out of tries
        await Expect(async () => context()).toThrowAsync();
    }

}