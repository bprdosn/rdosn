/**
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
import {DataObject} from "./dataObject";
import * as moment from "moment";
import {ProviderInfo} from "./providerInfo";

export class RedditPost extends DataObject {
    /**
     * The kind of the reddit post. Should always be "t3"
     * @type string
     */
    public readonly kind: string;

    // data
    /*public readonly domain: string;
    public readonly approved_at_utc: any;
    public readonly media_embed: any;
    public readonly subreddit: string;
    public readonly selftext_html: string;
    /** use score instead, idk what this is for*/
    /*public readonly likes: any;
    public readonly suggested_sort: any;
    public readonly user_reports: any;
    public readonly secure_media: any;
    public readonly is_reddit_media_domain: boolean;
    public readonly saved: boolean;
    public readonly banned_at_utc: any;
    public readonly view_count: null;
    public readonly archived: boolean;
    public readonly clicked: boolean;
    public readonly report_reasons: any;
    public readonly num_crossposts: number;
    public readonly link_flair_text: any;
    public readonly mod_reports: any;
    public readonly can_mod_post: boolean;
    public readonly is_crosspostable: boolean;
    public readonly pinned: boolean;
    public readonly approved_by: any;
    public readonly over_18: boolean;
    public readonly hidden: boolean;
    public readonly thumbnail: string;
    public readonly subreddit_id: string;
    public readonly edited: boolean;
    public readonly link_flair_css_class: any;
    public readonly author_flair_css_class: any;
    public readonly contest_mode: boolean;
    public readonly gilded: number;
    public readonly downs: number;
    public readonly brand_safe: boolean;
    public readonly secure_media_embed: any;
    public readonly removal_reason: any;
    public readonly author_flair_text: any;
    public readonly stickied: boolean;
    public readonly can_gild: boolean;
    public readonly is_self: boolean;
    public readonly parent_whitelist_status: string;
    public readonly name: string;
    public readonly spoiler: boolean;
    public readonly permalink: string;
    public readonly subreddit_type: string;
    public readonly locked: boolean;
    public readonly hide_score: boolean;
    public readonly created: number;
    public readonly whitelist_status: string;
    public readonly quarantine: boolean;
    public readonly subreddit_name_prefixed: string;
    public readonly ups: number;
    public readonly media: any;
    public readonly num_comments: number;
    public readonly visited: boolean;
    public readonly num_reports: any;
    public readonly is_video: boolean;
    public readonly distinguished: any;*/
    // public readonly author: string;
    public readonly score: number;

    /**
     * Initializes the data object
     * @param {*} rawData any field in data will be accessible as a field in this object as public readonly
     * @param {string} subreddit where the post comes from
     */
    public constructor(rawData: any, subreddit: string) {
        super("RE" + rawData.data.id, rawData.data.title, rawData.data.selftext, moment.unix(rawData.data.created_utc), [rawData.data.url], "https://www.reddit.com" + rawData.data.permalink,
              new ProviderInfo("Reddit", "r/" + subreddit + ", u/" + rawData.data.author));

        this.kind = rawData.kind;
        // this.author = rawData.data.author;
        this.score = rawData.data.score;
    }

    /**
     * Provides an interface for the popularity calculations
     * @returns {*} list of attributes that are needed for the popularity calculations, with weight being between 0 and 1 and all weights adding up to 1
     */
    public get popularityAttributes(): Array<{ value: number, weight: number }> {
        return [{ value: this.score, weight: 1 }];
    }
}
