/**
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import * as Sl from "slack-node";
import {isNullOrUndefined} from "util";
import {Notification} from "./notification";
import {env} from "../../config/env";

export class Slack extends Notification<string> {
    private static _instance: Slack;

    private handle: Sl;

    private constructor() {
        super();

        /* istanbul ignore next */
        if(isNullOrUndefined(env().SLACK_WEBHOOK))
            throw new TypeError("Missing Slack Webhook");

        this.handle = new Sl();
        this.handle.setWebhook(env().SLACK_WEBHOOK!);
    }

    public static get instance(): Slack {
        if(!Slack._instance)
            Slack._instance = new Slack();

        return Slack._instance;
    }

    protected send(channel: string, username: string, content: string): Promise<Sl.WebhookResponse> {
        return new Promise<Sl.WebhookResponse>((resolve, reject) => {
            this.handle.webhook({
                    channel: channel,
                    username: username,
                    text: content
                },
                (err: any, response: Sl.WebhookResponse) => {
                    /* istanbul ignore next */
                    if(err)
                        reject(err);
                    else if (response.status == "fail")
                        reject(response);
                    else
                        resolve(response);
            });
        });
    }
}