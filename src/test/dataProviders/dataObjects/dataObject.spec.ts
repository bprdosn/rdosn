/**
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import {TestFixture, Expect, TestCase, Test} from "alsatian";
import {DataObject} from "../../../main/dataProviders/dataObjects/dataObject";
import {isNullOrUndefined} from "util";
import {FacebookPost} from "../../../main/dataProviders/dataObjects/facebookPost";
import {HackernewsStory} from "../../../main/dataProviders/dataObjects/hackernewsStory";
import {RedditPost} from "../../../main/dataProviders/dataObjects/redditPost";
import {RssFeed} from "../../../main/dataProviders/dataObjects/rssFeed";
import {Tweet} from "../../../main/dataProviders/dataObjects/tweet";
import * as moment from "moment";
import {MinDataObject} from "./minDataObject";
import {XForceCollection} from "../../../main/dataProviders/dataObjects/xforceCollection";

@TestFixture("Testing Data Object")
export class DataObjectSpec {

    @TestCase("")
    @TestCase("  ")
    @TestCase(null)
    @TestCase(undefined)
    @Test("DataObject Constructor should throw")
    public testDataObjectConstructorThrow(id: string) {
        Expect(() => new MinDataObject(id, "", "", moment(), [], "")).toThrow();
    }

    @TestCase("Hi", "Be")
    @TestCase(" Test", undefined)
    @TestCase(undefined, "Test ")
    @TestCase(" Test2", null)
    @TestCase(null, "Test3 ")
    @Test("DataObject content")
    public testDataObjectContent(content_short: string, content_long: string) {
        const obj = new MinDataObject("x", content_short, content_long, moment(), [], "");

        if(isNullOrUndefined(content_long)) {
            // Content should be just short content
            Expect(obj.content.trim()).toEqual(content_short.trim());
        } else if(isNullOrUndefined(content_short)) {
            // Content should be just long content
            Expect(obj.content.trim()).toEqual(content_long.trim());
        } else {
            // Both, short and long content, should appear in content getter
            Expect(obj.content.includes(content_short) && obj.content.includes(content_long)).toBeTruthy();
        }
    }

    @Test("Data Object Relevance")
    public testDataObjectRelevance() {
        const obj = new MinDataObject("x", "Hi", "Be", moment(), [], "");

        Expect(obj.popularity).not.toBeDefined();

        // Cant set it to invalid values
        Expect(() => obj.popularity = -1).toThrow();
        Expect(() => obj.popularity = 3).toThrow();

        obj.popularity = 1;

        Expect(obj.popularity).toBeDefined();
        Expect(() => obj.popularity = 0.5).toThrow(); // Cant set it again
    }

    @TestCase(new FacebookPost({id: "938737727918485504", created_time: 123432,
        message: "Dummy", likes: 0, shares: 0}, [], "Test Facebook user"))
    @TestCase(new HackernewsStory({id: "938737727918485504", time: 123432,
        title: "Dummy", score: 0}))
    @TestCase(new RedditPost({ data: {id: "938737727918485504", created_utc: 123432,
        text: "Dummy", score: 0}}, "Test subreddit"))
    @TestCase(new RssFeed({link: "h1"}, "Test RSS feed"))
    @TestCase(new FacebookPost({id: "938737727918485504", created_time: 123432,
        message: "Dummy", likes: 0, shares: 0}, [], "Test Facebook user"))
    @TestCase(new Tweet({id_str: "938737727918485504", created_at: moment("2017-12-07T12:51:29.000"),
        text: "Dummy", user: { id: "H" }, retweet_count: 0, favorite_count: 0}, [], "Test Tweeter"))
    @TestCase(new XForceCollection({caseFileID: "938737727918485504", created: moment("2017-12-07T12:51:29.000"),
        title: "Dummy", tags: ["hey", "bra"], owner: { verified: "H" }, total_votes: 10}))
    @TestCase(new XForceCollection({caseFileID: "938737727918485504", created: moment("2017-12-07T12:51:29.000"),
        title: "Dummy", tags: ["hey", "bra"], owner: { }, total_votes: undefined}))
    @Test("Data Object RelevanceAttributes")
    public testDataObjectRelevanceAttributes(obj: DataObject) {
        obj.popularityAttributes.forEach((val) => {
            Expect(val.weight).toBeGreaterThan(0);
            Expect(val.weight <= 1).toBeTruthy();
        });
        const weightSum = obj.popularityAttributes.reduce((acc, val) => { return acc + val.weight; }, 0);

        Expect(weightSum).toBe(1);
    }
}