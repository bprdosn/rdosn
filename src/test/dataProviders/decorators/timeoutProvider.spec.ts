/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {AsyncTest, Expect, TestFixture, TestCase, Timeout} from "alsatian";
import {DataProvider} from "../../../main/dataProviders/dataProvider";
import {DataObject} from "../../../main/dataProviders/dataObjects/dataObject";
import {sleep} from "../../../main/util/sleep";
import {TimeoutProvider} from "../../../main/dataProviders/decorators/timeoutProvider";

class DummyProvider implements DataProvider<DataObject, void> {

    public constructor(public readonly time: number) { }

    public getContext() {
        return async () => {
            await sleep(this.time * 1000);
            return [];
        };
    }
}

@TestFixture("Testing timeout provider")
export class TimeoutProviderSpec {

    @AsyncTest("timeout not exceeded")
    @TestCase(1, 0.1)
    @TestCase(0.1, 0.05)
    @Timeout(200)
    public async testNotExceeded(timeout: number, context_wait: number) {
        const p = new TimeoutProvider(
            new DummyProvider(context_wait),
            timeout
        );
        const data = await p.getContext({})();
        Expect(data.length).toBe(0);
    }


    @AsyncTest("timeout is exceeded")
    @TestCase(0.1, 1)
    @TestCase(0.05, 0.1)
    @Timeout(1000)
    public async testExceeded(timeout: number, context_wait: number) {
        const p = new TimeoutProvider(
            new DummyProvider(context_wait),
            timeout
        );
        await Expect(() => p.getContext({})()).toThrowAsync();
    }
}