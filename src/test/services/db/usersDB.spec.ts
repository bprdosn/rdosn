/*
 The MIT License (MIT)

 Copyright (c) 2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {AsyncTest, Timeout, Expect, TestFixture} from "alsatian";
import {User, UserIdentity} from "../../../main/services/db/user";
import {cloudant} from "../../../main/services/db/dbs";
import {UsersDB} from "../../../main/services/db/usersDB";
import {sleep} from "../../../main/util/sleep";

@TestFixture("Testing UsersDB")
export class UsersDBSpec {


	private readonly db = new UsersDB(cloudant(), "test");

	@AsyncTest("get or create user (get)")
	@Timeout(3000)
	public async testGetOrCreate_Get() {

		const identitiy: UserIdentity = {provider: "test", id: "GET_OR_CREATE_TEST_" + Date.now()};
		const email = "testEmail";
		const user = new User(identitiy, email, false);

		await this.db.insert(user);

		// it should be in the db
		const fetchedUser = await this.db.getOrCreateUser(identitiy, email);

		UsersDBSpec.expectEqual(fetchedUser, user);

	}

	@AsyncTest("get or create (create)")
	@Timeout(3000)
	public async testGetOrCreate_Create() {
		// create a new user
		const newIdentity: UserIdentity = {provider: "test", id: "GET_OR_CREATE_TEST_NEW_" + Date.now()};
		const email = "testEmail";
		const id = User.getID(newIdentity);
		await Expect(async () => this.db.get(id)).toThrowAsync(); // 404
		const newUser = await this.db.getOrCreateUser(newIdentity, email);
		// creating the user is async, so give a second
		await sleep(1000);
		const fetchedUser = await this.db.get(id);

		UsersDBSpec.expectEqual(fetchedUser, newUser);
	}

	private static expectEqual(user: User, expected: User) {
		Expect(user._id).toEqual(user._id);
		Expect(user._rev).toEqual(user._rev);
		Expect(user.updateInterval).toEqual(user.updateInterval);
		Expect(user.email).toEqual(user.email);
		Expect(user.receiveEMailNotifications).toEqual(user.receiveEMailNotifications);
		Expect(user.identity).toEqual(user.identity);
	}


}
