/*
 The MIT License (MIT)

 Copyright (c) 2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {DataObject} from "../../../../main/dataProviders/dataObjects/dataObject";
import {BinaryCorrelationAnalytics} from "../../../../main/analytics/correlation/analytics/binaryCorrelationAnalytics";
import {Reason} from "../../../../main/analytics/correlation/reason/reason";
import {BasicCorrelation} from "../../../../main/analytics/correlation/basicCorrelation";
import {Correlation} from "../../../../main/analytics/correlation/correlation";

export class DummyBinaryAnalyser extends BinaryCorrelationAnalytics {

	public constructor(private readonly type: string, private readonly factor: number = 0.5) {
		super();
	}

	public analyze(obj1: DataObject, obj2: DataObject): Correlation {
		return new BasicCorrelation(
			[obj1, obj2],
			this.factor,
			Reason.create(this.type, [obj1._id + " and " + obj2._id], this.factor)
		);
	}
}