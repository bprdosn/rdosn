/*
 The MIT License (MIT)

 Copyright (c) 2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {Correlation} from "../../analytics/correlation/correlation";
import {Moment} from "moment";
import {StringMap} from "../../util/stringMap";
import {Reason} from "../../analytics/correlation/reason/reason";
import {DataObject} from "../../dataProviders/dataObjects/dataObject";
import {dataObjectsDB} from "./dbs";
import {orThrow} from "../../util/orDefault";
import moment = require("moment");

/**
 * Minimal version od DataObject with only the id for reference and the self link.
 */
class MinimalDataObject {
	public constructor(
		readonly _id: string,
		readonly selfLink: string
	) {}

	public static getMinimalVersion(d: DataObject): MinimalDataObject {
		return new MinimalDataObject(d._id, d.originURL);
	}

}

/**
 * Same as Correlation but uses MinimalDataObjects instead of regular ones
 */
export class MinimalCorrelation {
	public constructor(
		public readonly id: string,
		public readonly dataObjects: Array<MinimalDataObject>,
		public readonly correlationFactor: number,
		public readonly reasons: StringMap<Reason>,
		public readonly avgRelevance: number,
		public readonly avgPopularity: number
	) {}

	public static getMinimalVersion(c: Correlation): MinimalCorrelation {
		return new MinimalCorrelation(
			c.id,
			c.dataObjects.map(d => MinimalDataObject.getMinimalVersion(d)),
			c.correlationFactor,
			c.reasons,
			c.avgRelevance,
			c.avgPopularity
		);
	}

	public async getExpandedVersion(): Promise<Correlation> {
		const ids = this.dataObjects.map(d => d._id);
		const expandedDataObjects = await dataObjectsDB().getFullDocs(ids);

		return {
			id: this.id,
			dataObjects: expandedDataObjects,
			correlationFactor: this.correlationFactor,
			reasons: this.reasons,
			avgRelevance: this.avgRelevance,
			avgPopularity: this.avgPopularity,
		};
	}

	public static fromJSON(json: any): MinimalCorrelation {
		return new MinimalCorrelation(
			orThrow(json.id, "Correlation missing id"),
			orThrow(json.dataObjects, "Correlation missing data objects"),
			orThrow(json.correlationFactor, "Correlation missing correlation factor"),
			orThrow(json.reasons, "Correlation missing reasons"),
			orThrow(json.avgRelevance, "Correlation missing avg relevance"),
			orThrow(json.avgPopularity, "Correlation missing avg popularity")
		);
	}
}

/**
 * The same as report but with minimal dat object info
 */
export class MinimalReport {
	public constructor(
		public readonly _id: string,
		public readonly createdAt: Moment,
		public readonly correlations: Array<MinimalCorrelation>
	) {}

	public static fromJSON(json: any): MinimalReport {
		const correlations: Array<MinimalCorrelation> =
			orThrow<any>(json.correlations, "Report missing correlations")
				.map((c: any) => MinimalCorrelation.fromJSON(c));

		return new MinimalReport(
			orThrow(json._id, "Report missing _id"),
			orThrow(moment(json.createdAt), "Report missing createdAt"),
			correlations
		);
	}
}

/**
 * The result of a complete analysis
 */
export class Report {

	public readonly _id: string;

	public constructor(
		public readonly createdAt: Moment,
		public readonly correlations: Array<Correlation>
	) {
		// the id is just the timestamp so finding the newest is easier
		this._id = createdAt.unix().toString();
	}

	/**
	 * A version optimised for storing in the db.
	 * @return {MinimalReport} same as this, but with less info on all the data objects
	 */
	public getMinimalVersion(): MinimalReport {
		return new MinimalReport(
			this._id,
			this.createdAt,
			this.correlations.map(c => MinimalCorrelation.getMinimalVersion(c))
		);
	}
}
