/*
 The MIT License (MIT)

 Copyright (c) 2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */


import {AsyncTest, Expect, TestCase, TestFixture} from "alsatian";
import {limitAsync} from "../../main/util/limitAsync";
import {sleep} from "../../main/util/sleep";

@TestFixture("limit async")
export class LimitAsyncSpec {

	private calls_running = 0;
	private calls_total = 0;

	@TestCase(20, 1)
	@TestCase(1, 20)
	@TestCase(20, 20)
	@TestCase(20, 10)
	@AsyncTest("limit async")
	public async testLimit(n: number, limit: number) {
		this.calls_total = 0;
		this.calls_running = 0;
		const result = await limitAsync(limit, this.createCalls(n, limit));
		Expect(this.calls_total).toBe(n);
		Expect(result.length).toBe(n);
		result.forEach(r => Expect(r).toBe("Done"));
	}

	private createCalls(n: number, limit: number): Array<() => Promise<string>> {

		const result = [];
		for (let i = 0; i < n; i++) {
			result.push(async () => {
				if (this.calls_running >= limit) throw Error("More than limit running!");
				this.calls_running++;
				this.calls_total++;
				await sleep(1);
				this.calls_running--;
				return "Done";
			});
		}
		return result;

	}

}