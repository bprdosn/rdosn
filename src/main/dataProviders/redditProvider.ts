/**
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import {ProviderContext, DataProvider} from "./dataProvider";
import {RedditPost} from "./dataObjects/redditPost";
import {PopularityRelevance} from "../analytics/relevance/popularityRelevance";
import {getLogger} from "log4js";
import {env} from "../config/env";
import {orThrow} from "../util/orDefault";
let rawJS = require("raw.js");

const logger = getLogger("provider.reddit");


/**
 * subreddit: the subreddit to fetch from
 * limit: limit how many posts should be fetched (max 100)
 */
export type RedditProviderOptions = {
	subreddit: string;
	limit: number;
};

/**
 * gives access to Reddit via raw.js (reddit.com/r/rawjs/)
 */
export class RedditProvider implements DataProvider<RedditPost, RedditProviderOptions> {
	private static _instance: RedditProvider;

    private readonly redditAPI: any;

    public readonly relevance: PopularityRelevance<RedditPost> = new PopularityRelevance<RedditPost>();

	/**
	 * Initialises the Reddit client
	 */
	private constructor() {
		this.redditAPI = new rawJS("RDOSN raw.js");
	}

    public static get instance(): RedditProvider  {
        if(!RedditProvider._instance)
            RedditProvider._instance = new RedditProvider();

        return RedditProvider._instance;
    }

	/**
	 * Authenticates this client instance. Credentials must be in .env
	 * @returns {Promise<*>} returns the reddit response wrapped in a promise
	 */
	public async authenticate(): Promise<any> {
		return new Promise<any>((resolve, reject) => {
			logger.trace("authenticating...");
			this.redditAPI.setupOAuth2(env().REDDIT_CLIENT_ID, env().REDDIT_SECRET);

			this.redditAPI.auth({
				"username": env().REDDIT_USERNAME,
				"password": env().REDDIT_PASSWORD
			}, (err: any, response: any) => {
                /* istanbul ignore next */
				if (err) {
					logger.error("Unable to authenticate user: ", err);
					reject("Unable to authenticate user: " + err);
					return;
				}

				// The user is now authenticated. If you want the temporary bearer token, it's available as response.access_token
				// and will be valid for response.expires_in seconds.
				// raw.js will automatically refresh the bearer token as it expires. Unlike web apps, no refresh tokens are available.
				logger.trace("authenticated");
				resolve(response);
			});
		});
	}

    /**
     * Get the limit newest posts from subReddit
     * @returns {Promise<Array<RedditPost>>} An array of Redditposts wrapped in a promise
     * @param {RedditProviderOptions} options {@link RedditProviderOptions}
     */
	public async getData(options: RedditProviderOptions): Promise<Array<RedditPost>> {
		orThrow(options.subreddit, "Missing subreddit!");

		return this.getNew(options.subreddit, options.limit);
	}

    /**
     * @returns {ProviderContext} use this and pull data via context.fetchData
     * @param {RedditProviderOptions} options {@link RedditProviderOptions}
     */
    public getContext(options: RedditProviderOptions): ProviderContext<RedditPost> {
        return () => this.getData(options);
    }

    /**
     * Get the limit hottest posts from subReddit
     * @param {string} subReddit the subreddit to fetch from
     * @param {number} limit how many posts should be fetched
     * @returns {Promise<Array<RedditPost>>} An array of Redditposts wrapped in a promise
     */
    public async getHot(subReddit: string, limit: number): Promise<Array<RedditPost>> {
        return new Promise<Array<RedditPost>>((resolve, reject) => {
            this.redditAPI.hot({"r": subReddit, "limit": limit}, (err: any, response: any) => {
                if (err) {
                    logger.error("Could not fetch hot /r/", subReddit, ": ", err);
                    reject("Could not fetch hot /r/" + subReddit + ": " + err);
                    return;
                }

                resolve(response.children.map((value: any) => new RedditPost(value, subReddit)));
            });
        });
    }

    /**
     * Get the limit newest posts from subReddit
     * @param {string} subReddit the subreddit to fetch from
     * @param {number} limit how many posts should be fetched
     * @returns {Promise<Array<RedditPost>>} An array of Redditposts wrapped in a promise
     */
    public async getNew(subReddit: string, limit: number): Promise<Array<RedditPost>> {
        logger.trace("Getting new posts from ", subReddit);
		
        return new Promise<Array<RedditPost>>((resolve, reject) => {
            this.redditAPI.new({"r": subReddit, "limit": limit}, (err: any, response: any) => {
                if (err) {
                    logger.error("Could not fetch new /r/", subReddit, ": ", err);
                    reject("Could not fetch new /r/" + subReddit + ": " + err);
                    return;
                }

                resolve(response.children.map((value: any) => new RedditPost(value, subReddit)));
            });
        });
    }

	/**
	 * Get the limit top posts from subReddit
	 * @param {string} subReddit the subreddit to fetch from
	 * @param {number} limit how many posts should be fetched
	 * @returns {Promise<Array<RedditPost>>} An array of Redditposts wrapped in a promise
	 */
	public async getTop(subReddit: string, limit: number): Promise<Array<RedditPost>> {
		return new Promise<Array<RedditPost>>((resolve, reject) => {
			this.redditAPI.top({"r": subReddit, "limit": limit}, (err: any, response: any) => {
				if (err) {
					logger.error("Could not fetch top /r/", subReddit, ": ", err);
					reject("Could not fetch top /r/" + subReddit + ": " + err);
					return;
				}

                resolve(response.children.map((value: any) => new RedditPost(value, subReddit)));
			});
		});
	}

	/**
	 * Get the limit most controversial posts from subReddit
	 * @param {string} subReddit the subreddit to fetch from
	 * @param {number} limit how many posts should be fetched
	 * @returns {Promise<Array<RedditPost>>} An array of Redditposts wrapped in a promise
	 */
	public async getControversial(subReddit: string, limit: number): Promise<Array<RedditPost>> {
		return new Promise<Array<RedditPost>>((resolve, reject) => {
			this.redditAPI.controversial({"r": subReddit, "limit": limit}, (err: any, response: any) => {
				if (err) {
					logger.error("Could not fetch controversial /r/", subReddit, ": ", err);
					reject("Could not fetch controversial /r/" + subReddit + ": " + err);
					return;
				}

				resolve(response.children.map((value: any) => new RedditPost(value, subReddit)));
			});
		});
	}
}