/**
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


import {ProviderContext, DataProvider} from "./dataProvider";
import {Tweet} from "./dataObjects/tweet";
import {LinkResolver} from "../analytics/linkResolver";
import {TextAnalytics} from "../analytics/textAnalytics";
import {getLogger} from "log4js";
import {env} from "../config/env";

let Twitter = require("twitter");

const logger = getLogger("provider.twitter");

/**
 * Nomination according to the twitter api
 * screen_name: the user to fetch the timeline from
 * count: count how many posts should be fetched (1200 max)
 */
export type TwitterProviderOptions = {
    screen_name: string;
    count: number;
};

/**
 * Allows Access to the Twitter API (https://developer.twitter.com/en/docs)
 * All communication with the Twitter API should be handled by this class
 */
export class TwitterProvider implements DataProvider<Tweet, TwitterProviderOptions> {
    private static _instance: TwitterProvider;

    private readonly client: any;

    /**
     * Initializes the Twitter client
     */
    private constructor() {
        this.client = new Twitter({
            consumer_key: env().TWITTER_CONSUMER_KEY,
            consumer_secret: env().TWITTER_CONSUMER_SECRET,
            access_token_key: env().TWITTER_ACCESS_TOKEN_KEY,
            access_token_secret: env().TWITTER_ACCESS_TOKEN_SECRET
        });
    }

    public static get instance(): TwitterProvider  {
        if(!TwitterProvider._instance)
            TwitterProvider._instance = new TwitterProvider();

        return TwitterProvider._instance;
    }

    /**
     * Accesses a users timeline via a get request
     * @param {TwitterProviderOptions} options to perform the request with
     * @return {Promise<Array<Tweet>>} last tweets as an array wrapped in a promise
     */
    public async getData(options: TwitterProviderOptions): Promise<Array<Tweet>> {
        logger.trace("Getting recent tweets from " + options.screen_name);
        const tweets: Array<any> = await this.client.get("statuses/user_timeline", options);

        const promisedTweets: Promise<Tweet>[] = tweets.map(async (tweet) => {
            // grab the text and find all links
            const text = tweet.text;
            const unresolvedLinks: Array<string> = TextAnalytics.extractLinks(text);

            // resolve all links (each resolve returns a promise, which we collect in Promise.all to one promise)
            const links: Array<string> = await Promise.all(unresolvedLinks.map( link => LinkResolver.resolveLink(link)));

            return new Tweet(tweet, links, options.screen_name);
        });

       return await Promise.all(promisedTweets);
    }

    /**
     * @param {TwitterProviderOptions} options to fetch posts for
     * @returns {ProviderContext} use this and pull data via context.fetchData
     */
    public getContext(options: TwitterProviderOptions): ProviderContext<Tweet> {
        return () => this.getData(options);
    }
}