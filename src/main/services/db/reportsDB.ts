/*
 The MIT License (MIT)

 Copyright (c) 2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {DB} from "./db";
import {Correlation} from "../../analytics/correlation/correlation";
import {MinimalReport, Report} from "./report";
import moment = require("moment");
import {orThrow} from "../../util/orDefault";
import {getLogger} from "log4js";
import {retry} from "../../util/retry";

const logger = getLogger("report db");

export class ReportsDB extends DB {

    // As of this comment (https://github.com/cloudant/java-cloudant/issues/317#issuecomment-261496308)
    // requests are limited to one MB for the free version.
    private static readonly MAX_SIZE = 1000000;

    public async getLastReport(): Promise<MinimalReport> {
        const results = await this.list({
            include_docs: true, // gte the doc included
            limit: 1, // only the newest
            descending: true // ids are timestamps, so highest is newest
        });

        /* istanbul ignore next */
        if (results.length != 1) {
            throw new Error("Limit 1 was requested, but " + results.length + " results were returned. Result: " + JSON.stringify(results));
        }

        const doc = orThrow(
            results[0].doc,
            "The last report is undefined! Results: " + JSON.stringify(results)
        );

        return MinimalReport.fromJSON(doc);

    }

    public async saveReport(correlations: Array<Correlation>): Promise<Report> {
        const report = new Report(moment(), correlations);
        const toSave = report.getMinimalVersion();
        const reportSize = JSON.stringify(toSave).length;

        /* istanbul ignore next */
        if (reportSize > ReportsDB.MAX_SIZE) {
            throw new Error("Report is too big to be saved! (" + reportSize + " of a max of " + ReportsDB.MAX_SIZE + ")");
        }

        // since its a big object, it fails a lot. So try ten times and sleep 5 seconds in between
        // Also, for some reason cloudant closes the connection without telling us what error occurred,
        // so retry all errors. I think it should send a quota exceeded message as it normally does.
        await retry(
            () => this.insert(toSave),
            10,
            5000,
            logger,
            (e) => true
        );

        return report;
    }

    /**
     * Get the document with the given name/id and creates a minimal report from it
     * @param {string} name the _id field of the document
     * @returns {Promise<*>} the document
     */
    public async get(name: string): Promise<MinimalReport> {
        const result = await super.get(name);
        return MinimalReport.fromJSON(result);
    }
}