/**
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import {TestFixture, TestCase, Expect, AsyncTest, Timeout} from "alsatian";
import {Slack} from "../../../main/services/notification/slack";

@TestFixture("Testing Slack")
export class SlackSpec {

    @TestCase("#test")
    @AsyncTest(".sendSlack")
    @Timeout(1000)
    public async testSendSlack(receiver: string) {
        const handle = Slack.instance.getHandle(receiver);
        await Expect(async () => await handle("Test", "Test")).not.toThrowAsync();
    }

    @TestCase("NotAChannel")
    @AsyncTest(".sendSlack (FAIL)")
    @Timeout(1000)
    public async testSendSlackFail(receiver: string) {
        const handle = Slack.instance.getHandle(receiver);
        await Expect(async () => await handle("Test", "Test")).toThrowAsync();
    }
}