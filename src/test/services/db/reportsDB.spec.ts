/*
 The MIT License (MIT)

 Copyright (c) 2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {AsyncTest, TestFixture, Expect, Timeout} from "alsatian";
import {ReportsDB} from "../../../main/services/db/reportsDB";
import {reportsDB} from "../../../main/services/db/dbs";
import {BasicCorrelation} from "../../../main/analytics/correlation/basicCorrelation";
import {Reason} from "../../../main/analytics/correlation/reason/reason";

@TestFixture("reports db")
export class ReportsDBSpec {

	private readonly testReportDB: ReportsDB = reportsDB();

	@AsyncTest(".getLastReport")
	@Timeout(20000)
	public async testNewest() {

		const newReport = await this.testReportDB.saveReport([
			new BasicCorrelation(
				[],
				1,
				Reason.create("test reason", ["test" + Math.random()], 1)
			)
		]);

		const newest = await this.testReportDB.getLastReport();

		Expect(newest._id).toBe(newReport._id);
		Expect(newest.createdAt.unix()).toBe(newReport.createdAt.unix());

		// we can also fetch by id
		const fetched = await this.testReportDB.get(newest._id);
		Expect(fetched).toEqual(newest);
	}

}