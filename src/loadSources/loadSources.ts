import {Source} from "../main/services/db/source";
import {sourcesDB} from "../main/services/db/dbs";
import {setupLogger} from "../main/config/loggerSetup";
import {getLogger} from "log4js";
import yaml = require("js-yaml");
import fs   = require("fs");

setupLogger();
const logger = getLogger();

/**
 * Pushes sources to the database
 * Cleans database beforehand, so that only sources still included in source.yaml exist in db
 * @param {string} path to read sources from, provided as ".yaml" file
 * @returns {Promise<void>} promise
 */
async function loadSources(path: string) {
    logger.info("Loading sources from " + path);
    const doc = yaml.safeLoad(fs.readFileSync(path, "utf8"));

    const sources: Array<Source> = [];
    // Each key corresponds to the type of source, e.g. Facebook, Reddit...
    for (let key in doc) {
        for (let options of doc[key]) {
            sources.push(new Source(key, options));
        }
    }

    logger.info("Cleaning database...");
    const handles = await sourcesDB().destroyAll();

    logger.info("Deleted " + handles.length + " sources. " +
                "Inserting " + sources.length + " new sources.");
    await sourcesDB().bulkInsert(sources);

    logger.info("Successfully inserted sources");
}

loadSources("sources.yaml")
    .catch((e) => logger.error("Error while loading sources!", e));

/* Example for the format this script is expecting:
%YAML 1.2
---
Facebook:
    - "heisec.de" # Such Source, very WOW!

Reddit:
    - subreddit: "netsec"
      limit: 200

Twitter:
    - screen_name: "McAfee_Labs"
      count: 100

HackerNews:
    -

RSSFeed:
    - "https://krebsonsecurity.com/feed/"

XForce:
    - 14
 */