/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {ProviderContext} from "../dataProvider";
import {DataObject} from "../dataObjects/dataObject";
import {TfidfRelevance} from "../../analytics/relevance/tfidfRelevance";
import {DataProviderDecorator} from "./dataProviderDecorator";
import {keywords} from "../../config/keywords";
import {TokenizerAndStemmer} from "../../util/tokenizerAndStemmer";

/*
 * Decorates the DataObjects with Relevance
 * DataProviders may implement a custom process - if popularity is already set, this decorator will skip it
 */
export class AddTfIdfRelevance<T extends DataObject, O> extends DataProviderDecorator<T, O> {

    private readonly tfidfRelevance: TfidfRelevance<T> = new TfidfRelevance<T>();

    protected async decorate(context: ProviderContext<T>): Promise<T[]> {
        const data = await context();

        const preparedData = this.tfidfRelevance.addDocuments(data);
        const preparedKeywords = TokenizerAndStemmer.prepareKeywords(keywords);
        data.forEach((obj, index) => {
            obj.relevance = this.tfidfRelevance.tfIdfAvg(preparedData[index], preparedKeywords);
        });

        return data;
    }
}