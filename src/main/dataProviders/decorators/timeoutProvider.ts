/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {DataProvider, ProviderContext} from "../dataProvider";
import {DataObject} from "../dataObjects/dataObject";
import {DataProviderDecorator} from "./dataProviderDecorator";

/*
 * Adds a timeout to a provider returning early with an exception if the provider takes too long.
 * It does not however cancel the exceuction of the underlying provider. Its result will simply be ignored.
 */
export class TimeoutProvider<T extends DataObject, O> extends DataProviderDecorator<T, O> {
    public static readonly DEFAULT_TIMEOUT_SECONDS: number = 30;

    public constructor(provider: DataProvider<T, O>, private readonly timeoutSeconds: number = TimeoutProvider.DEFAULT_TIMEOUT_SECONDS) {
        super(provider);
    }

    protected async decorate(context: ProviderContext<T>): Promise<T[]> {
        // use a promise to throw from the setTimeout function
        return new Promise<T[]>((resolve, reject) => {
            setTimeout(() => {
                reject(Error("Timeout reached"));
            }, this.timeoutSeconds * 1000);

            // forward all promise calls
            context()
                .then(result => resolve(result))
                .catch(e => reject(e));
        });
    }
}