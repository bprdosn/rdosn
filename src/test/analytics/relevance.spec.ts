/**
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import {Test, TestFixture, Expect} from "alsatian";
import {Information, PopularityRelevance} from "../../main/analytics/relevance/popularityRelevance";
import {DataObject} from "../../main/dataProviders/dataObjects/dataObject";
import {FacebookPost} from "../../main/dataProviders/dataObjects/facebookPost";
import {HackernewsStory} from "../../main/dataProviders/dataObjects/hackernewsStory";
import {RedditPost} from "../../main/dataProviders/dataObjects/redditPost";
import {RssFeed} from "../../main/dataProviders/dataObjects/rssFeed";
import {Tweet} from "../../main/dataProviders/dataObjects/tweet";
import * as moment from "moment";

/**
 * Checks popularity for tests
 * @param {PopularityRelevance<DataObject>} relevance to test
 * @param {DataObject} obj1 to test
 * @param {DataObject} obj2 to test
 * @returns {*} nothing - ESLint pls...
 */
function checkRelevance(relevance: PopularityRelevance<DataObject>, obj1: DataObject, obj2: DataObject) {
    relevance.prepareInformation([obj1, obj2]);

    Expect(relevance.getRelevance(obj1)).toBeLessThan(0.01);
    Expect(relevance.getRelevance(obj2)).toBeGreaterThan(0.9);

    relevance.prepareInformation([obj2, obj2]);

    Expect(relevance.getRelevance(obj2)).toBeGreaterThan(0.4);
    Expect(relevance.getRelevance(obj2)).toBeLessThan(0.6);
}

// TODO: Remove unnecessary tests/overhead

@TestFixture("Testing FacebookPost Relevance")
export class FacebookPostRelevanceSpec {

    private readonly popularity = new PopularityRelevance<FacebookPost>();
    private readonly fb1 = new FacebookPost({id: "938737727918485504", created_time: 123432,
        message: "Dummy", likes: 0, shares: 0}, [], "Test Facebook user");
    private readonly fb2 = new FacebookPost({id: "124123452352345235", created_time: 324242,
        message: "Dummy2", likes: 20, shares: 20}, [], "Test Facebook user");

    @Test(".getRelevance (FAIL)")
    public testGetRelevanceFail() {
        Expect(() => new PopularityRelevance<FacebookPost>().getRelevance(this.fb1)).toThrow();
    }

    @Test(".avgScores (FAIL)")
    public testAvgScores() {
        Expect(() => new Information<FacebookPost>().avgScores).toThrow();
    }

    @Test(".getRelevance")
    public testGetRelevance() {
        checkRelevance(this.popularity, this.fb1, this.fb2);
    }
}

@TestFixture("Testing HackerNews Relevance")
export class HackernewsStoryRelevanceSpec {

    private readonly popularity = new PopularityRelevance<HackernewsStory>();
    private readonly hn1 = new HackernewsStory({id: "938737727918485504", time: 123432,
        title: "Dummy", score: 0});
    private readonly hn2 = new HackernewsStory({id: "124123452352345235", time: 324242,
        title: "Dummy2", score: 20});

    @Test(".getRelevance (FAIL)")
    public testGetRelevanceFail() {
        Expect(() => new PopularityRelevance<HackernewsStory>().getRelevance(this.hn1)).toThrow();
    }

    @Test(".avgScores (FAIL)")
    public testAvgScores() {
        Expect(() => new Information<FacebookPost>().avgScores).toThrow();
    }

    @Test(".getRelevance")
    public testGetRelevance() {
        checkRelevance(this.popularity, this.hn1, this.hn2);
    }
}

@TestFixture("Testing RedditPost Relevance")
export class RedditPostRelevanceSpec {

    private readonly popularity= new PopularityRelevance<RedditPost>();
    private readonly rp1 = new RedditPost({ data: {id: "938737727918485504", created_utc: 123432,
        text: "Dummy", score: 0}}, "Test subreddit");
    private readonly rp2 = new RedditPost({ data: {id: "123213377279184855042", created_utc: 1223432,
        text: "Dummy2", score: 20}}, "Test subreddit");

    @Test(".getRelevance (FAIL)")
    public testGetRelevanceFail() {
        Expect(() => new PopularityRelevance<RedditPost>().getRelevance(this.rp1)).toThrow();
    }

    @Test(".avgScores (FAIL)")
    public testAvgScores() {
        Expect(() => new Information<FacebookPost>().avgScores).toThrow();
    }

    @Test(".getRelevance")
    public testGetRelevance() {
        checkRelevance(this.popularity, this.rp1, this.rp2);
    }
}


@TestFixture("Testing FacebookPost Relevance")
export class RssFeedRelevanceSpec {

    private readonly popularity= new PopularityRelevance<RssFeed>();

    @Test(".getRelevance (FAIL)")
    public testGetRelevanceFail() {
        Expect(() => new PopularityRelevance<RssFeed>().getRelevance(new RssFeed({link: "h2"}, "Test RSS feed"))).toThrow();
    }

    @Test(".avgScores (FAIL)")
    public testAvgScores() {
        Expect(() => new Information<FacebookPost>().avgScores).toThrow();
    }

    @Test(".getRelevance")
    public testGetRelevance() {
        this.popularity.prepareInformation([new RssFeed({link: "h1"}, "Test RSS feed")]);

        Expect(this.popularity.getRelevance(new RssFeed({link: "h3"}, "Test RSS feed"))).toBe(0.5);
    }
}

@TestFixture("Testing Tweet Relevance")
export class TweetRelevanceSpec {

    private readonly popularity= new PopularityRelevance<Tweet>();
    private readonly tweet1 = new Tweet({id_str: "938737727918485504", created_at: moment("2017-12-07T12:51:29.000"),
        text: "Dummy", user: { id: "H" }, retweet_count: 0, favorite_count: 0}, [], "Test Tweeter");
    private readonly tweet2 = new Tweet({id_str: "124123452352345235", created_at: moment("2017-12-07T13:51:29.000"),
        text: "Dummy2", user: { id: "H" }, retweet_count: 20, favorite_count: 20}, [], "Test Tweeter");

    @Test(".getRelevance (FAIL)")
    public testGetRelevanceFail() {
        Expect(() => new PopularityRelevance<Tweet>().getRelevance(this.tweet1)).toThrow();
    }

    @Test(".avgScores (FAIL)")
    public testAvgScores() {
        Expect(() => new Information<FacebookPost>().avgScores).toThrow();
    }

    @Test(".getRelevance")
    public testGetRelevance() {
        checkRelevance(this.popularity, this.tweet1, this.tweet2);
    }
}