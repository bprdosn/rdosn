/**
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import {ProviderContext, DataProvider} from "./dataProvider";
import {getLogger} from "log4js";
import {env} from "../config/env";
import {XForceCollection} from "./dataObjects/xforceCollection";
import {orThrow} from "../util/orDefault";
import * as moment from "moment";
const xfe = require("xfe");

const logger = getLogger("provider.xforce");

/**
 * Allows Access to the XForce API (https://api.xforce.ibmcloud.com/doc)
 * All communication to the XForce API should be handled by this class
 */
export class XforceProvider implements DataProvider<XForceCollection, number> {
    private static _instance: XforceProvider;

    private readonly client: any;

    /**
     * Initializes the XForce client
     */
    private constructor() {
        const key = orThrow(env().XFORCE_KEY, "Missing XForce key!");
        const password = orThrow(env().XFORCE_PASSWORD, "Missing XForce password!");

        this.client = new xfe(key, password);
    }

    public static get instance(): XforceProvider {
        if (!XforceProvider._instance)
            XforceProvider._instance = new XforceProvider();

        return XforceProvider._instance;
    }

    /**
     * Framework documentation: https://cdn.rawgit.com/ibm-xforce/xfe-node/v0.1.0/doc
     * Detailed Information about the collections part: https://github.com/ibm-xforce/xfe-node/blob/master/app/collections.ts
     * While normally the main routines remove outdated data objects, for performance reasons it is favorable to filter them out directly here
     * @param {number} pastDaysToPull number of days collections should be pulled for
     * @return {Promise<Array<XForceCollection>>} resulting collections
     */
    public async getCollections(pastDaysToPull: number): Promise<Array<XForceCollection>> {
        logger.trace("Getting XForce Collections");

        const response = JSON.parse(await this.client.collections.get({ type: "public" }));
        const collections = response.casefiles.reduce((arr: Array<XForceCollection>, collection: any) => {
            // Filter out collections created more than "pastDaysToPull" days ago
            if(moment(collection.created).add(pastDaysToPull, "d").isSameOrAfter(moment()))
                return arr.concat(new XForceCollection(collection));
            else
                return arr;
        }, []);

        return collections;
    }

    /**
     * @param {number} pastDaysToPull number of days collections should be pulled for
     * @return {ProviderContext<XForceCollection>} resulting collections
     */
    public getContext(pastDaysToPull: number): ProviderContext<XForceCollection> {
        return () => this.getCollections(pastDaysToPull);
    }
}