/**
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import {setupLogger} from "./config/loggerSetup";
import {getLogger} from "log4js";
import {NotificationService} from "./services/notification/notificationService";
import {DataSourceCollection} from "./dataProviders/dataSourceCollection";
import {dataObjectsDB, reportsDB} from "./services/db/dbs";
import {runContinuously} from "./util/runContinuously";
import {analytics} from "./config/analytics";
import {guaranteeUniqueness} from "./services/guarantieUniqueness";
import {DataObject} from "./dataProviders/dataObjects/dataObject";

setupLogger();
const logger = getLogger();

const providers = new DataSourceCollection();
const getData = providers.getContext();

const notificationService = new NotificationService();

const MAX_DATA_OBJECTS = 1000;

/* eslint-disable valid-jsdoc */
/**
 * moved into separate scope to free data when finished
 * @param {() => Promise<Array<DataObject>>} getData the fetch method
 * @returns {Promise<void>} resolves when done storing data
 */
async function gatherAndSaveData(getData: () => Promise<Array<DataObject>>) {
	// 2. gather data
	const data = guaranteeUniqueness(await getData());
	logger.trace("Provider data collected (" + data.length + " elements)");

	// 3. save data
	await dataObjectsDB().bulkUpdate(data);
	logger.trace("Data saved to DB");
}

// Run main program async in background
runContinuously(0, async () => {
    logger.info("Running main program");

    // 1. update sources
    await providers.updateSources();

	// 2. gather data then 3. save data
	await gatherAndSaveData(getData);


    // 4. expire old data - even data that was just saved can be removed
    await dataObjectsDB().removeOutdated();

    // 5. get current data
    let fullData = await dataObjectsDB().getAllDocs();
	// limit data
	fullData = fullData.sort((a, b) => a.created_at.isBefore(b.created_at) ? -1 : 1).splice(1, MAX_DATA_OBJECTS);
	logger.trace("Data read from DB: (" + fullData.length + " elements)");

    // 6. perform analysis
    const correlations = analytics.analyze(fullData);
    logger.trace("Finished analysis, found " + correlations.length + " correlations.");

    // 7 and 8 are executed in parallel (but we wait on both before returning from this function)

    // 7. save report
    const reportSaved = await reportsDB().saveReport(correlations);
	logger.trace("Report saved");

    // 8. inform users
    await notificationService.notify(reportSaved);
    logger.trace("Users notified");
})
	.then(res => logger.info("Program finished", "ran " + res + " times"))
	.catch(reason => logger.error(reason));