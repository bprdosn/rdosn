/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {AsyncTest, TestCase, Expect, TestFixture, Timeout} from "alsatian";
import {LinkResolver} from "../../main/analytics/linkResolver";

@TestFixture("Testing Link Resolver")
export class LinkResolverSpec {

    @TestCase("https://t.co/IVSIjYb5Fy",
        "https://www.heise.de/security/meldung/Sicherheitsluecke-in-HP-Druckern-Firmware-Updates-stehen-bereit-3897679.html")
    @TestCase("https://t.co/pWoZgrB8td",
        "https://www.heise.de/newsticker/meldung/E-Mail-Dienst-ProtonMail-verschluesselt-Adressbuch-3897579.html")
    @TestCase("https://l.facebook.com/l.php?u=https%3A%2F%2Fwww.heise.de%2Fsecurity%2Fmeldung%2FClkScrew-Geheime-Schluessel-mit-der-Brechstange-aus-der-ARM-Trustzone-auslesen-3841424.html&h=ATOUuG8PjbZBIWrgea1fojGV0HzL_9ZbFFd3eEf1YMjQiVv9geVCYOJeiSeb83MeowMiKvVPGTzL--1LEhL4dSbDXZTC0JWxLD5J4q-w2zLgjyNO3UG2-hxEJ8_b2iB_Rcq8MeIYxroPrjyMcBa1eGTonbzvTEFD0xfcRgE2XCeis5th7fyZRZeHqwpAzJ31AsueE7IsTROUq_MEw7LIFMNu9Rnew_PKYGk8RvLQR8k3KvQRtPUGdS9oVVd7wv_k91ZRyI5TB25kCWtK7dhdhqQyBT6X8tjHNSx0wfE6kQuZmKVqW8KFwLGQ",
        "https://www.heise.de/security/meldung/ClkScrew-Geheime-Schluessel-mit-der-Brechstange-aus-der-ARM-Trustzone-auslesen-3841424.html")
    @TestCase("https://l.facebook.com/l.php?u=http%3A%2F%2Fbit.ly%2F2AnlHlk&h=ATOggGPWfBVxmWihFmtQi6RXF7k9jXa0rQbp8TevQzWjEZcGQsbVELIGpKin90P4HR2xGEPhyF1ONzuElmi-rvSp8b6pJm81yhr6arpfeT76DjeRKxmFGs5NujbDjO6NdixfXmXZ0CykqWWj2dwy4eK4Tf9pp7vugdzVtvgsW2COrsf1osQ2Cd0iCNXobgd9I9OOXFRJao4Yq-_HU7VUfLKC78zSQRzO5syp_b2W5YC_LGz6lEZtvlyNOivV7HKDTAy4MN1oB26mDDYOxOr5zc2IdSW9eRHr--5HDc4d5JmQjjSK1SKlUx9AGSE",
        "https://blog.0day.rocks/catching-phishing-using-certstream-97177f0d499a")
    @TestCase("https://www.heise.de", "https://www.heise.de")
    @AsyncTest(".resolveLink")
    @Timeout(2000)
    public async resolveLink(link: string, expected: string) {
        const resolved = await LinkResolver.resolveLink(link);

        Expect(resolved).toBe(expected);
    }


    @TestCase("https://t.co/ThisLinkDoesNotExists")
    @TestCase("https://l.facebook.com/THISLINKDOESNOTEXIST")
    @AsyncTest(".resolveLink(FAIL)")
    @Timeout(2000)
    public async resolveTwitterLinkFail(invalidLink: string) {
        const resolved = await LinkResolver.resolveLink(invalidLink);
        // no harm is done to the URL
        Expect(resolved).toEqual(invalidLink);
    }
}