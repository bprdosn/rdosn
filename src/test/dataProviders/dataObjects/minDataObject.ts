/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {DataObject} from "../../../main/dataProviders/dataObjects/dataObject";
import * as moment from "moment";
import {ProviderInfo} from "../../../main/dataProviders/dataObjects/providerInfo";
import {isNullOrUndefined} from "util";

// Use this dummy class for testing since DataObject is abstract
export class MinDataObject extends DataObject {

	public constructor(
		_id: string, content_short: string | undefined = "", content_long: string | undefined = "",
		created_at: moment.Moment = moment(), links: Array<string> = [], origURL: string = "",
		popularity?: number, relevance?: number
	) {
		super(_id, content_short, content_long, created_at, links, origURL, new ProviderInfo("MinDataObject", "TEST"));
		if (!isNullOrUndefined(popularity)) super.popularity = popularity;
		if (!isNullOrUndefined(relevance)) super.relevance = relevance;
	}


    /* istanbul ignore next */
	public get popularityAttributes() {
		return [];
	}
}