/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {BinaryCorrelationAnalytics} from "./binaryCorrelationAnalytics";
import {WeightedCompositeCorrelation} from "../weightedCompositeCorrelation";
import {DataObject} from "../../../dataProviders/dataObjects/dataObject";
import {WeightedCorrelation} from "../weightedCorrelation";


/**
 * Simple type with an analyzer and its weight
 */
export type WeightedBinaryCorrelationAnalytics = {
    analytics: BinaryCorrelationAnalytics;
    /**
     * How much this analyzer should contribute to the result.
     * For example returning the parameter directly would mean if a correlation was found it is
     * important but if none were found we don't care
     * @param {number} correlationFactor between 0 (nothing) and 1 (full)
     * @return {number} thew weight
     */
    weight: (correlationFactor: number) => number;
};

/**
 * Combines multiple binary analytics into one while weighting the different results.
 */
export class WeightedCompositeBinaryAnalytics extends BinaryCorrelationAnalytics {

    public constructor(private readonly binaryAnalytics: Array<WeightedBinaryCorrelationAnalytics>) {
        super();
    }

    public prepare(data: Array<DataObject>): void {
        this.binaryAnalytics.forEach(ba => ba.analytics.prepare(data));
    }

    public finish() {
        this.binaryAnalytics.forEach(ba => ba.analytics.finish());
    }

    /**
     * Calculates correlation between 2 DataObjects using Keyword and Link Analytics
     * @param {DataObject} obj1 first Object to analyze
     * @param {DataObject} obj2 second object to analyze
     * @returns {WeightedCompositeCorrelation} with proportional weights
     */
    public analyze(obj1: DataObject, obj2: DataObject): WeightedCompositeCorrelation {

        const correlations: Array<WeightedCorrelation> = this.binaryAnalytics.map(ba => {
            const correlation = ba.analytics.analyze(obj1, obj2);
            //TODO maybe we should filter based on the factor here
            const factor = ba.weight(correlation.correlationFactor);
            return new WeightedCorrelation(correlation, factor);
        });

        return new WeightedCompositeCorrelation(correlations);
    }
}

