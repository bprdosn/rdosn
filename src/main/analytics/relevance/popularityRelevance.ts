/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {DataObject} from "../../dataProviders/dataObjects/dataObject";
import * as moment from "moment";
import {sigmoid} from "../../util/math";

/**
 * Represents Information needed for an popularity analysis
 * Type T is needed so only DataObjects of one kind can get in
 */
export class Information<T extends DataObject> {
    private data: Array<number> = [];
    private post_count: number = 0;

    public readonly created_at: moment.Moment;

    public constructor() {
        this.created_at = moment();
    }

    /**
     * Processes objects to build up the information needed for an popularity analysis
     * @param {DataObject} obj to process
     * @returns {*} nothing, but ESLint wont believe me...
     */
    public processData(obj: T): void {
        if(this.post_count < 1)
            obj.popularityAttributes.forEach((val, index) => this.data[index] = 0);

        obj.popularityAttributes.forEach((val, index) => this.data[index] += val.value);
        this.post_count++;
    }

    public get avgScores(): Array<number> {
        if(this.post_count < 1)
            throw new TypeError("Cant calculate average scores with no data given!");

        return this.data.map((val) => { return val / this.post_count; } );
    }
}

// TODO: Consider giving a warning if prepareInformation has not been called in quite some time
// Type T is needed so only DataObjects of one kind can get in
// To achieve a separation between multiple posting users, use an array of Relevance Objects
export class PopularityRelevance<T extends DataObject> {

	/* The exclamation mark is the "Definite Assignment Assertion".
	 * This tells the compiler we will manually assign it a value (as we do in prepareInformation).
	 */
    private information!: Information<T>;

    /**
     * Prepares the information needed to perform the popularity calculations
     * Therefor, always call this at least once before calculating the popularity
     * @param {Array<DataObject>} data to draw the information from
     * @returns {*} nothing
     */
    public prepareInformation(data: Array<T>): void {
        this.information = new Information<T>();
        data.forEach((d) => this.information.processData(d));
    }

    /**
     * Measures the popularity of a DataObject, unified across platforms
     * @param {T} obj to calculate the popularity for
     * @returns {number} popularity expressed as a number between 0 and 1, 1 being maximal relevant and 0.5 having average popularity
     */
    public getRelevance(obj: T): number {
        const scores = this.information.avgScores;

        let relevance = 0;
        obj.popularityAttributes.forEach((val, index) => {
            relevance += (val.value / scores[index]) * val.weight;
        });

        return sigmoid(relevance * 5 - 5);
    }
}