/**
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


import {DataObject} from "./dataObject";
import {isNullOrUndefined} from "util";
import {orDefault} from "../../util/orDefault";
import * as moment from "moment";
import {ProviderInfo} from "./providerInfo";

// This class should represent the relevant attributes of a single tweet
export class XForceCollection extends DataObject {

    // Collection attributes shared/writable may be added when needed
    public readonly owner: {
        name: "string";
        userid: string;
        verified: string;
    };
    public readonly votes: number;

    /**
     * Creates a new XForceCollection
     * @param {*} collection returned from the XForce API (untyped)
     */
    public constructor(collection: any) {
        super("XF" + collection.caseFileID, collection.title, collection.tags.reduce((str: string, val: string) => str + " " + val, ""),
              moment(collection.created), orDefault(collection.links, []),
              "https://exchange.xforce.ibmcloud.com/collection/" + collection.caseFileID, new ProviderInfo("XForce", collection.owner.name));

        this.owner = collection.owner;
        this.votes = orDefault(collection.totalVotes, 0);
    }

    /**
     * Provides an interface for the popularity calculations
     * In this case, the only popularity attributes are whether the user is verified or not as well as the "votes" count
     * Many collections don not get any votes at all and many users are not verified
     * Since this should not make the difference between 0 and 1, it is weighted between 0.25 and 1
     * @returns {*} list of attributes that are needed for the popularity calculations, with weight being between 0 and 1 and all weights adding up to 1
     */
    public get popularityAttributes(): Array<{ value: number, weight: number }> {
        return [{ value: 1, weight: 0.25 }, { value: this.isOwnerVerified ? 1 : 0, weight: 0.25 }, { value: this.votes, weight: 0.5 }];
    }

    public get isOwnerVerified(): boolean {
        return !isNullOrUndefined(this.owner.verified);
    }
}