/*
 The MIT License (MIT)

 Copyright (c) 2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {retry} from "../../util/retry";
import {getLogger} from "log4js";

const logger = getLogger("Quota");

/* eslint-disable valid-jsdoc */ //TODO fix jsdoc
/**
 * Creates a proxy around the method call that can retry the method (waiting in between calls).
 * This is used to overcome the limit by cloudant of 5 queries per second
 * Uses a method decorator factory
 * @param {number} waitMilliseconds how long to wait between calls (in ms)
 * @param {number} maxRetries how often to retry the call.
 * @return {(target: any, propertyKey: string, descriptor: PropertyDescriptor) => PropertyDescriptor} factory for the decorator
 */
export function WaitForQuota(waitMilliseconds: number = 5000, maxRetries: number = 2) {
	return function(target: any, propertyKey: string, descriptor: PropertyDescriptor) {

		const method = descriptor.value;

		descriptor.value = function(...args: any[]) {
			// safe this so we can use it in the arrow function
			const self = this;
			// retry the method call
			return retry(
				() => method.apply(self, args),
				maxRetries,
				waitMilliseconds,
				logger,
				// only retry if the error is a quota error
				(e) => e.message.startsWith("You've exceeded your current limit")
			);
		};

		return descriptor;

	};
}