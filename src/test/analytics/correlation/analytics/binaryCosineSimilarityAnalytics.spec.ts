/**
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {DataObject} from "../../../../main/dataProviders/dataObjects/dataObject";
import {Expect, Test, TestCase, TestFixture} from "alsatian";
import {BinaryCosineSimilarityAnalytics} from "../../../../main/analytics/correlation/analytics/binaryCosineSimilarityAnalytics";
import {Reason} from "../../../../main/analytics/correlation/reason/reason";
import {MinDataObject} from "../../../dataProviders/dataObjects/minDataObject";

@TestFixture("Testing BinaryCosineSimilarityAnalyticsSpec")
export class BinaryCosineSimilarityAnalyticsSpec {

	@TestCase(new MinDataObject("1", "IPhoNe EXPLOIT bei APPLE?", undefined, undefined, [], undefined, undefined),
		new MinDataObject("2", "IPhoNe EXPLOIT bei APPLE?", undefined, undefined, [], undefined, undefined), 0.99) //Same titles should lead to almost "1"
	@TestCase(new MinDataObject("3", "IPhoNe EXPLOIT bei APPLE?", undefined, undefined, [], undefined, undefined),
		new MinDataObject("4", "OMG WTF! APplE! iphone? exploit!", undefined, undefined, [], undefined, undefined), 0.7) //Same keywords should lead to almost "1", but since there are some other words too, should be near 0.75 (little lower to be exact)
	@Test("Cosine moreThan correlationFactor .analyze")
	public testMoreAnalyze(obj1: DataObject, obj2: DataObject, expected: number) {
		const analytics = new BinaryCosineSimilarityAnalytics();
		analytics.prepare([obj1, obj2]);
		Expect(analytics.analyze(obj1, obj2).correlationFactor).toBeGreaterThan(expected);
		analytics.finish();
	}

	@TestCase(new MinDataObject("1", "IPhoNe?", undefined, undefined, [], undefined, undefined),
		new MinDataObject("2", "IPhoNe EXPLOIT bei APPLE?", undefined, undefined, [], undefined, undefined), 0.5) //Same but just 1 Keyword (no links) should be less than 0.5
	@TestCase(new MinDataObject("3", "IPhoNe EXPLOIT bei APPLE?", undefined, undefined, [], undefined, undefined),
		new MinDataObject("4", "OMG WTF! SAMsung! LG? in AndroiD!", undefined, undefined, [], undefined, undefined), 0.01) //Totally different keywords should lead to a "0"
	@TestCase(new MinDataObject("5", "nothing in here", undefined, undefined, [], undefined, undefined),
		new MinDataObject("6", "text without keywords", undefined, undefined, [], undefined, undefined), 0.01) //Totally different keywords should lead to a "0"
	@Test("Cosine lessThan correlationFactor .analyze")
	public testLessAnalyze(obj1: DataObject, obj2: DataObject, lessThan: number) {
		const analytics = new BinaryCosineSimilarityAnalytics();
		analytics.prepare([obj1, obj2]);
		Expect(analytics.analyze(obj1, obj2).correlationFactor).toBeLessThan(lessThan);
		analytics.finish();
	}

	@TestCase(new MinDataObject("1", "IPhoNe EXPLOIT bei APPLE?", undefined, undefined, [], undefined, undefined),
		new MinDataObject("2", "OMG WTF! APplE! iphone? exploit!", undefined, undefined, [], undefined, undefined), ["iphone", "apple", "exploit"])
	@TestCase(new MinDataObject("3", "IPhoNe EXPLOIT bei APPLE?", undefined, undefined, [], undefined, undefined),
		new MinDataObject("4", "OMG WTF! SAMsung! LG? in AndroiD!", undefined, undefined, [], undefined, undefined), [])
	@TestCase(new MinDataObject("5", "IPhoNe EXPLOIT bei APPLE?", undefined, undefined, [], undefined, undefined),
		new MinDataObject("6", "OMG WTF! exPloit! LG? in AndroiD!", undefined, undefined, [], undefined, undefined), ["exploit"])
	@TestCase(new MinDataObject("7", "apple EXPLOIT bei APPLE?", undefined, undefined, [], undefined, undefined),
		new MinDataObject("8", "OMG WTF! exPloit! apple? in Apple!", undefined, undefined, [], undefined, undefined), ["apple", "exploit"]) //check uniqueness
	@Test("Cosine reason .analyze")
	public testReasonAnalyze(obj1: DataObject, obj2: DataObject, expected: Array<string>) {
		const analytics = new BinaryCosineSimilarityAnalytics();
		analytics.prepare([obj1, obj2]);
		const result = analytics.analyze(obj1, obj2);
		const reasons: Array<Reason> = Object.keys(result.reasons).map(type => result.reasons[type]);
		Expect(reasons.map(r => r.toString()).toString()).toBe(expected.toString());
		analytics.finish();
	}
}

