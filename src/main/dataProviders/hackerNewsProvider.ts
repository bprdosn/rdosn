/**
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import {ProviderContext, DataProvider} from "./dataProvider";
import {HackernewsStory} from "./dataObjects/hackernewsStory";
import {PopularityRelevance} from "../analytics/relevance/popularityRelevance";
import * as requestPromise from "request-promise-native";
import {getLogger} from "log4js";

const logger = getLogger("provider.hackernews");

// Provides async access to the firebase endpoint of Hackernews
// see here for more info: https://github.com/HackerNews/API
export class HackerNewsProvider implements DataProvider<HackernewsStory, void> {
    private static _instance: HackerNewsProvider;

    private readonly API = "https://hacker-news.firebaseio.com/v0/";

    public readonly relevance: PopularityRelevance<HackernewsStory> = new PopularityRelevance<HackernewsStory>();

    private constructor() { }

    public static get instance(): HackerNewsProvider  {
        if(!HackerNewsProvider._instance)
            HackerNewsProvider._instance = new HackerNewsProvider();

        return HackerNewsProvider._instance;
    }

    public async getData(): Promise<HackernewsStory[]> {
        logger.trace("Getting recent stories");
        const ids: number[] = await this.getRecentStoryIds();
        return await this.getStories(ids);
    }

    /**
     * @returns {ProviderContext} use this and pull data via context.fetchData
     */
    public getContext(): ProviderContext<HackernewsStory> {
        return () => this.getData();
    }

    public getRecentStoryIds(): Promise<Array<number>> {
        return this.get("newstories")
            .then(value => value as Array<number>);
    }

    /**
     * Pulls a single hackernews Story
     * @param {number} id of story to pull
     * @returns {Promise<HackernewsStory>} of the story, but the popularity property is not set!
     */
    public getStory(id: number): Promise<HackernewsStory | null> {
        return this.get("item/" + id).then(value => {
            if (value == null) {
                // this probably means the story was deleted
                logger.trace("The Hackernews story with id " + id + " returned " + value + ". This probably means it was deleted. It will be removed from the result.");
                return null;
            } else {
                return new HackernewsStory(value);
            }
        });
    }

    /**
     * Gets all stories with matching ids from the API. If an id results in a null returned it is not added to the list.
     * @param {number[]} ids if ids contain duplicates, the result will contain duplicates as well
     * @returns {Promise<HackernewsStory[]>} Corresponding stories. No guarantee is made about the order.
     */
    private async getStories(ids: Array<number>): Promise<HackernewsStory[]> {
        const storiesPromises = ids.map(id => this.getStory(id), this);
        const stories_unfiltered: (HackernewsStory | null)[] = await Promise.all(storiesPromises);

        return stories_unfiltered.filter(story => story != null) as Array<HackernewsStory>;
    }

    /**
     * Performs a get request on the firebase api
     * @param {string} path relative path, the base url and the extension (.json) are added
     * @returns {Promise<*>} wraps the request in a Promise. Also parses the JSON
     */
    private get(path: string): Promise<any> {
        const url = this.API + path + ".json";
        return requestPromise.get(url).then(body => JSON.parse(body));
    }
}