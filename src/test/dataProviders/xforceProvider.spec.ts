/**
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import {AsyncTest, TestCase, TestFixture, Expect, Timeout} from "alsatian";
import {XforceProvider} from "../../main/dataProviders/xforceProvider";
import * as moment from "moment";

@TestFixture("Testing XForce Provider")
export class XForceProviderSpec {

    @TestCase(0)
    @TestCase(10)
    @TestCase(50)
    @AsyncTest(".getCollections")
    @Timeout(5000) // May take five seconds
    public async testGetCollections(pastDaysToPull: number) {
        const collections = await XforceProvider.instance.getContext(pastDaysToPull)();

        // No Collection should be older than "pastDaysToPull" days
        collections.forEach((collection) => {
            Expect(collection.created_at.add(pastDaysToPull, "d").isSameOrAfter(moment()));
        });

        // In the last month there should be at least one collection to pull
        if(pastDaysToPull > 30)
            Expect(collections).not.toBeEmpty();
    }
}
