import {AsyncTest, TestFixture, Expect, Timeout, TestCase} from "alsatian";
import {RedditProvider} from "../../main/dataProviders/redditProvider";
import {RedditPost} from "../../main/dataProviders/dataObjects/redditPost";

@TestFixture("Testing reddit Provider")
export class RedditProviderSpec {

	@AsyncTest(".authenticate")
	@Timeout(4000) // May take four seconds
	public async testAuthenticate() {
        const response: any = await RedditProvider.instance.authenticate();

        // The list of posts should not be empty
        Expect(response).toBeDefined();
	}

	@TestCase()
	@TestCase("netsec", 5)
	@TestCase("netsec", 25)
	@TestCase("rawjs", 5)
	@TestCase("rawjs", 25)
	@AsyncTest(".getHot")
	@Timeout(4000) // May take four seconds
	public async testGetHot(subReddit: string = "netsec", limit = 2) {
        const response: Array<RedditPost> = await RedditProvider.instance.getHot(subReddit, limit);

        // The list of posts should not be empty
        Expect(response).toBeDefined();

        // Each valid post has kind="t3"
        for (let post of response) {
            Expect(post.kind).toEqual("t3");
            Expect(post.content_short).not.toBeNull();
        }
	}

	@AsyncTest(".getHot(FAIL)")
	@Timeout(4000) // May take four seconds
	public async testGetHotFail() {
		try {
			const response: Array<RedditPost> = await RedditProvider.instance.getHot("thisisnotavalidsubreddit", 1);

			// The list of posts should be empty
			Expect(response).not.toBeDefined();
		} catch (e) {
			// The Promise should be rejected
			Expect(e).not.toBeNull();
		}
	}

    @TestCase()
    @TestCase("netsec", 5)
    @TestCase("netsec", 25)
    @TestCase("rawjs", 5)
    @TestCase("rawjs", 25)
    @AsyncTest(".getData reddit")
    @Timeout(4000) // May take four seconds
    public async testGetData(subreddit: string = "netsec", limit = 2) {
        const response = await RedditProvider.instance.getContext({subreddit, limit})();

        // The list of posts should not be empty
        Expect(response).toBeDefined();

        // Each valid post has kind="t3"
        for (let post of response) {
            Expect(post.kind).toEqual("t3");
            Expect(post.content_short).not.toBeNull();
        }
    }

	@TestCase()
	@TestCase("netsec", 5)
	@TestCase("netsec", 25)
	@TestCase("rawjs", 5)
	@TestCase("rawjs", 25)
	@AsyncTest(".getNew")
	@Timeout(4000) // May take four seconds
	public async testGetNew(subReddit: string = "netsec", limit = 2) {
        const response: Array<RedditPost> = await RedditProvider.instance.getNew(subReddit, limit);

        // The list of posts should not be empty
        Expect(response).toBeDefined();

        // Each valid post has kind="t3"
        for (let post of response) {
            Expect(post.kind).toEqual("t3");
            Expect(post.content_short).not.toBeNull();
        }
	}

	@AsyncTest(".getData(FAIL)")
	@Timeout(4000) // May take four seconds
	public async testGetDataFail() {
		try {
			const response: Array<RedditPost> = await RedditProvider.instance.getData({subreddit: "thisisnotavalidsubreddit", limit: 1});

			// The list of posts should be empty
			Expect(response).not.toBeDefined();
		} catch (e) {
			// The Promise should be rejected
			Expect(e).not.toBeNull();
		}
	}

    @AsyncTest(".getNew(FAIL)")
    @Timeout(4000) // May take four seconds
    public async testGetNewFail() {
        try {
            const response: Array<RedditPost> = await RedditProvider.instance.getNew("thisisnotavalidsubreddit", 1);

            // The list of posts should be empty
            Expect(response).not.toBeDefined();
        } catch (e) {
            // The Promise should be rejected
            Expect(e).not.toBeNull();
        }
    }

	@TestCase()
	@TestCase("netsec", 5)
	@TestCase("netsec", 25)
	@TestCase("rawjs", 5)
	@TestCase("rawjs", 25)
	@AsyncTest(".getTop")
	@Timeout(4000) // May take four seconds
	public async testGetTop(subReddit: string = "netsec", limit = 2) {
        const response: Array<RedditPost> = await RedditProvider.instance.getTop(subReddit, limit);

        // The list of posts should not be empty
        Expect(response).toBeDefined();

        // Each valid post has kind="t3"
        for (let post of response) {
            Expect(post.kind).toEqual("t3");
            Expect(post.content_short).not.toBeNull();
        }
	}


	@AsyncTest(".getTop(FAIL)")
	@Timeout(4000) // May take four seconds
	public async testGetTopFail() {
		try {
			const response: Array<RedditPost> = await RedditProvider.instance.getTop("thisisnotavalidsubreddit", 1);

			// The list of posts should be empty
			Expect(response).not.toBeDefined();
		} catch (e) {
			// The Promise should be rejected
			Expect(e).not.toBeNull();
		}
	}

	@TestCase()
	@TestCase("netsec", 5)
	@TestCase("netsec", 25)
	@TestCase("rawjs", 5)
	@TestCase("rawjs", 25)
	@AsyncTest(".getControversial")
	@Timeout(4000) // May take four seconds
	public async testGetControversial(subReddit: string = "netsec", limit = 2) {
        const response: Array<RedditPost> = await RedditProvider.instance.getControversial(subReddit, limit);

        // The list of posts should not be empty
        Expect(response).toBeDefined();

        // Each valid post has kind="t3"
        for (let post of response) {
            Expect(post.kind).toEqual("t3");
            Expect(post.content_short).not.toBeNull();
        }
	}


	@AsyncTest(".getControversial(FAIL)")
	@Timeout(4000) // May take four seconds
	public async testGetControversialFail() {
		try {
			const response: Array<RedditPost> = await RedditProvider.instance.getControversial("thisisnotavalidsubreddit", 1);

			// The list of posts should be empty
			Expect(response).not.toBeDefined();
		} catch (e) {
			// The Promise should be rejected
			Expect(e).not.toBeNull();
		}
	}

}
