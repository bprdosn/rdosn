import {Server} from "../web/server";
import {Test, TestFixture, Expect} from "alsatian";

@TestFixture("Server Spec")
class ServerSpec {

    @Test("PageRegistration")
    public testPageRegistration() {
        const initLength = Server["pages"].length;
        const testPage = { route: "Test", requiresAuthentication: false,
                           getHandler: (req: any, res: any) => null,
                           postHandler: (req: any, res: any) => null };

        Server.registerPage(testPage);

        Expect(Server["pages"].length).toBe(initLength + 1);

        // This page should not be added to "pages" since it has the same route
        Server.registerPage(testPage);

        Expect(Server["pages"].length).toBe(initLength + 1);
    }
}