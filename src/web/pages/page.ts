/**-Adding a Page-
 * First, create a folder in "pages", containing usually a .ejs, .ts and .less file, all carrying the name of the page
 * -Add the .less file as an import to styles.less
 * -Register your Page using the Typescript object at the bottom of server.ts, using Server.registerPage(...);
 * -Include partials (head, header, footer) in your .ejs when appropriate
 */
export abstract class Page {
    abstract route: string;
    abstract requiresAuthentication: boolean;

    public getHandler(req: any, res: any): void {
        res.send(403);
    }

    public postHandler(req: any, res: any): void {
        res.send(403);
    }
}