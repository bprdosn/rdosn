/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {isNullOrUndefined, isNumber} from "util";

/**
 * Returns the value or if value is not defined, the default value
 * @param {T | null | undefined} value tested for null or undefined
 * @param {T} defaultValue only returned when value is not defined
 * @returns {T} the value or if value is not defined, the default value. If the value is a number, NaN counts as undefined
 */
export function orDefault<T>(value: T|undefined|null, defaultValue: T): T {
    return isNullOrUndefined(value) || isNumber(value) && isNaN(value) ? defaultValue : value;
}

/**
 * Converts a nullable type into a non nullable one (like ! does), but throws an error if it is not defined
 * @param {T | null | undefined} value tested for null or undefined
 * @param {string} msg of the error when the value is null or undefined
 * @returns {T} guaranteed to be neither null nor undefined
 */
export function orThrow<T>(value: T|undefined|null, msg: string): T {
    if (isNullOrUndefined(value)) {
        throw new TypeError(msg);
    }
    return value;
}