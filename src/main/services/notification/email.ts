/**
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import * as nodemailer from "nodemailer";
import * as Mail from "nodemailer/lib/mailer";
import {Notification} from "./notification";
import {orThrow} from "../../util/orDefault";
import {env} from "../../config/env";

export class Email extends Notification<string> {
    private static _instance: Email;

    private transporter: Mail;
    private email: string;

    private constructor() {
        super();

        this.email = orThrow(env().EMAIL, "Missing Email Credentials (EMAIL)");
        const password = orThrow(env().EMAIL_PW, "Missing Email Credentials (EMAIL_PW");

        // It would be way better if we could use Bluemix SendGrid...
        this.transporter = nodemailer.createTransport({
            service: "gmail",
            auth: {
                user: this.email,
                pass: password
            }
        });
    }

    public static get instance(): Email {
        if(!Email._instance)
            Email._instance = new Email();

        return Email._instance;
    }

    protected send(receiver: string, subject: string, content: string): Promise<nodemailer.SentMessageInfo> {
        const mailOptions = {
            from: this.email,
            to: receiver,
            subject: subject,
            text: content
        };

        return this.transporter.sendMail(mailOptions);
    }
}