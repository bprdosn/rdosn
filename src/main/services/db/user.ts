/**
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import {Email} from "../notification/email";
import {NotificationHandle} from "../notification/notification";
import {Correlation} from "../../analytics/correlation/correlation";
import {Moment} from "moment";
import moment = require("moment");
import DurationConstructor = moment.unitOfTime.DurationConstructor;

export type UserIdentity = { provider: string; id: string };

type UpdateInterval = "INSTANT" | "HOURLY" | "DAILY" | "WEEKLY";

export class User {

    private readonly emailHandle: NotificationHandle;

    public readonly handles: Array<NotificationHandle> = [];

    public readonly _id: string;
    public _rev: string|undefined;

    public constructor(
        public readonly identity: UserIdentity,
        public readonly email: string,
        public readonly receiveEMailNotifications: boolean = false,
        public readonly updateInterval: UpdateInterval = "DAILY",
        private seenCorrelations: Array<string> = [],
        private lastReportTime?: string
    ) {
        this._id = User.getID(this.identity);

        this.emailHandle = Email.instance.getHandle("bp.rdons@gmail.com");
        this.handles.push((subject: string, content: string) => User.sendHandle(this, subject, content));
    }

    public addSeenCorrelations(correlations: Array<Correlation>) {
        correlations.forEach((c) => this.seenCorrelations.push(c.id));
    }

    public hasSeen(correlation: Correlation): boolean {
        return this.seenCorrelations.includes(correlation.id);
    }


    public static getID(identity: UserIdentity): string {
        return identity.provider + "_" + identity.id;
    }

	public static intervalToUnit(interval: UpdateInterval): DurationConstructor {
		switch (interval) {
			case "HOURLY": return "hours";
			case "DAILY": return "days";
			case "WEEKLY": return "weeks";
			default: return "hours";
		}
	}

	/* istanbul ignore next */
	private static async sendHandle(self: User, subject: string, content: string): Promise<any> {
        if (!self.receiveEMailNotifications) return;

        const now = moment();

        if (self.updateInterval != "INSTANT") {
            // check interval
            const canSendAfter: Moment = moment(self.lastReportTime)
                .add(1, User.intervalToUnit(self.updateInterval));

            if (canSendAfter.isBefore(now)) return;
        }

        // we can send now
        self.lastReportTime = now.toISOString();
        return await self.emailHandle(subject, content);

    }
}