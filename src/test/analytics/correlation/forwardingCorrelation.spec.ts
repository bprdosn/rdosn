/*
 The MIT License (MIT)

 Copyright (c) 2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {Expect, Test, TestFixture} from "alsatian";
import {BasicCorrelation} from "../../../main/analytics/correlation/basicCorrelation";
import {MinDataObject} from "../../dataProviders/dataObjects/minDataObject";
import moment = require("moment");
import {Reason} from "../../../main/analytics/correlation/reason/reason";
import {ForwardingCorrelation} from "../../../main/analytics/correlation/forwardingCorrelation";

class DummyForwardingCorrelation extends ForwardingCorrelation {}

@TestFixture("Forwarding correlation")
export class ForwardingCorrelationSpec {

	@Test("forwarding correlation")
	public test() {

		const o1 = new MinDataObject("1", "", "", moment(), [], "");
		const o2 = new MinDataObject("2", "", "", moment(), [], "");
		o2.popularity = 1;
		o1.relevance = 0.5;
		const reason = Reason.create("type", ["a"], 1);
		const c = new BasicCorrelation([o1, o2], 1, reason);
		const f = new DummyForwardingCorrelation(c);

		Expect(f.id).toBe(c.id);
		Expect(f.correlationFactor).toBe(c.correlationFactor);
		Expect(f.avgRelevance).toBe(c.avgRelevance);
		Expect(f.avgRelevance).toBe(0.5);
		Expect(f.avgPopularity).toBe(c.avgPopularity);
		Expect(f.avgPopularity).toBe(1);
		Expect(f.dataObjects).toBe(c.dataObjects);
		Expect(f.reasons).toBe(c.reasons);
	}


}
