/*
 The MIT License (MIT)

 Copyright (c) 2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {DB} from "./db";
import {User, UserIdentity} from "./user";
import {getLogger} from "log4js";

const logger = getLogger("usersDB");

export class UsersDB extends DB {

	public async getOrCreateUser(identity: UserIdentity, email: string): Promise<User> {
		const id = User.getID(identity);
		try {
			return await this.get(id);
		} catch (e) {
			/* istanbul ignore next */
			if(e.statusCode != 404) {
				logger.error("Error while retrieving user from db ", e);
				// we still attempt to create the user below
			}
			// else it was a 404 meaning the user was simply not found.
			// It will be created now.

			const user = new User(identity, email);
			// async
			this.insert(user).catch(
				/* istanbul ignore next */
				e =>  logger.error("Failed to safe user to db ", e)
			);
			return user;
		}
	}

}