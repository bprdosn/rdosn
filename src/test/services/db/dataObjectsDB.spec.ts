/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {AsyncTest, Expect, TestFixture, Timeout} from "alsatian";
import {cloudant} from "../../../main/services/db/dbs";
import {DataObjectsDB} from "../../../main/services/db/dataObjectsDB";
import moment = require("moment");
import {MinDataObject} from "../../dataProviders/dataObjects/minDataObject";

@TestFixture("Testing Data object DB")
export class DataObjectsDBSpec {

	private db = new DataObjectsDB(cloudant(), "test_data-objects");

	@AsyncTest("test old data objects removal")
	@Timeout(10000)
	public async testRemoval() {
		// clear database
		await this.db.destroyAll();

		// insert data
		await this.db.bulkInsert([
			new MinDataObject("RemovalTest1", "content", "long", moment(), [], ""),
			new MinDataObject("RemovalTest2", "content", "long", moment(), [], "")
		]);
		await this.db.insert(new MinDataObject("RemovalTest3", "content", "long", moment(), [], ""));

		// clear old data (10 days)
		await this.db.removeOutdated(10);

		// we still have the two entries
		Expect(await this.db.count()).toBe(3);

		// now expire the data (set expiry days to -1 to expire all data that is older than tomorrow, i.e. everything)
		await this.db.removeOutdated(-1);

		// both of them got expired
		Expect(await this.db.count()).toBe(0);
	}

	@AsyncTest("Test Data object creation")
	@Timeout(10000)
	public async testDataObjectCreation() {
		// create one data object so the test has some data
		await this.db.insert(new MinDataObject(
				"TEST" + moment(),
				"test content",
				"test long",
				undefined,
				undefined,
				"origURL",
				0.7,
				0.5
			));
		const docs = await this.db.getAllDocs();
		Expect(docs.length).toBeGreaterThan(0);
		docs.forEach(doc => Expect(doc.content).toBeDefined());
		docs.forEach(doc => Expect(() => doc.popularityAttributes).toThrow());
	}

}