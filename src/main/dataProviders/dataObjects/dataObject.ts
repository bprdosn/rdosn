/**
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import {isNullOrUndefined} from "util";
import * as moment from "moment";
import {orDefault} from "../../util/orDefault";
import {ProviderInfo} from "./providerInfo";

// This is the general Interface for all Data Objects like a tweet, facebook post...
// Id, as well as content_short and content_long are guaranteed to be defined
// These objects are stored in Cloudant, so no fields can begin with "_" (except _id, _rev and maybe other special fields)
export abstract class DataObject {

    public readonly content_short: string;
    public readonly content_long: string;

    /**
     * popularity of the object, represented as a number between 0 and 1
     * it stays undefined until popularity is computed
     */
    private backing_popularity: number | undefined = undefined;

	/**
	 * relevance of the object, represented as a number between 0 and 1
	 * it stays undefined until relevance is computed
	 */
    private tfidf_relevance: number | undefined = undefined;

    /**
     * Creates a basic DataObject
     * @param {string} _id unique across all platforms and data objects
     * @param {string} content_short of DataObject, like a title
     * @param {string} content_long, may be undefined if there is no long version of the content
     * @param {moment.Moment} created_at date/time when posted - this is the initial time of creation and needs
     *        to stay consistent when the object is updated - FoundCorrelation.id depends on it
     * @param {Array<string>} links present in the DataObject either directly or in the text TODO are these unique or can we have duplicate links? Reason for Link analysis relies on uniqueness atm.
     * @param {string} originURL is the url that links to the original post
     * @param {ProviderInfo} providerInfo where the data object came from
     */
    public constructor(
        public readonly _id: string,
        content_short: string | undefined,
        content_long: string | undefined,
        public readonly created_at: moment.Moment,
        public readonly links: Array<string>,
        public readonly originURL: string,
        public readonly providerInfo: ProviderInfo
    ) {
        if(isNullOrUndefined(this._id) || this._id.trim() == "") throw new TypeError("Every Data Object needs a valid ID!");

        this.content_short = orDefault<string>(content_short, "");
        this.content_long = orDefault<string>(content_long, "");
    }

    /**
     * @returns {string} complete content of DataObject
     */
    public get content(): string {
        return this.content_short + " " + this.content_long;
    }

    /**
     * Relevance gets set after the initial creation of the post
     * @param {number} popularity calculated by Provider
     */
    public set popularity(popularity: number | undefined) {
        if(!isNullOrUndefined(this.backing_popularity))
            throw new TypeError("Popularity of a DataObject may only be set once!");

        if(isNullOrUndefined(popularity) || popularity < 0 || popularity > 1)
            throw new TypeError("Invalid Popularity Value: " + popularity);

        this.backing_popularity = popularity;
    }

    /**
     * @returns {number} popularity or undefined if it has not been set yet
     */
    public get popularity(): number | undefined {
        return this.backing_popularity;
    }

    /**
     * Relevance gets set after the initial creation of the post
     * @param {number} rel popularity calculated by Provider
     */
    public set relevance(rel: number | undefined) {
        if(!isNullOrUndefined(this.tfidf_relevance))
            throw new TypeError("Relevance of a DataObject may only be set once!");

        if(isNullOrUndefined(rel) || rel < 0 || rel > 1)
            throw new TypeError("Invalid Relevance Value: " + rel);

        this.tfidf_relevance = rel;
    }

    /**
     * @returns {number} relevance or undefined if it has not been set yet
     */
    public get relevance(): number | undefined {
        return this.tfidf_relevance;
    }

    /**
     * Provides an interface for the popularity calculations
     * @returns {*} list of attributes that are needed for the popularity calculations, with weight being between 0 and 1 and all weights adding up to 1
     */
    public abstract get popularityAttributes(): Array<{ value: number; weight: number }>;
}