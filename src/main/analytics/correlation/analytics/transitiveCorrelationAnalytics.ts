/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {DataObject} from "../../../dataProviders/dataObjects/dataObject";
import {Correlation} from "../correlation";
import {WeightedCompositeCorrelation} from "../weightedCompositeCorrelation";
import {isNullOrUndefined} from "util";
import {mapPushArray} from "../../../util/arrayMap";
import {BinaryCorrelationAnalytics} from "./binaryCorrelationAnalytics";
import {CorrelationAnalytics} from "./correlationAnalytics";
import {WeightedCorrelation} from "../weightedCorrelation";
import {getLogger} from "log4js";

const logger = getLogger("transitive analysis");

/**
 * Expands a given BinaryCorrelationAnalytics by transitively adding data objects to groups.
 */
//TODO we could add a stricter transitive analyzer that only merges into a group if more than one (or all) objects correlate
export class TransitiveCorrelationAnalytics implements CorrelationAnalytics {

    /* eslint-disable valid-jsdoc */
    /**
     * @param {BinaryCorrelationAnalytics} binaryAnalyzer the underlying binary analysers used
     *                                     for pairwise comparision
     * @param {number} thresholdCorrelationFactor threshold for the correlation factor which
     *                                            every binary Correlation at least needs to have
     * @param {number} thresholdRelevance threshold for the average popularity, which every binary
     *                                    correlation at least needs to have
     * @param {(group: Array<Correlation>) => Array<WeightedCorrelation>} addWeightsToGroup Used for weighting the binary correlations.
     *                          The default assigns every group member a weight of one.
     */
    public constructor(
        private readonly binaryAnalyzer: BinaryCorrelationAnalytics,
        private readonly thresholdCorrelationFactor: number = 0.5,
        private readonly thresholdRelevance: number = 0.4,
        private readonly addWeightsToGroup: (group: Array<Correlation>) => Array<WeightedCorrelation>
            = (group) => group.map(c => new WeightedCorrelation(c, 1))
    ) {}

    /**
     * Analyzes given values transitively by using thresholds for correlation factor and average popularity
     * and a BinaryCorrelationAnalytics to find all correlations between two DataObjects.
     * It returns all found Correlations wrapped in an array.
     * @param {DataObject[]} values array of DataObjects to analyze
     * @returns {Correlation[]} found correlations wrapped in an array
     */
    public analyze(values: DataObject[]): Array<Correlation> {
        logger.trace("Starting analysis of " + values.length + " data objects");
        // give option to analyser of preparing the data
        this.binaryAnalyzer.prepare(values);
        logger.trace("analyzer prepared");
        // get all binary correlations
        const binaryCorrelationsMap: Map<DataObject, Array<Correlation>> = this.findBinaryCorrelations(values);
        logger.trace("binary analysis complete");
        // clean up binary analyser
        this.binaryAnalyzer.finish();
        // transitively combine them
        return this.combineBinaryCorrelations(binaryCorrelationsMap, values);
    }

    private combineBinaryCorrelations(binaryCorrelationsMap: Map<DataObject, Array<Correlation>>, values: DataObject[]): Correlation[] {
        // get a set of all data objects in the map
        //TODO only include items with correlations or all of them? (binaryCorrelationsMap.keys() vs values) If only keys the null or undefined check in findCorrelationGroup can be removed
        const dataObjectsToCheck: Set<DataObject> = new Set(values);

        const correlations: Array<Correlation> = [];

        // DEBUG ---
        const total = dataObjectsToCheck.size;
        const logAt = Math.round(total / 100); // every one percent
        let nextLogPoint = logAt;
        // ---------

        // start a new correlation group for each data object
        for(let groupStarter of dataObjectsToCheck) {

            // for debug remove from set
            dataObjectsToCheck.delete(groupStarter);

            const correlationGroup = this.findCorrelationGroup(
                groupStarter,
                binaryCorrelationsMap,
                dataObjectsToCheck
            );

            if (correlationGroup.length > 0) {
                const weightedCorrelations = this.addWeightsToGroup(correlationGroup);
                const compositeCorrelation = new WeightedCompositeCorrelation(weightedCorrelations);
                correlations.push(compositeCorrelation);
            }

            const count = total - dataObjectsToCheck.size;
            if (count > nextLogPoint) {
                nextLogPoint += logAt;
                logger.trace("Finished " + count + " combines (" + ((count / total) * 100 )+ "%)");
            }
        }

        return correlations;
    }

    /**
     * Expands a group of data objects by following correlations (i.e. apply transitive rule)
     * @param {DataObject} groupStarter where to begin expanding the group
     * @param {Map<DataObject, Correlation[]>} binaryCorrelationsMap where to find the correlations
     * @param {Set<DataObject>} dataObjectsToCheck where to remove all found objects from
     * @return {Correlation[]} a non empty (since groupStarter is in binaryCorrelation) list of
     *                         all found correlations
     */
    private findCorrelationGroup(
        groupStarter: DataObject,
        binaryCorrelationsMap: Map<DataObject, Array<Correlation>>,
       dataObjectsToCheck: Set<DataObject>
    ): Array<Correlation> {

        // the data objects that are in the group
        const dataObjectGroup = new Set([groupStarter]);
        // the correlations that form the group
        const correlationGroup: Set<Correlation> = new Set();

        // use the set like a queue and iterate over it
        for (let currentDataObject of dataObjectGroup) {
            //check current
            const currentCorrelations = binaryCorrelationsMap.get(currentDataObject);
            if (!isNullOrUndefined(currentCorrelations)) {
                // go through all outgoing correlations adding the data objects
                for (let correlation of currentCorrelations) {
                    correlation.dataObjects.forEach(dataObject => {
                        // dataObject is in this group so don't start another group with it
                        dataObjectsToCheck.delete(dataObject);
                        // expand the query for the current group
                        dataObjectGroup.add(dataObject);
                    });

                    // save the correlation for the result
                    correlationGroup.add(correlation);
                }
            }
        }

        // convert to array and return
        return [...correlationGroup];
    }

    /**
     * Run a binary analysis on all pairs
     * @param {DataObject[]} values to run the analysis on
     * @return {Map<DataObject, Array<Correlation>>} all found correlations mapped by all data
     *                                               data objects (that have at least one correlation)
     */
    private findBinaryCorrelations(values: DataObject[]): Map<DataObject, Array<Correlation>> {
        let corMap = new Map<DataObject, Array<Correlation>>();

        // ---- Debug info ----
        let count = 0;
        const total = (values.length * (values.length + 1)) / 2;
        const logAt = Math.round(total / 100); // every one percent
        // --------------------

        // iterate over every unique pair once (order of the pair is not relevant, so (a,b)
        // and (b, a) are not unique
        for (let i = 0; i < values.length; i++) {
            for (let j = i + 1; j < values.length; j++) {
                count++;

                if (count % logAt === 0) logger.trace("Finished " + count + " binary analyses (" + ((count / total) * 100 )+ "%)");

                // check the popularity first before doing an analysis
                const avgPopularity = (values[i].popularity! + values[j].popularity!) / 2;
                if (avgPopularity < this.thresholdRelevance) continue;

                const correlation = this.binaryAnalyzer.analyze(values[i], values[j]);

                if (correlation.correlationFactor >= this.thresholdCorrelationFactor) {
                    // add the correlation to both
                    mapPushArray(corMap, values[i], correlation);
                    mapPushArray(corMap, values[j], correlation);
                }
            }
        }

        logger.trace("Finished binary analysis. Total: " + count);
        return corMap;
    }

}