/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

/**
 * @param {number} x value to calculate sigmoid for
 * @returns {number} value of sigmoid function
 */
export function sigmoid(x: number) {
    return 1 / (1 + Math.exp(-x));
}

/**
 * returns a value between 0 and 1 by using sigmoid. The returned value scales with total
 * @param {number} portion smaller or equal to total
 * @param {number} total larger or equal to portion
 * @param {number} factor positive number which increases results the higher it gets
 * @returns {number} number between 0 and 1
 */
export function scalingProportion(portion: number, total: number, factor = 0): number {
    return sigmoid( (Math.max(Math.log(total), 1) + 9 + factor) * (1 - portion/ total) - 5);
}
