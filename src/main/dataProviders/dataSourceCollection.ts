/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {DataProvider, ProviderContext} from "./dataProvider";
import {DataObject} from "./dataObjects/dataObject";
import {sourcesDB} from "../services/db/dbs";
import {getLogger} from "log4js";
import {limitAsync} from "../util/limitAsync";

const logger = getLogger("data source collection");

/**
 * Container for data sources (ProviderContext)
 */
export class DataSourceCollection implements DataProvider<DataObject, void> {

    private providerContexts: Array<ProviderContext<DataObject>> = [];

    /**
     * Last seen sequence number - used to detect when a new source is added or an existing one has been changed
     * @type {string}
     */
    private last_seq: string = "0";

    /**
     * @returns {ProviderContext<DataObject>} the context for all sources combined
     */
    public getContext(): ProviderContext<DataObject> {
        return () => this.getAll();
    }

    /**
     * Checks for updated Sources in the DB and updates the local list if needed
     * @returns {void}
     */
    public async updateSources() {
        const changes = await sourcesDB().changes(this.last_seq);
        this.last_seq = changes.last_seq;

        // There have been changes since the last update --> reload sources
        if(changes.results.length > 0)
            await this.loadSources();
    }

    /**
     * Fill the list of sources
     * @returns {void}
     */
    private async loadSources() {
        // first get data from db
        const sources = await sourcesDB().getAllDocs();

        // then map that data onto the registered providers
        this.providerContexts = sources.reduce((contexts: Array<ProviderContext<DataObject>>, src) => {

            /* istanbul ignore next */
            try {
                const ctx = src.getContext();
                contexts.push(ctx);
            } catch (e) {
                // ignore the source
                logger.warn("Can't map source to provider. It will not be included.", e);
            }
            return contexts;
        }, []);
    }

    /**
     * Collects the results of all ProviderContexts
     * @return {Promise<DataObject[]>} collected results
     */
    private async getAll(): Promise<DataObject[]> {
        /*
        Some measurements on the limit to the DataObjects. There should be around 4440, less means there were failures/timeouts
        With a 30 second timeout

        limit time    (received)
        1:    4:57.20 (4440 elements)
        2:    2:34.12 (4440 elements)
        3:    2:16.17 (4439 elements)
        4:    2:22.85 (4439 elements)
        5:    1:58.59 (4439 elements) this is the fastest limit without receiving data loss
        6:    1:58.72 (4340 elements)
        7:    1:43.47 (4140 elements)
        8:    1:44.25 (4315 elements)
        1000: 0:55.31 (1265 elements)
         */

        const results = await limitAsync(5, this.providerContexts);
        return results.reduce((a: Array<DataObject>, b: Array<DataObject>) => a.concat(b), []);
    }
}
