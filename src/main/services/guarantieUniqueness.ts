/*
 The MIT License (MIT)

 Copyright (c) 2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {DataObject} from "../dataProviders/dataObjects/dataObject";
import {getLogger} from "log4js";

const logger = getLogger("data object uniqueness");

/**
 * inserts dataObject into target with a new unique id
 * @param {DataObject} dataObject to insert
 * @param {Set<string>} idSet ids already in use
 * @param {Array<DataObject>} target array of already inserted data objects. Will only be added to
 * @returns {*} nothing
 */
function insertUnique(dataObject: DataObject, idSet: Set<string>, target: Array<DataObject>) {
	if (idSet.has(dataObject._id)) {
		// conflict!!
		logger.error("ID conflict detected. It will be resolved automatically, but this should really not happen. Please adjust the responsible DataProvider. id=" + dataObject._id + ", dataObject="+JSON.stringify(dataObject));
		const replaced: any = {...dataObject, _id: dataObject._id + "-I"};
		// recursive in case more collisions happen
		insertUnique(replaced, idSet, target);
	} else {
		idSet.add(dataObject._id);
		target.push(dataObject);
	}
}

/**
 * Replace non unique ids with unique ones
 * @param {Array<DataObject>} data the list to check
 * @return {Array<DataObject>} each _id is unique
 */
export function guaranteeUniqueness(data: Array<DataObject>): Array<DataObject> {

	const idSet: Set<string> = new Set();
	const result: Array<DataObject> = [];

	data.forEach(d => insertUnique(d, idSet, result));

	return result;
}