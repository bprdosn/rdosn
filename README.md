# rdosn

> Risk Detection on Social Networks

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contribute](#contribute)
- [License](#license)

## Background
Created in cooperation with IBM as part of a "Bachelor Praktikum" at TU Darmstadt, Germany.

## Installation and Usage

Detailed discriptions are provided in the [wiki](https://bitbucket.org/bprdosn/rdosn/wiki/).

## Maintainers

[@RDOSNGroup](https://bitbucket.org/bprdosn/g)

## Contribute

See [here](CONTRIBUTING.md).

## License

[MIT � 2017-2018 RDOSN Group](LICENSE.md)