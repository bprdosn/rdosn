/**
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import {ProviderContext, DataProvider} from "./dataProvider";
import {RssFeed} from "./dataObjects/rssFeed";
import {PopularityRelevance} from "../analytics/relevance/popularityRelevance";
import {getLogger} from "log4js";

const feedParser = require("rss-parser");
const logger = getLogger("provider.rss");

// All information parsing related to RSSFeed should be handled by this class
export class RSSFeedProvider implements DataProvider<RssFeed, string> {
    private static _instance: RSSFeedProvider;

    public readonly relevance: PopularityRelevance<RssFeed> = new PopularityRelevance<RssFeed>();

    private constructor() { }

    public static get instance(): RSSFeedProvider  {
        if(!RSSFeedProvider._instance)
            RSSFeedProvider._instance = new RSSFeedProvider();

        return RSSFeedProvider._instance;
    }

    /**
     * @param {string} feedURL page containing the feed
     * @returns {Promise<RssFeed>} current feed as an array wrapped in a promise
     */
    public getData(feedURL: string): Promise<Array<RssFeed>> {
        logger.trace("Getting rss feed " + feedURL);
        return new Promise<Array<RssFeed>>((resolve, reject) => {
            feedParser.parseURL(feedURL, (err: any, parsed: any) => {
                if (err != undefined) {
                    reject(err);
                    return;
                }

                //TODO maybe a better name than the url can be found
                resolve(parsed.feed.entries.map((val: any) => new RssFeed(val, feedURL)));
            });
        });
    }

    /**
     * @param {string} feedURL to pull from
     * @returns {ProviderContext} use this and pull data via context.fetchData
     */
    public getContext(feedURL: string): ProviderContext<RssFeed> {
        return () => this.getData(feedURL);
    }
}
