/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */


import {configure, getLogger} from "log4js";
import {orDefault} from "../util/orDefault";
import {env} from "./env";

/**
 * Either use the LOG_LEVEL from env or log all messages (trace)
 * @returns {string} the log level to use
 */
function getLevel(): string {
    const envLevel = env().LOG_LEVEL;
    return orDefault<string>(envLevel, "trace");
}

/**
 * Which log level to use for the slack appender
 * @returns {string} SLACK_LOG_LEVEL or if not defined default to "error"
 */
function getSlackLevel(): string {
    const slackLevel = env().SLACK_LOG_LEVEL;
    return orDefault<string>(slackLevel, "error");
}

/**
 * Setup the logging
 * @returns {void}
 */
export function setupLogger() {

    configure({
        appenders: {
            // we define to appenders to stdout, a file and slack
            "out": { type: "stdout" },
            "file": { type: "file", filename: "logs/all.log", maxLogSize: 10485760 /*10 MB*/},
            "slack": { type: "slack", token: env().SLACK_ACCESS_TOKEN,
                       channel_id: "errorlogs", username: "system" },
            "slackFiltered" : { type: "logLevelFilter", appender: "slack", level: getSlackLevel() }
        },
        categories: {
            default: {
                appenders: ["out", "file", "slackFiltered"],
                level: getLevel()
            }
        }
    });

    // to set a category (and its sub categories) to a level:
    // getLogger("provider").level = "error";

    const logger = getLogger("config");
    logger.info("Logging setup on level", getLogger().level);
    logger.info("Slack logger is set to level", getSlackLevel());
}