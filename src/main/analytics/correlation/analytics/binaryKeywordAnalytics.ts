/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {DataObject} from "../../../dataProviders/dataObjects/dataObject";
import {TextAnalytics} from "../../textAnalytics";
import {BinaryCorrelationAnalytics} from "./binaryCorrelationAnalytics";
import {BasicCorrelation} from "../basicCorrelation";
import {Keywords} from "../reason/keywords";
import {scalingProportion} from "../../../util/math";
import {keywords} from "../../../config/keywords";

export class BinaryKeywordAnalytics extends BinaryCorrelationAnalytics {

    /**
     * Calculates a correlation between 2 DataObjects based on keywords
     * In it's actual state it just analyses based on the objects short content
     * @param {DataObject} obj1 first Object to analyze
     * @param {DataObject} obj2 second object to analyze
     * @returns {BasicCorrelation} Correlation with a correlationFactor and Reasons
     */
    public analyze(obj1: DataObject, obj2: DataObject): BasicCorrelation {
        // Transform the titles in lower case and split it into words
        const split_title1 = TextAnalytics.extractWords(obj1.content_short.toLowerCase());
        const split_title2 = TextAnalytics.extractWords(obj2.content_short.toLowerCase());

        // Get just the included Keywords (filter this way to have all keywords based on the same strings)
        const keywords1 = keywords.filter((word) =>
            split_title1.includes(word.valueOf()));
        const keywords2 = keywords.filter((word) =>
            split_title2.includes(word.valueOf()));
        let keywords_concat = keywords1.concat(keywords2);

        // Filter out duplicates
        keywords_concat = keywords_concat.filter((elem, index, self) => index === self.indexOf(elem));

        //Get unique same keywords
        let commonWords: Array<string> = keywords1.filter((item: string) => keywords2.indexOf(item.valueOf()) >= 0);

        // differences
        const first_not_in_second = keywords1.filter(item => keywords2.indexOf(item.valueOf()) < 0);
        const second_not_in_first = keywords2.filter(item => keywords1.indexOf(item.valueOf()) < 0);
        const total_diff = first_not_in_second.concat(second_not_in_first);

        // calculating correlation factor
        const correlationFactor: number = (keywords_concat.length == 0) ? 0 :
            scalingProportion(total_diff.length, keywords_concat.length);

        const reason = Keywords(commonWords, correlationFactor);

        return new BasicCorrelation([obj1, obj2], correlationFactor, reason);
    }
}

