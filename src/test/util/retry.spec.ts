/*
 The MIT License (MIT)

 Copyright (c) 2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {AsyncTest, Expect, TestFixture} from "alsatian";
import {retry} from "../../main/util/retry";
import {getLogger} from "log4js";

const logger = getLogger("retry test");

@TestFixture("Testing retry")
export class RetrySpec {

	@AsyncTest("retry success")
	public async testSuccess() {
		const v = "Success";
		const result = await retry(() => v, 0, 0, logger);
		Expect(result).toBe(v);
	}

	@AsyncTest("retry fail")
	public async testFail() {
		await Expect(() => retry(() => {throw Error();}, 2, 0, logger)).toThrowAsync();
	}


	@AsyncTest("retry filtered")
	public async testFiltered() {
		const msg = "abc";
		await Expect(() => retry(
			() => {throw Error(msg);},
			2,
			0,
			logger,
			() => false
		)).toThrowErrorAsync(Error, msg);
	}

	@AsyncTest("retry success after retry")
	public async testSuccessAfterRetry() {
		const v = "Yay!";
		let firstTry = true;
		const result = await retry(
			() => {
				if (firstTry) {
					firstTry = false;
					throw Error();
				}
				return v;
			},
			1,
			0,
			logger
		);
		Expect(result).toBe(v);
	}

}
