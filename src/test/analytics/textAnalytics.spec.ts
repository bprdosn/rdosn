/**
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import {Test, TestFixture, TestCase, Expect} from "alsatian";
import {TextAnalytics} from "../../main/analytics/textAnalytics";

@TestFixture("Testing Text Analysis")
export class TextAnalyticsSpec {

    @TestCase("hallo, Peter;   Ich BINS! D",
        [
            "hallo",
            "Peter",
            "Ich",
            "BINS",
            "D"
        ])
    @TestCase("hallo. der Hans? das bin  !  Ich! D",
        [
            "hallo",
            "der",
            "Hans",
            "das",
            "bin",
            "Ich",
            "D"
        ])
    @TestCase("ends with a dot.", ["ends", "with", "a", "dot"])
    @Test(".extractWords")
    public extractWords(text: string, expected: Array<string>) {
        Expect(TextAnalytics.extractWords(text)).toEqual(expected);
    }


    @TestCase("Here is the first link https://medium.com/@BambouClub/what-will-happen-when-the-shit-hits-the-fan-with-tether-f59f92fd8dca\n" +
        " and another one https://t.co/BtHKr7tnRg", ["https://medium.com/@BambouClub/what-will-happen-when-the-shit-hits-the-fan-with-tether-f59f92fd8dca", "https://t.co/BtHKr7tnRg"])
    @TestCase("Links are only separated by a new line: http://google.de\nhttp://spiegel.de", ["http://google.de", "http://spiegel.de"])
    @TestCase("Links are only separated by a new line: https://google.de\nhttps://spiegel.de", ["https://google.de", "https://spiegel.de"])
    @Test(".extractLinks")
    public extractLinks(text: string, expected: Array<string>) {
        Expect(TextAnalytics.extractLinks(text)).toEqual(expected);
    }
}
