/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {StringMap} from "../../../util/stringMap";
import {unique} from "../../../util/arrayUtil";

/**
 * Encapsulates a reason for a correlation.
 * The class is kept simple on purpose the make storing it as JSON simpler.
 */
export class Reason {

    /**
     *
     * @param {Array<string>} value an arary of reasons. For example a lis of keywords. Can of
     *                              course also be just a single entry. The entries should be unique.
     * @param {number} strength an estimate on how much impact this reason has.
     *                          Can be as large as need be.
     */
    private constructor(
        public readonly value: Array<string>,
        public readonly strength: number
    ) {}

    public toString(): string {
        return this.value.toString();
    }

    /**
     * Convinience method for creating a map with a single entry, namely type mapped to a new reason
     * @param {string} type under which label to store the reason
     * @param {Array<string>} value see Reason constructor
     * @param {number} strength see Reason constructor
     * @return {StringMap<Reason>} map with a single entry
     */
    public static create(type: string, value: Array<string>, strength: number): StringMap<Reason> {
        const m: StringMap<Reason> = {};
        m[type] = new Reason(value, strength);
        return m;
    }

    /**
     * Adds two reasons together making sure the values are still unique.
     * Creates a new object leaving this untouched.
     * @param {Reason} other will not be modified
     * @return {Reason} a new combined reason.
     */
    public combine(other: Reason): Reason {
        return new Reason(
            unique(this.value.concat(other.value)),
            this.strength + other.strength
        );
    }
}