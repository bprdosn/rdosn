/*
 The MIT License (MIT)

 Copyright (c) 2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

// collection of useful array methods

/* eslint-disable valid-jsdoc */
import {isNullOrUndefined} from "util";

/**
 * concat the results of the map function
 * @param {Array<T>} array each element will get mapped
 * @param {(t: T) => Array<S>} map the mapping function
 * @return {Array<S>} merged array
 */
export function flatMap<T, S>(array: Array<T>, map: (t: T) => Array<S>): Array<S> {
	return array.reduce((acc: Array<S>, t: T) => {
		const mapResult: Array<S> = map(t);
		return acc.concat(mapResult);
	}, []);
}

/**
 * removes duplicates
 * @param {Array<T>} array will not be modified
 * @return {Array<T>} new array with no duplicate items
 */
export function unique<T>(array: Array<T>): Array<T> {
	return array.filter((value: T, index: number) => array.indexOf(value) === index);
}

/**
 * Sums up all the numbers in the array
 * @param {Array<number>} array an array of numbers
 * @return {number} the sum of all numbers
 */
export function sum(array: Array<number>): number {
	return array.reduce((sum, a) => sum + a, 0);
}

/**
 * The average of the numbers that are not undefined
 * @param {Array} nums undefined will be ignored
 * @returns {number} average
 */
export function avg(nums: Array<number | undefined>): number {

	if (isNullOrUndefined(nums) || nums.length == 0) return 0;

	const filtered: Array<number> = nums
		.filter(num => !isNullOrUndefined(num))
		.map(num => num!);

	if (filtered.length == 0) return 0;

	return sum(filtered) / filtered.length;
}

/* istanbul ignore next */
/**
 * Checks if two arrays contain the same elements
 * @param {Array<T>} a an array to check
 * @param {Array<T>} b an array to check
 * @return {boolean} true if all alements in a are also in b and vice versa
 */
export function arraysEquals<T>(a: Array<T>, b: Array<T>): boolean {

	if (a === b) return true;
	if (a.length != b.length) return false;


	// sort both then compare so the order does not matter
	a.sort();
	b.sort();

	for (let i = 0; i < a.length; i++) {
		if (a[i] != b[i]) return false;
	}

	return true;

}