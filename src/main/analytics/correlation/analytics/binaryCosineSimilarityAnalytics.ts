/**
 The MIT License (MIT)

 Copyright (c) 2017 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
import {DataObject} from "../../../dataProviders/dataObjects/dataObject";
import {BasicCorrelation} from "../basicCorrelation";
import {PorterStemmer} from "natural";
import {keywords} from "../../../config/keywords";
import {TfidfRelevance} from "../../relevance/tfidfRelevance";
import {Reason} from "../reason/reason";
import {orDefault} from "../../../util/orDefault";
import {BinaryCorrelationAnalytics} from "./binaryCorrelationAnalytics";
import {TokenizerAndStemmer} from "../../../util/tokenizerAndStemmer";
import {isNullOrUndefined} from "util";

const cosine = require("compute-cosine-similarity");

export class BinaryCosineSimilarityAnalytics extends BinaryCorrelationAnalytics {

    private tokenizedAndStemmedData: Map<string, Array<string>> = new Map<string, Array<string>>();
    private tfidf = new TfidfRelevance<DataObject>();

    private preparer: TokenizerAndStemmer = new TokenizerAndStemmer();
    // since keywords don't change, we can compute them once. If they at some point change,
    // move this call to prepare and clear them in finish
    private stemmedKeywords: Array<string> = keywords.map(word => PorterStemmer.stem(word.toLowerCase()));

    public prepare(data: Array<DataObject>) {
        data.forEach(currentDataObject => {
            const pData: Array<string> = this.preparer.prepareDataObject(currentDataObject);
            this.tfidf.addDocument(pData);
            this.tokenizedAndStemmedData.set(
                currentDataObject._id,
                pData
            );
        });
    }

    public finish() {
        this.tokenizedAndStemmedData.clear();
        this.tfidf = new TfidfRelevance<DataObject>();
    }

    public analyze(obj1: DataObject, obj2: DataObject): BasicCorrelation {

        let obj1PreparedData = this.tokenizedAndStemmedData.get(obj1._id),
            obj2PreparedData = this.tokenizedAndStemmedData.get(obj2._id);

        if (obj1PreparedData == undefined || obj2PreparedData == undefined) throw new Error("obj not in prepared Data");

        const tfidfVector1 = this.tfidf.tfIdfVector(obj1PreparedData, this.stemmedKeywords);
        const tfidfVector2 = this.tfidf.tfIdfVector(obj2PreparedData, this.stemmedKeywords);

        const tokenSets = BinaryCosineSimilarityAnalytics.createTokenSets(obj1PreparedData, obj2PreparedData);

        const cosineTfidf = orDefault(cosine(tfidfVector1, tfidfVector2), 0);
        const cosineRaw = orDefault(cosine(tokenSets[0], tokenSets[1]), 0);

        const correlationWeightedAvg = (cosineRaw + 2 * cosineTfidf) / 3;
        const importantKeywords = this.analyzeImportantKeywords(tfidfVector1, tfidfVector2);

        const reason = Reason.create("Cosine similarity with TfIdf", importantKeywords, (cosineRaw + 2 * cosineTfidf) / 3);
        return new BasicCorrelation([obj1, obj2], correlationWeightedAvg, reason);
    }

    private analyzeImportantKeywords(tfidfVector1: Array<number>, tfidfVector2: Array<number>): Array<string> {

        const importantKeywords1 = tfidfVector1.map((item, index) => {
            if (item > 0)
                return keywords[index];
            return undefined;
        }).filter(item => !isNullOrUndefined(item)).map(k => k!);
        const importantKeywords2 = tfidfVector2.map((item, index) => {
            if (item > 0)
                return keywords[index];
            return undefined;
        }).filter(item => !isNullOrUndefined(item)).map(k => k!);

        return importantKeywords1.filter(value => importantKeywords2.indexOf(value) !== -1);
    }

    private static createTokenSets(obj1PreparedData: Array<string>, obj2PreparedData: Array<string>): [Array<number>, Array<number>] {

        let tokenSet1 = new Map<string, number>();
        let tokenSet2 = new Map<string, number>();
        obj1PreparedData.forEach(pData => {
            if (tokenSet1.has(pData)) {
                tokenSet1.set(pData, tokenSet1.get(pData)! + 1);
            } else {
                tokenSet1.set(pData, 1);
                tokenSet2.set(pData, 0);
            }
        });
        obj2PreparedData.forEach(pData => {
            if (tokenSet1.has(pData)) {
                tokenSet2.set(pData, tokenSet2.get(pData)! + 1);
            } else {
                tokenSet1.set(pData, 0);
                tokenSet2.set(pData, 1);
            }
        });

        return [Array.from(tokenSet1.values()), Array.from(tokenSet2.values())];
    }
}