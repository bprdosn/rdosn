/**
 The MIT License (MIT)

 Copyright (c) 2017 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
import {DataObject} from "../../../dataProviders/dataObjects/dataObject";
import {BasicCorrelation} from "../basicCorrelation";
import {Reason} from "../reason/reason";
import {PorterStemmer, WordTokenizer} from "natural";
import {BinaryCorrelationAnalytics} from "./binaryCorrelationAnalytics";

const natural = require("natural");

export class BinaryLevenshteinAnalytics extends BinaryCorrelationAnalytics {

	private tokenizedAndStemmedData: Map<string, Array<string>> = new Map<string, Array<string>>();


	public prepare(data: Array<DataObject>) {
		const tokenizer = new WordTokenizer();
		data.forEach(currentDataObject => {
			const content = currentDataObject.content;
			let data = tokenizer.tokenize(content.toLowerCase())
				.map((word: string) => PorterStemmer.stem(word));
			this.tokenizedAndStemmedData.set(
                currentDataObject._id,
				data
			);
		});
	}

	public finish() {
		this.tokenizedAndStemmedData.clear();
	}

	public analyze(obj1: DataObject, obj2: DataObject): BasicCorrelation {
		const obj1PreparedData = this.tokenizedAndStemmedData.get(obj1._id),
			obj2PreparedData = this.tokenizedAndStemmedData.get(obj2._id);

        /* istanbul ignore next */
		if (obj1PreparedData == undefined || obj2PreparedData == undefined) throw new Error("obj not in prepared Data");

		const distanceRaw = natural.DamerauLevenshteinDistance(obj1PreparedData, obj2PreparedData, {transposition_cost: 0});
		const distance = BinaryLevenshteinAnalytics.considerDocumentLength(
			distanceRaw,
			Math.min(obj1PreparedData.length, obj2PreparedData.length)
		);
		// this probably as to be adjusted together with the considerDocumentLength method
		const offset = 5;
		const correlation = 1 / 2 * Math.tanh(-3 / 2 * (distance - offset)) + 0.5;

		const reason = Reason.create("Levenshtein Distance", ["Distance of the posts on word basis"], correlation);
		return new BasicCorrelation([obj1, obj2], correlation, reason);
	}

	private static considerDocumentLength(distance: number, size: number) {
		// this probably has to be adjusted together with the offset
		const k = 1 + Math.pow(1 / 2, size - 3);
		return k * distance;
	}
}