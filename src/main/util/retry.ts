/*
 The MIT License (MIT)

 Copyright (c) 2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {format} from "util";
import {Logger} from "log4js";
import {sleep} from "./sleep";

/* eslint-disable valid-jsdoc */ //TODO fix jsdoc
/**
 * Util function try a given function a couple of times before throwing an error
 * @param {() => T} f the function to try. If the function throws an error it will be called again.
 * @param {number} maxRetries how often to retry. That means the max number of tries will
 *                 be that value plus one.
 * @param {number} waitMilliseconds how long to wait after a failed attempt
 * @param {Logger} logger where to log warnings about retries.
 * @param isRetryError optional parameter to filter errors. If the function returns false
 *                     then no retry attempt is made and the error thrown. Default is to retry
 *                     all errors.
 * @return {Promise<T>} The result of the function f if it succeeds.
 */
export async function retry<T>(
	f: () => T,
	maxRetries: number,
	waitMilliseconds: number,
	logger: Logger,
	isRetryError: (e: Error) => Boolean = () => true
): Promise<T> {

	let tries = 0;
	let errors = [];
	while (tries <= maxRetries) {
		try {
			const result = await f();
			// at this point the call was a success
			if (tries > 0) {
				// this was not done on the first try, so log it
				logger.warn("Succeeded on try number " + (tries + 1) + ". These errors were thrown: ", errors);
			}
			return result;
		} catch (e) {
			// check that this is an error we should handle
			if (!isRetryError(e)) {
				throw e; // not our responsibility
			}
			// keep track of errors but let program continue
			errors.push(e);
			// maybe wait a bit
			// only wait if attempting another try afterwards (and we should wait at all)
			if (waitMilliseconds > 0 && tries <= maxRetries) {
				await sleep(waitMilliseconds);
			}
		}
		tries++;
	}
	// we use format to inspect errors
	throw Error(format("Exceeded max tries (", tries, "). These errors were thrown: ", errors));

}