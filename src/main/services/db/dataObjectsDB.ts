/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {Cloudant, DB, DocumentHandle} from "./db";
import moment = require("moment");
import {DataObject} from "../../dataProviders/dataObjects/dataObject";
import {orThrow} from "../../util/orDefault";
import {ProviderInfo} from "../../dataProviders/dataObjects/providerInfo";
import {isNullOrUndefined} from "util";

const EXPIRY_TIME = 14;
const EXPIRY_TIMEUNIT = "days";

const dateViewName = "dates";
// this is slightly abusing the view feature: key is rev and value is created_at
const dateView = "function (doc) {if (doc.created_at) {emit(doc._rev, doc.created_at);}}";

class DBDataObject extends DataObject {

	public static fromJSON(json: any): DataObject {
		const d: any = new DBDataObject(
			orThrow<string>(json._id, "Data object missing _id"),
			json.content_short,
			json.content_long,
			moment(orThrow<string>(json.created_at, "Data object missing created_at")),
			orThrow<Array<string>>(json.links, "Data object missing links"),
			orThrow<string>(json.originURL, "Data object missing originURL"),
			orThrow<ProviderInfo>(json.providerInfo, "Data object missing providerInfo"),
		);

		const props = Object.keys(d);

		for (const [key, value] of Object.entries(json)) {
			if (!props.includes(key) || isNullOrUndefined(d[key])) {
				// the key either does not exist or is undefined
				// we can safely add the value
				d[key] = value;
			}
		}

		return d;
	}

	public get popularityAttributes(): Array<{ value: number; weight: number }> {
		throw new Error("Data object from DB does not support popularity attributes");
	}
}

/**
 * Database access with functions specific for data objects
 */
export class DataObjectsDB extends DB {

	private isInitialised = false;

	public constructor(cloudant: Cloudant, name: string) {
		super(cloudant, name);
	}

	public async init() {
		if (this.isInitialised) return;
		this.isInitialised = true;
		await this.createView(dateViewName, dateView);
	}


	public async removeOutdated(expiryDays: number = EXPIRY_TIME) {
		await this.init();

		// get expiry date
		const expiryDate = moment().subtract(expiryDays, EXPIRY_TIMEUNIT);

		// get all data (only the dates)
		const data = await this.view(dateViewName);

		// find expired ones
		const expired = data.filter(d => {
			const created_at = moment(d.value);
			return expiryDate.isAfter(created_at, EXPIRY_TIMEUNIT);
		});

		// rename the key to the expected "rev"
		expired.forEach(d => d.rev = d.key);

		await this.bulkDestroy(expired);
	}

	//TODO the following two methods should maybe map to proper data objects

	// add some typing (TODO not actually doing anything, maybe add generics to db)
	public async getAllDocs(): Promise<Array<DataObject>> {
		const docs = await super.getAllDocs();
		return docs.map(doc => DBDataObject.fromJSON(doc));
	}

	public async getFullDocs(ids: Array<string>): Promise<Array<DataObject>> {
		const docs = await super.fetchDocs(ids, {"include_docs": true});
		return docs.map(d => DBDataObject.fromJSON(d.doc));
	}

	// add last_modified when inserting into the db. update should call insert, so that is covered too.

	public async bulkInsert(docs: Array<DataObject>): Promise<Array<DocumentHandle>> {
		const timestampedDocs = docs.map(DataObjectsDB.addLastModified);
		return super.bulkInsert(timestampedDocs);
	}

	public async insert(doc: DataObject, id?: string): Promise<DocumentHandle> {
		const timestampedDoc = DataObjectsDB.addLastModified(doc);
		return super.insert(timestampedDoc, id);
	}

	private static addLastModified(doc: DataObject): any {
		return {...doc, last_modified: moment()};
	}

}