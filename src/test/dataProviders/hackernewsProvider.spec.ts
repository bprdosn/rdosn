/**
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import {AsyncTest, TestFixture, Timeout, Expect, TestCase} from "alsatian";
import {HackerNewsProvider} from "../../main/dataProviders/hackerNewsProvider";
import {HackernewsStory} from "../../main/dataProviders/dataObjects/hackernewsStory";
import {RetryProvider} from "../../main/dataProviders/decorators/retryProvider";
import * as moment from "moment";
import * as requestPromise from "request-promise-native";
import * as sinon from "sinon";

@TestFixture("Test Hackernews provider")
export class HackernewsProviderSpec {

    @AsyncTest(".getData Hackernews")
    @Timeout(10000) // plenty of time, if a need for quicker times arises change it then
    public async getRecent() {
        // HN is not reliable, so add a retry
        const stories = await new RetryProvider(HackerNewsProvider.instance).getContext({})();
        Expect(stories).toBeDefined();

        // the api says 500 new stories are returned, so lets check that
        // however, some stories might have been deleted, so allow some room
        Expect(stories.length).toBeGreaterThan(490);
        Expect(stories.length).toBeLessThan(501);
        for (const storyPromise of stories) {
            const story = await storyPromise;
            Expect(story).not.toBeNull();
        }
    }

    // some random test stories
    // note the date getting parsed as UTC, same as the story.time
    @TestCase(15712377, "Magic: The Gathering Is Turing Complete (2012)", moment.utc("2017-11-16 13:15:13"))
    @TestCase(15715339, "1Caml – implementation of 1ML targeting the OCaml runtime", moment.utc("2017-11-16 18:49:03"))
    @TestCase(15714920, "Security alerts on GitHub", moment.utc("2017-11-16 18:05:45"))
    @AsyncTest(".getStory")
    @Timeout(10000)
    public async getStory(id: number, title: string, date: moment.Moment) {
        const story: HackernewsStory = await HackerNewsProvider.instance.getStory(id) as HackernewsStory;

        Expect(story.content_short).toBe(title);
        Expect(story.created_at.isSame(date)).toBeTruthy();
    }

    @TestCase(-1)
    @AsyncTest(".getStory null")
    @Timeout(4000)
    public async getStoryNull(id: number) {
        const story = await HackerNewsProvider.instance.getStory(id);

        Expect(story).toBeNull();
    }

    @AsyncTest("No connection throws an exception") // now handled further up in decorator
    @Timeout(10000)
    public async connectionError() {
        // create sandbox to restore the stub later
        const sbox = sinon.sandbox.create();
        // replace the requestPromise.get method with an exception
        sbox.stub(requestPromise, "get").rejects();
        // it should fail
        await Expect(() => HackerNewsProvider.instance.getStory(1)).toThrowAsync();

        sbox.restore();
    }

}