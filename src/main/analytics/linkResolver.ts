/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import * as requestPromise from "request-promise-native";
import {RequestPromiseOptions} from "request-promise-native";
import {URL} from "url";
import {isNullOrUndefined} from "util";
import {getLogger} from "log4js";

const logger = getLogger("analytics.linkResolver");

/**
 * Provides utils to resolve links to their raw form (e.g. for links that got shortened)
 */
export class LinkResolver {

    /*
     * List of handlers for host names
     * The handlers _must_ return an URL on another domain to avoid going in circles
     * If the resolve fails, they should throw an error.
     */
    private static linkResolvers: Map<string, ((link: string) => Promise<string>)> = new Map([
        ["t.co", LinkResolver.resolveTwitterLink],
        ["bit.ly", LinkResolver.getResponseLocation],
        ["ow.ly", LinkResolver.getResponseLocation],
        ["l.facebook.com", LinkResolver.resolveFacebookLink]
    ]);

    /**
     * Tries to resolve to the url. If the link cannot be resolved, either because it is not a valid
     * URL, it already is resolved or the resolving URL is somehow wrong (e.g. deleted short URL),
     * no error is thrown. Instead the URL is returned as it was received
     * @param {string} link a valid URL
     * @returns {Promise<string>} the resolved string
     */
    public static async resolveLink(link: string): Promise<string> {
        try {
            const url = new URL(link);
            // find a resolver with matching host
            const resolver: ((link: string) => Promise<string>) | undefined = this.linkResolvers.get(url.host);
            // if the host has no resolver, return the original link
            if (isNullOrUndefined(resolver)) return link;
            // apply the resolver
            const resolved: string = await resolver(link);
            // In case of nested resolvers (for example facebook -> bitly -> actual link),
            // wrap the result in another resolve. However, this can fail if we go around in circles.
            // Here we only detect whether the same link is returned. Longer circles is not detected.
            // TODO make circle proof, e.g. by adding a max resolves limit
            if (resolved == link) return resolved;
            return this.resolveLink(resolved);
        } catch (e) {
            // we have failed to resolve the link
            // log the error and return the original
            logger.warn(e);
            return link;
        }
    }

    /**
     * If the link is a facebook link it will be resolved to the actual link
     * @param {string} link to resolve (usually a facebook link)
     * @returns {string} the link facebook is supposed to resolve to
     */
    private static async resolveFacebookLink(link: string): Promise<string> {
        // the actual mechanism facebook uses to redirect you checks
        // 1. if the user agent is set (but not to what it is set)
        // 2. if no session cookies are provided, it redirects to a "Are you sure?" web page
        // so we can either somehow create a facebook session, parse the "Are you sure?" page or,
        // as we'll do here, just parse the url. The actual link is passed in the "u" query parameter.
        const url = new URL(link);
        const realLink: string | null = url.searchParams.get("u");
        if (realLink == null) {
            // the resolution failed
            throw "The query parameter 'u' in the facebook url is null: " + link;
        }

        return realLink;
    }

    /**
     * Grabs the resolved link from the 'location' header (assumes a redirect return code was given)
     * Throws an exception when the resolve fails
     * @param {string} link to perform the request on
     * @returns {Promise<string>} the resolved URL
     */
    private static async getResponseLocation(link: string): Promise<string> {
        const options: RequestPromiseOptions = {
            resolveWithFullResponse: true,
            followRedirect: false,
            simple: false // allow errors (we get a "301 Moved Permanently" return code )
        };

        // TODO: find typing
        const response: any = await requestPromise(link, options);

        // get the redirection location
        const resolvedLink = response["headers"]["location"];

        // in case of a 404 or similar the resolved linked may be undefined
        if (resolvedLink == undefined) {
            throw "The resolved link of '" + link + "' is undefined";
        }

        return resolvedLink;
    }

    /**
     * Same as response location, but validates the link first.
     * Every twitter link has a path of exactly ten characters
     * @param {string} link to perform the request on
     * @return {Promise<string>} the resolved URL or the same link if it was invalid
     */
    private static async resolveTwitterLink(link: string): Promise<string> {
        const url = new URL(link);
        if (url.pathname.length != 11) { // 10 + 1 for '/' at the start
            //TODO decide on what logging level to output
            // logger.warn("This is not a twitter link: " + link);
            return link;
        } else {
            return await LinkResolver.getResponseLocation(link);
        }
    }

}