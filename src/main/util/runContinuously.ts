/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {sleep} from "./sleep";

let run: boolean = false;

/* eslint-disable valid-jsdoc */ //I think ESLint can't handle higher order functions
/**
 * Run the function every [time] mins
 * @param {number} time how long to wait between running the function
 * @param {() => void} f the function to run, can be async
 * @returns {Promise<number>} returns a Promise with the number of times the program ran
 */
export async function runContinuously(time: number, f: () => void): Promise<number> {
    let i: number;
    run = true;

    for (i = 0; run; i++) {
        // TODO: add try block to catch all exceptions (and never crash)?
        await f();
        // wait [time] minutes
        await sleep(time * 60 * 1000);
    }

    return i;
}