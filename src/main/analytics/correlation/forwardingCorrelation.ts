/*
 The MIT License (MIT)

 Copyright (c) 2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {Correlation} from "./correlation";
import {DataObject} from "../../dataProviders/dataObjects/dataObject";
import {Reason} from "./reason/reason";
import {StringMap} from "../../util/stringMap";

/**
 * A decorator with no functionality added
 */
export abstract class ForwardingCorrelation implements Correlation {

	public constructor(public readonly correlation: Correlation) {}

	public get id(): string {
		return this.correlation.id;
	}

	public get dataObjects(): Array<DataObject> {
		return this.correlation.dataObjects;
	}

	public get correlationFactor(): number {
		return this.correlation.correlationFactor;
	}

	public get reasons(): StringMap<Reason> {
		return this.correlation.reasons;
	}

	public get avgRelevance(): number {
		return this.correlation.avgRelevance;
	}

	public get avgPopularity(): number {
		return this.correlation.avgPopularity;
	}

}