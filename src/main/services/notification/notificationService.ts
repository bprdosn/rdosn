/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {Slack} from "./slack";
import {getLogger} from "log4js";
import {User} from "../db/user";
import {usersDB} from "../db/dbs";
import {Report} from "../db/report";
import {env} from "../../config/env";
import {orDefault} from "../../util/orDefault";

export class NotificationService {

    private readonly logger = getLogger("notification");

    private readonly slackUser: User;

    public constructor() {
        this.slackUser = new User({ id: "", provider: "" }, "", false);
        this.slackUser.handles.push(Slack.instance.getHandle("#allgemein"));
    }

    /**
     * Notifies all users about new correlations
     * @param {Report} report to inform about
     * @returns {Promise<*>} nothing
     */
    public async notify(report: Report) {
        this.logger.trace("Notifying users");

        // TODO: Users are pulled every time completely from db, maybe keep them in memory and just update them every time?
        let users = await usersDB().getAllDocs();
        users = users.map((u) => new User(u.identity, u.email, u.receiveEMailNotifications,
                                                  u.seenCorrelations));

        if(orDefault<string>(env().SLACK_NOTIFICATIONS, "false") == "true")
            users.push(this.slackUser);
        users.forEach((u) => this.notifyUser(u, report));
    }

    /**
     * Notifies a user about new correlations
     * It will just send notifications about correlations the user has not seen yet
     * @param {User} user to notify
     * @param {Report} report to notify about
     * @returns {void} nothing
     */
    public notifyUser(user: User, report: Report) {
        const filtered = report.correlations.filter((c) => !user.hasSeen(c));

        const reportLink = "https://rdosn.mybluemix.net/report/" + report._id + "/";

        if(filtered.length > 0) {
            user.addSeenCorrelations(filtered);

            let text = "New Notificaitons: \n \n";
            filtered.forEach((c) => {

                const correlationLink = reportLink + c.id;

                text += "BasicCorrelation (BasicCorrelation Factor: " + c.correlationFactor + "): \n";
                text += c.dataObjects[0].content_short + "\n" + c.dataObjects[1].content_short + "\n";
                text += "For more detailed information, visit the report page: " + correlationLink + "\n \n";
            });

            user.handles.forEach((h) => {
                h("New Notification", text)
                    .catch((e) => { throw new TypeError("Failed to notify user! " + JSON.stringify(e)); });
            });
        }
    }
}