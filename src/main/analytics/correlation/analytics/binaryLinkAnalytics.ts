/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {DataObject} from "../../../dataProviders/dataObjects/dataObject";
import {BinaryCorrelationAnalytics} from "./binaryCorrelationAnalytics";
import {BasicCorrelation} from "../basicCorrelation";
import {scalingProportion} from "../../../util/math";
import {unique} from "../../../util/arrayUtil";
import {Links} from "../reason/links";
import {isNullOrUndefined} from "util";
import {orThrow} from "../../../util/orDefault";

export class BinaryLinkAnalytics extends BinaryCorrelationAnalytics {

    private linkMap: Map<string, Array<string>> = new Map<string, Array<string>>();

    /**
     * Calculates a correlation between 2 DataObjects based on similar links
     * @param {DataObject} obj1 first Object to analyze
     * @param {DataObject} obj2 second object to analyze
     * @returns {BasicCorrelation} Correlation with a correlationFactor and Reasons
     */
    public analyze(obj1: DataObject, obj2: DataObject): BasicCorrelation {
        let obj1Links = orThrow(this.linkMap.get(obj1._id),
            "this should never happen, somehow the prepare function does not work correctly");
        let obj2Links = orThrow(this.linkMap.get(obj2._id),
            "this should never happen, somehow the prepare function does not work correctly");

        const uniqueCommonLinks: Array<string> = obj1Links
            .filter((value: string) => obj2Links.indexOf(value) >= 0);

        // concatenated links without duplicates
        const uniqueLinks = unique(obj1Links.concat(obj2Links));

        const first_not_in_second = obj1Links.filter(item => obj2Links.indexOf(item.valueOf()) < 0);
        const second_not_in_first = obj2Links.filter(item => obj1Links.indexOf(item.valueOf()) < 0);
        const total_diff =  first_not_in_second.concat(second_not_in_first);

        // calculating correlation factor
        const correlationFactor: number = (uniqueLinks.length == 0) ? 0 :
            scalingProportion(total_diff.length, uniqueLinks.length, 2);

        const reason = Links(uniqueCommonLinks, correlationFactor);

        return new BasicCorrelation([obj1, obj2], correlationFactor, reason);
    }

    public prepare(dataObjects: Array<DataObject>) {
        dataObjects.forEach(dataObject =>
            this.linkMap.set(dataObject._id, dataObject.links.filter(item => !isNullOrUndefined(item))));
    }

    public finish() {
        this.linkMap.clear();
    }
}