/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {DataObject} from "../dataObjects/dataObject";
import {ProviderContext} from "../dataProvider";
import {getLogger} from "log4js";
import {DataProviderDecorator} from "./dataProviderDecorator";

const logger = getLogger("provider");

/*
 * Catch any errors to log them but passing empty data back instead of throwing the error further.
 */
export class IgnoreErrorProvider<T extends DataObject, O> extends DataProviderDecorator<T, O> {

    protected async decorate(context: ProviderContext<T>, options: O): Promise<T[]> {
        try {
            return await context();
        } catch (e) {
            logger.error("Ignoring error (provider=", this.provider, ", options=", options, "): ", e);
            return [];
        }
    }

}