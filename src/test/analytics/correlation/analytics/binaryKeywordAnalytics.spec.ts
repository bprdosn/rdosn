/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

import {DataObject} from "../../../../main/dataProviders/dataObjects/dataObject";
import {Expect, Test, TestCase, TestFixture} from "alsatian";
import {BinaryKeywordAnalytics} from "../../../../main/analytics/correlation/analytics/binaryKeywordAnalytics";
import {Reason} from "../../../../main/analytics/correlation/reason/reason";

@TestFixture("Testing BinaryKeywordAnalytics")
export class BinaryKeywordAnalyticsSpec {

    @TestCase({ content_short: "IPhoNe EXPLOIT bei APPLE?", links: []},
        { content_short: "IPhoNe EXPLOIT bei APPLE?", links: []}, 0.99) //Same titles should lead to almost "1"
    @TestCase({ content_short: "IPhoNe EXPLOIT bei APPLE?", links: []},
        { content_short: "OMG WTF! APplE! iphone? exploit!", links: []}, 0.99) //Same keywords should lead to almost "1"
    @Test("moreThan correlationFactor .analyze")
    public testMoreAnalyze(obj1: DataObject, obj2: DataObject, expected: number) {
        Expect(new BinaryKeywordAnalytics().analyze(obj1, obj2).correlationFactor).toBeGreaterThan(expected);
    }

    @TestCase({ content_short: "IPhoNe?", links: []},
        { content_short: "IPhoNe EXPLOIT bei APPLE?", links: []}, 0.5) //Same but just 1 Keyword (no links) should be less than 0.5
    @TestCase({ content_short: "IPhoNe EXPLOIT bei APPLE?", links: []},
        { content_short: "OMG WTF! SAMsung! LG? in AndroiD!", links: []}, 0.01) //Totally different keywords should lead to a "0"
    @TestCase({ content_short: "nothing in here", links: []},
        { content_short: "text without keywords", links: []}, 0.01) //Totally different keywords should lead to a "0"
    @Test("lessThan correlationFactor .analyze")
    public testLessAnalyze(obj1: DataObject, obj2: DataObject, lessThan: number) {
        Expect(new BinaryKeywordAnalytics().analyze(obj1, obj2).correlationFactor).toBeLessThan(lessThan);
    }

    @TestCase({ content_short: "IPhoNe EXPLOIT bei APPLE?", links: []},
        { content_short: "OMG WTF! APplE! iphone? exploit!", links: []}, ["iphone", "apple", "exploit"])
    @TestCase({ content_short: "IPhoNe EXPLOIT bei APPLE?", links: []},
        { content_short: "OMG WTF! SAMsung! LG? in AndroiD!", links: []}, [])
    @TestCase({ content_short: "IPhoNe EXPLOIT bei APPLE?", links: []},
        { content_short: "OMG WTF! exPloit! LG? in AndroiD!", links: []}, ["exploit"])
    @TestCase({ content_short: "apple EXPLOIT bei APPLE?", links: []},
        { content_short: "OMG WTF! exPloit! apple? in Apple!", links: []}, ["apple", "exploit"]) //check uniqueness
    @Test("reason .analyze")
    public testReasonAnalyze(obj1: DataObject, obj2: DataObject, expected: Array<string>) {
        const result = new BinaryKeywordAnalytics().analyze(obj1, obj2);
        const reasons: Array<Reason> = Object.keys(result.reasons).map(type => result.reasons[type]);
        Expect(reasons.map(r => r.toString()).toString()).toBe(expected.toString());
    }
}

