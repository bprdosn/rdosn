/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */
import {getLogger} from "log4js";
import {orThrow} from "../../util/orDefault";
import {isNullOrUndefined} from "util";
import {WaitForQuota} from "./waitForQuota";

const logger = getLogger("db");

export type DocumentHandle = {
    id: string;
    rev: string;
};

type DesignDoc = {
    _id: string;
    _rev: string | undefined;
    views: any;
};

export type Cloudant = { // we could add the database methods (create a new db, etc.) when needed here
    db: any;
    request: (opts: any, callback: (err: any, body: any) => void) => void;
};


// default values for design docs
const designDocName = "design";

/**
 * Wrapper for the database. The database here is a Cloudant NoSQL document store.
 * The static methods/fields create and hold database connections.
 */
export class DB {

    /**
     * Indicates whether the view for all docs has been created.
     * The destroyAll resets this variable, the destroy method does not.
     * Thus we have to assume no one will destroy this view directly.
     * WARNING: Inconsistent state can also happen if someone tinkers with the database
     * through a different program / the cloudant dashboard. Restart this app if that happens.
     * @type {boolean}
     */
    private hasAllDocsView: boolean = false;

    /**
     * Holds the instance of the db connection
     */
    protected readonly db: any;

    /**
     * Create a wrapper for a specific table/database
     * @param {Cloudant} cloudant the cloudant connection
     * @param {string} name of the table/database
     */
    public constructor(private readonly cloudant: Cloudant, private readonly name: string) {
        logger.info("Connecting to database '" + name + "'");
        this.db = cloudant.db.use(name);
    }

    /*
     * All the db functions get a tuple returned where the first value is the response body and the
     * second value are the response headers. Since we only need the body we usually use response[0].
     */

    /**
     * List all elements in the db (including design docs)
     * @param {*} params can be used to include document bodies
     * @return {Promise<Array<*>>} as returned by the db
     */
    @WaitForQuota()
    public async list(params: any = {}): Promise<Array<any>> {
        const result = await this.db.list(params);
        return result[0].rows;
    }

    /**
     * Creates DocumentHandles for everything in the database
     * @return {Promise<Array<DocumentHandle>>} result wrapped in DocumentHandle
     */
    public async listAllDocumentHandles(): Promise<Array<DocumentHandle>> {
        const rows = await this.list();
        return DB.mapToDocumentHandle(rows);
    }

    public async count(): Promise<number> {
        // TODO maybe we should create a view, otherwise this seems like a needlessly expensive operation. No calls in production code yet, so low priority
        const all = await this.getAllDocs();
        return all.length;
    }

    /**
     * Insert a document into the database
     * @param {*} doc to insert
     * @param {string | undefined} id if defined, use as the id otherwise generate a new one. The id can also be given with an _id field in the doc.
     * @returns {Promise<DocumentHandle>} the assigned id and revision
     */
    @WaitForQuota()
    public async insert(doc: any, id?: string): Promise<DocumentHandle> {
        if (isNullOrUndefined(id)) id = doc._id; // to be on the safe side, since there might be issues with sending undefined id
        const response = await this.db.insert(doc, id);
        if (response[0]["ok"] != true) throw new Error("Failed to insert document with doc=" + doc + ", id=" + id + ". Response: " + JSON.stringify(response));
        return response[0];
    }

    /**
     * Insert all items in the array
     * @param {Array<*>} docs array of documents. Each should either have an _id or a new one will be generated
     * @returns {Promise<Array<DocumentHandle>>} an array of ids and revs for each document inserted (order should be consistent)
     */
    @WaitForQuota()
    public async bulkInsert(docs: Array<any>): Promise<Array<DocumentHandle>> {
        const response = await this.db.bulk({"docs":docs});

        const handles: Array<DocumentHandle> = response[0];

        // detect errors:
        const errors: Array<any> = handles.filter((handle: any) => !isNullOrUndefined(handle.error));
        if (errors.length > 0) throw Error("bulk insert failed with the following reasons: " + JSON.stringify(errors));

        return handles;
    }

    /**
     * If an entry with the same ID exists, then it is updated. Otherwise a new entry is inserted.
     * @param {*} doc with a mandatory _id field
     * @return {Promise<DocumentHandle>} the assigned id and revision
     */
    public async update(doc: any): Promise<DocumentHandle> {
        // reuse the solution for multiple docs but only pass the one doc in the array
        const newDocs = await this.fetchRevsForDocs([doc]);
        /* istanbul ignore next */
        if (newDocs.length != 1) throw new Error("Somehow we got " + newDocs.length + " docs back, but we only queried for one. doc._id= " + doc._id);
        const newDoc = newDocs[0];
        return this.insert(newDoc);
    }

    /**
     * Updates the docs by first getting the current rev then using that to insert
     * @param {Array<*>} docs with a valid _id
     * @return {Promise<Array<DocumentHandle>>} the ids and revs inserted
     */
    public async bulkUpdate(docs: Array<any>): Promise<Array<DocumentHandle>> {
        const revs = await this.fetchRevsForDocs(docs);
        return await this.bulkInsert(revs);
    }

    /**
     * Fetches for all ids the corresponding docs without the content, set params to {"include_docs": true} to include document content
     * @param {Array<string>} ids to fetch
     * @param {*} params extra arguments see https://console.bluemix.net/docs/services/Cloudant/api/database.html#get-documents
     * @returns {Promise<Array<*>>} list of documents. If a document does not exist instead of a value field an error field is returned
     */
    @WaitForQuota()
    //TODO include_docs: false seems to return the docs anyway
    public async fetchDocs(ids: Array<string>, params: any = {"include_docs": false}): Promise<Array<any>> {
        const result = await this.db.fetch({"keys": ids}, params);
        return result[0].rows;
    }

    /**
     * Fetches the _rev for existing elements and writes it in docsWithoutRevs
     * @param {*} docsWithoutRevs the documents that the revs should be fetched for. Can contain documents that are not yet in the DB
     * @param {string} docsWithoutRevs._id the id of the document. Needed.
     * @returns {Promise<Array<*>>} returns docsWithoutRevs with an added field, "_rev". Existing _rev fields will be overwritten
     */
    public async fetchRevsForDocs(docsWithoutRevs: Array<any>): Promise<Array<any>> {
        const ids: Array<string> = docsWithoutRevs.map((item: any) => {
            if(!item._id) throw new Error("Every doc has to have an _id field!");
            return item._id;
        });

        // fetch all ids + revs then make the result conform to DocumentHandle just to be safe
        const docs = await this.fetchDocs(ids);
        const validDocs = docs
            .filter(doc => !isNullOrUndefined(doc.value)) // this means the doc was not found
            .filter(doc => isNullOrUndefined(doc.value.deleted) || !doc.value.deleted); // don't get revs for deleted objects
        const handles: Array<DocumentHandle> = DB.mapToDocumentHandle(validDocs);

        // use a (hash) map (same as any other JS object, but more explicit) to make lookup fast
        const idToRevs = new Map(handles.map((handle): [string, string] => [handle.id, handle.rev]));

        docsWithoutRevs.forEach(doc => {
            // find the rev in the map
            // it does not matter if rev is undefined since that means it was not yet in the database and will be set on next insert
            doc._rev = idToRevs.get(doc._id);
        });

        return docsWithoutRevs;
    }

    /**
     * Get the document with the given name/id
     * @param {string} name the _id field of the document
     * @returns {Promise<*>} the document
     */
    @WaitForQuota()
    public async get(name: string): Promise<any> {
        const response = await this.db.get(name);
        return response[0];
    }

    /**
     * Removed the document associated with id and rev from the db
     * @param {DocumentHandle} handle document to destroy
     * @returns {Promise<DocumentHandle>} information on the deleted document
     */
    @WaitForQuota()
    public async destroy(handle: DocumentHandle): Promise<DocumentHandle> {
        const response = await this.db.destroy(handle.id, handle.rev);
        /* istanbul ignore next */
        if (response[0]["ok"] != true) throw new Error("Failed to destroy document with handle=" + handle + ". Response: " + JSON.stringify(response));
        return response;
    }

    public async bulkDestroy(handles: Array<DocumentHandle>): Promise<Array<DocumentHandle>> {
        // set each handle to be deleted
        const deletedDocs = handles.map(handle => {
            return {_id: handle.id, _rev: handle.rev, _deleted: true};
        });
        return this.bulkInsert(deletedDocs);
    }

    /**
     * Destroys *everything* in the database, i.e. not only docs but also designs etc.
     * @return {Promise<Array<DocumentHandle>>} handles of all the destroyed objects (can't be access anymore)
     */
    public async destroyAll(): Promise<Array<DocumentHandle>> {
        // get revs
        const docs: Array<DocumentHandle> = await this.listAllDocumentHandles();
        // mark the all docs view as destroyed
        this.hasAllDocsView = false;
        // destroy them
        return this.bulkDestroy(docs);
    }

    /**
     * Gets all Documents with data and returns them
     * Does not return any design docs
     * @returns {Promise<Array<*>>} all docs
     */
    public async getAllDocs(): Promise<Array<any>> {
        // this is not an ideal solution, since it probably performs worse than other solutions
        // however, these other solutions are either very sketchy or require more work:
        // - calling list with params endkey="_" and another call with startkey="_"
        // - creating a view for just documents since they do not contain design documents (this might be a better solution, with the cost of having to manage the design document) - also counts towards query quota
        // - getting all documents then filtering out the ones beginning with "_"

        // Here we call find with a selector matching all. But this still counts towards our query quota
        return await this.find();
    }

    /**
     * @param {{_id: {$gt: string}}} selector to search for
     * @param {Array<*>} fields - If you also provide a "fields" array, only the included attributes are requested
     * @returns {Promise<Array<*>>} queried docs
     */
    @WaitForQuota()
    public async find(selector = {_id: {"$gt": "0"}}, fields?: Array<any>): Promise<Array<any>> {
        const response = await this.request({
            path: "_find",
            method: "POST",
            body: { selector: selector, fields: fields }
        });

        return response.docs;
    }

    public async createView(viewName: string, viewFunction: string, designDoc: string = designDocName) {
        const design = await this.getOrCreateDesignDoc(designDoc);

        const existingView = design.views[viewName];
        if (!isNullOrUndefined(existingView)) {
            if (existingView.map == viewFunction) {
                return; // no need to update, since it is already set
            }
        }

        logger.info(`Creating or updating the view "${designDoc}/${viewName}"`);

        // add new view / override existing
        design.views[viewName] = {
            map: viewFunction
        };

        // update the doc in db
        await this.createDesignDoc(design);
    }

    /**
     * Same as view, but with documents included
     * @param {string} viewName the name of the view to use. Must be already present in db
     * @param {string} designDoc in which document to look for the view. Must be present in db.
     *                           Defaults to the default design doc
     * @return {Promise<Array<*>>} as returned by the db
     */
    // TODO: TEST!
    public async viewWithDocs(viewName: string, designDoc: string = designDocName) {
        return this.view(viewName, designDoc, {include_docs: true});
    }

    /**
     * Returns everything returned by the defined view
     * @param {string} viewName the name of the view to use. Must be already present in db
     * @param {string} designDoc in which document to look for the view. Must be present in db.
     *                           Defaults to the default design doc
     * @param {*} params Optional further options see: https://console.bluemix.net/docs/services/Cloudant/api/using_views.html#querying-a-view
     * @return {Promise<Array<*>>} as returned by the db
     */
    @WaitForQuota()
    public async view(viewName: string, designDoc: string = designDocName, params: any = {}): Promise<Array<any>> {
        try {
            const result = await this.db.view(designDoc, viewName, params);
            return result[0].rows;
        } catch (e) {
            /* istanbul ignore next */
            throw Error("Failed to view " + designDoc + "/" + viewName + ". Error: " + e);
        }
    }

    // TODO: JSDOC
    // TODO: Tests?
    // http://docs.couchdb.org/en/2.1.1/api/database/changes.html
    // since can be now, or last seq number
    public async changes(since: string = "0") {
        return await this.request({ method: "GET", path: "_changes", qs: { since: since } });
    }

    private async getOrCreateDesignDoc(designDoc: string): Promise<DesignDoc> {
        const id = "_design/" + designDoc;
        try { // try to get the document
            return await this.request({
                path: id,
                method: "GET"
            });
        } catch (e) {
            // it does not exist, so create it
            return {_id: id, _rev: undefined, views: {}};
        }
    }

    private async createDesignDoc(designDoc: DesignDoc) {
        try {
            await this.request({
                path: designDoc._id,
                method: "PUT",
                body: designDoc
            });
        } catch (e) {
            /* istanbul ignore next */
            throw Error("Failed to create design doc " + JSON.stringify(designDoc) + "\n" + e);
        }
    }

    /**
     * Generic Request to cloudant
     * "db" field is set internally, so it must not be included in the query
     * @param {*} query further information at https://www.npmjs.com/package/nano#nanorequestopts-callback
     * @returns {Promise<*>} response
     */
    private async request(query: any): Promise<any> {
        orThrow(query, "Query may not be undefined!");

        return new Promise<Array<any>>((resolve, reject) => {
            query.db = this.name;

            this.cloudant.request(query, (err: any, result: any) => {
                if (err)
                    reject(err);
                else
                    resolve(result);
            });
        });
    }

    private static mapToDocumentHandle(a: Array<any>): Array<DocumentHandle> {
        return a.map(doc => {
            return {
                id: doc.id,
                rev: doc.value.rev
            };
        });
    }

}