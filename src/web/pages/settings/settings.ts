import {Page} from "../page";
import {usersDB} from "../../../main/services/db/dbs";
import {User} from "../../../main/services/db/user";
import {getLogger} from "log4js";

const logger = getLogger("server");

export class SettingsPage extends Page {
    public static readonly instance = new SettingsPage();

    public readonly route = "/settings";
    public readonly requiresAuthentication = true;

    private constructor() {
        super();
    }

    public getHandler(req: any, res: any) {
        usersDB().getOrCreateUser(req.user!.identities[0], req.user!.email)
            .then(user =>  {
                res.render("settings/settings", {
                    receiveEMailNotifications: user.receiveEMailNotifications,
                    updateInterval: user.updateInterval
                });
            })
            .catch(e => {
                logger.error("Failed getting or creating a user (this should have been caught in the usersDB class)", e);
                res.render("error/error");
            });
    }

    public postHandler(req: any, res: any) {
        const user = new User(req.user!.identities[0], req.user!.email, req!.body.receiveEMailNotifications == "on", req!.body.updateInterval);

        usersDB().update(user)
            .then(() => { res.redirect("/settings"); })
            .catch((e) => {
                logger.error("Failed to save user to db ", e);

                res.render("error/error");
            });
    }
}
