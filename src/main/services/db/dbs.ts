/*
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */


import {orDefault, orThrow} from "../../util/orDefault";
import {lazy} from "../../util/lazy";
import {isNullOrUndefined} from "util";
import {Cloudant, DB} from "./db";
import {env} from "../../config/env";
import {DataObjectsDB} from "./dataObjectsDB";
import {SourcesDB} from "./sourcesDB";
import {UsersDB} from "./usersDB";
import {ReportsDB} from "./reportsDB";

const CloudantPromise = require("cloudant-promise");
const cfenv = require("cfenv");

/**
 * Connects the Cloudant DB
 * @returns {Cloudant} the configured cloudant connection.
 */
function init(): Cloudant {

	// Extract Cloudant credentials from environment
	const credentials = isNullOrUndefined(env().VCAP_SERVICES)
		// local
		? {
			account: orThrow(env().CLOUDANT_USERNAME,
				"Must specify CLOUDANT_USERNAME when not in cloud"),
			password: orThrow(env().CLOUDANT_PASSWORD,
				"Must specify CLOUDANT_PASSWORD when not in cloud")
		}
		// cloud
		: /* istanbul ignore next */ cfenv.getAppEnv().getServiceCreds("Cloudant NoSQL DB-ow");

    return CloudantPromise(credentials);
}

export const cloudant: () => Cloudant = lazy(() => init());

export const testDB = lazy(() => new DB(cloudant(), "test"));

export const dataObjectsDB: () => DataObjectsDB = lazy(() => {
    const resolvedDataObjectsDBName: string = orDefault<string>(
        env().CLOUDANT_DATA_OBJECTS_DB,
        "data-objects"
    );

    return new DataObjectsDB(cloudant(), resolvedDataObjectsDBName);
});

export const usersDB: () => UsersDB = lazy(() => {
    const resolvedUsersDBName: string = orDefault<string>(
        env().CLOUDANT_USERS_DB,
        "users"
    );

    return new UsersDB(cloudant(), resolvedUsersDBName);
});

export const sourcesDB: () => SourcesDB = lazy(() => {
    const resolvedSourcesDBName: string = orDefault<string>(
        env().CLOUDANT_SOURCES_DB,
        "sources"
    );

    return new SourcesDB(cloudant(), resolvedSourcesDBName);
});

export const reportsDB: () => ReportsDB = lazy(() => {
   const resolvedReportsDBName = orDefault<string>(env().CLOUDANT_REPORTS_DB, "reports");

   return new ReportsDB(cloudant(), resolvedReportsDBName);
});