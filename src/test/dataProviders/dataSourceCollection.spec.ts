import {AsyncTest, TestFixture, Expect, Timeout} from "alsatian";
import {DataSourceCollection} from "../../main/dataProviders/dataSourceCollection";
import {MinDataObject} from "./dataObjects/minDataObject";

@TestFixture("Testing DataSourceCollection")
export class DataSourceCollectionSpec {

    @AsyncTest("all")
    @Timeout(4000)
    public async testAll() {
        const c = new DataSourceCollection();

        //Important to start with seq number 0 in order to ensure the correct detection of updates
        Expect(c["last_seq"]).toEqual("0");
        
        //Granted that at least some sources are in the db
        Expect(c["providerContexts"]).toEqual([]);
        await c.updateSources();
        Expect(c["providerContexts"]).toBeDefined();
        Expect(c["providerContexts"]).not.toEqual([]);

        Expect(await c.getContext()).toBeDefined();
    }

    @AsyncTest("DataSourceCollection test data")
    public async testWithData() {
        const c = new DataSourceCollection();
        // inject the contexts
        const a = new MinDataObject("a");
        const b = new MinDataObject("b");
        c["providerContexts"] = [
            async () => [a],
            async () => [b]
        ];

        const ctx = c.getContext();
        Expect(await ctx()).toEqual([a, b]);
    }
}