/**
 The MIT License (MIT)

 Copyright (c) 2017-2018 Timothy Earley, Frederic Metzler, Ruslan Sandler, Lukas Struck, Nils Thoma

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import {AsyncTest, TestCase, TestFixture, Expect, Timeout} from "alsatian";
import {TwitterProvider} from "../../main/dataProviders/twitterProvider";

@TestFixture("Testing twitter Provider")
export class TwitterProviderSpec {

    @TestCase({ screen_name: "exploitdb", count: 100 })
    @TestCase({ screen_name: "McAfee_Labs", count: 100 })
    @TestCase({ screen_name: "BleepinComputer", count: 100 })
    @AsyncTest(".getData Twitter")
    @Timeout(5000) // May take five seconds
    public async testGetRecentTweets(options: any) {
        const tweets = await TwitterProvider.instance.getContext(options)();

        // The list of Tweets should not be empty
        Expect(tweets).not.toBeEmpty();
    }
}
